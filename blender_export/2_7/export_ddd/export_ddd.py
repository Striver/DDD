# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

import os

import bpy
import mathutils
import bpy_extras.io_utils

from array import array

from progress_report import ProgressReport, ProgressReportSubstep

from .export_ddd_static_mesh import write_file_static_mesh
from .export_ddd_skelet_anim_mesh import write_file_skelet_anim_mesh


def _write(context, filepath,
           EXPORT_TRI,  # ok
           EXPORT_EDGES,
           EXPORT_SMOOTH_GROUPS,
           EXPORT_SMOOTH_GROUPS_BITFLAGS,
           EXPORT_NORMALS,  # ok
           EXPORT_UV,  # ok
           EXPORT_APPLY_MODIFIERS,  # ok
           EXPORT_BLEN_OBS,
           EXPORT_GROUP_BY_OB,
           EXPORT_GROUP_BY_MAT,
           EXPORT_KEEP_VERT_ORDER,
           EXPORT_POLYGROUPS,
           EXPORT_ANIMATION,
           EXPORT_GLOBAL_MATRIX,
           EXPORT_PATH_MODE,  # Not used
           ):

    with ProgressReport(context.window_manager) as progress:
        base_name, ext = os.path.splitext(filepath)
        context_name = [base_name, '', '', ext]  # Base name, scene name, frame number, extension

        scene = context.scene

        # Exit edit mode before exporting, so current object states are exported properly.
        if bpy.ops.object.mode_set.poll():
            bpy.ops.object.mode_set(mode='OBJECT')

        orig_frame = scene.frame_current

        """
        # Export an animation?
        if EXPORT_ANIMATION:
            scene_frames = range(scene.frame_start, scene.frame_end + 1)  # Up to and including the end frame.
        else:
            scene_frames = [orig_frame]  # Dont export an animation.

        # Loop through all frames in the scene and export.
        progress.enter_substeps(len(scene_frames))
        if EXPORT_ANIMATION:  # Add frame to the filepath.
            context_name[2] = '_%.6d' % frame
        
        scene.frame_set(frame, 0.0)
        """
        
        obj = context.active_object

        full_path = ''.join(context_name)

        # erm... bit of a problem here, this can overwrite files when exporting frames. not too bad.
        # EXPORT THE FILE.
        progress.enter_substeps(1)
        if obj.type == "MESH":
            if EXPORT_ANIMATION:
                write_file_skelet_anim_mesh(full_path, obj, scene,
                   EXPORT_SMOOTH_GROUPS,
                   EXPORT_SMOOTH_GROUPS_BITFLAGS,
                   EXPORT_APPLY_MODIFIERS,
                   #EXPORT_POLYGROUPS,
                   EXPORT_GLOBAL_MATRIX,
                   EXPORT_PATH_MODE,
                   progress,
                   )
            else:
                write_file_static_mesh(full_path, obj, scene,
                       EXPORT_SMOOTH_GROUPS,
                       EXPORT_SMOOTH_GROUPS_BITFLAGS,
                       EXPORT_APPLY_MODIFIERS,
                       #EXPORT_POLYGROUPS,
                       EXPORT_GLOBAL_MATRIX,
                       EXPORT_PATH_MODE,
                       progress,
                       )
        else:
            raise ValueError("Активный объект не является мешем!")
        progress.leave_substeps()

        scene.frame_set(orig_frame, 0.0)
        #progress.leave_substeps()


"""
Currently the exporter lacks these features:
* multiple scene export (only active scene is written)
* particles
"""



def save(context,
         filepath,
         *,
         #use_triangles=False,
         #use_edges=True,
         #use_normals=False,
         use_smooth_groups=False,
         use_smooth_groups_bitflags=False,
         #use_uvs=True,
         #use_materials=True,
         use_mesh_modifiers=True,
         #use_blen_objects=True,
         #group_by_object=False,
         #group_by_material=False,
         #keep_vertex_order=False,
         #use_vertex_groups=False,
         use_animation=False,
         global_matrix=None,
         path_mode='AUTO'
         ):

    _write(context, filepath,
           EXPORT_TRI=True, #use_triangles,
           EXPORT_EDGES=False, #use_edges,
           EXPORT_SMOOTH_GROUPS=use_smooth_groups,
           EXPORT_SMOOTH_GROUPS_BITFLAGS=use_smooth_groups_bitflags,
           EXPORT_NORMALS=True, #use_normals,
           EXPORT_UV=True, #use_uvs,
           #EXPORT_MTL=False, #use_materials,
           EXPORT_APPLY_MODIFIERS=use_mesh_modifiers,
           EXPORT_BLEN_OBS=True, #use_blen_objects,
           EXPORT_GROUP_BY_OB=False, #group_by_object,
           EXPORT_GROUP_BY_MAT=False, #group_by_material,
           EXPORT_KEEP_VERT_ORDER=False, #keep_vertex_order,
           EXPORT_POLYGROUPS=False, #use_vertex_groups,
           EXPORT_ANIMATION=use_animation,
           EXPORT_GLOBAL_MATRIX=global_matrix,
           EXPORT_PATH_MODE=path_mode,
           )

    return {'FINISHED'}
