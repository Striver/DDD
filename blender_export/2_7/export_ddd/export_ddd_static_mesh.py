# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

import os

import bpy
import mathutils
import bpy_extras.io_utils

from array import array

from progress_report import ProgressReport, ProgressReportSubstep

from .export_ddd_funkcii import mesh_triangulate, veckey3d, veckey2d, StrokiVFloat, StrokiVUShort, \
    raschet_uv, raschet_normaley, raschet_graney, pereschet_graney

VERSION = 1    
    
def write_file_static_mesh(filepath, obj, scene,
               #EXPORT_TRI=True,
               EXPORT_SMOOTH_GROUPS=False,
               EXPORT_SMOOTH_GROUPS_BITFLAGS=False,
               #EXPORT_NORMALS=False,
               #EXPORT_UV=True,
               #EXPORT_MTL=True,
               EXPORT_APPLY_MODIFIERS=True,
               #EXPORT_BLEN_OBS=True,
               #EXPORT_GROUP_BY_OB=False,
               #EXPORT_GROUP_BY_MAT=False,
               #EXPORT_KEEP_VERT_ORDER=False,
               #EXPORT_POLYGROUPS=False,
               EXPORT_GLOBAL_MATRIX=None,
               EXPORT_PATH_MODE='AUTO',
               progress=ProgressReport(),
               ): 
    """Выгрузка статического меша. У этого меша обязательно должна присутсвовать UV-карта."""
    
    if EXPORT_GLOBAL_MATRIX is None:
        EXPORT_GLOBAL_MATRIX = mathutils.Matrix()
    
    with ProgressReportSubstep(progress, 2, "DDD Export path: %r" % filepath, "DDD Export Finished") as subprogress1:
        
        # Initialize totals, these are updated each object
        #totverts = totuvco = totno = 1
        face_vert_index = 1
        
        subprogress1.enter_substeps(1)
        with ProgressReportSubstep(subprogress1, 6) as subprogress2:
            #uv_unique_count = no_unique_count = 0
            
            ob, ob_mat = obj, obj.matrix_world
            try:
                me = ob.to_mesh(scene, EXPORT_APPLY_MODIFIERS, calc_tessface=False,
                                settings='PREVIEW')
            except RuntimeError:
                me = None
                raise ValueError("Активный объект не преобразуется в меш!")

            # _must_ do this before applying transformation, else tessellation may differ
            #if EXPORT_TRI:
                # _must_ do this first since it re-allocs arrays
            mesh_triangulate(me)
            #print("me.vertices1=", [v.co[:] for v in me.vertices[:]])
            print("EXPORT_GLOBAL_MATRIX=", EXPORT_GLOBAL_MATRIX)
            print("ob_mat=", ob_mat)

            me.transform(EXPORT_GLOBAL_MATRIX * ob_mat)
            # If negative scaling, we have to invert the normals...
            if ob_mat.determinant() < 0.0:
                me.flip_normals()

            print("me.vertices2=", [v.co[:] for v in me.vertices[:]])
            
            if True: #EXPORT_UV:
                faceuv = len(me.uv_textures) > 0
                if faceuv:
                    uv_texture = me.uv_textures.active.data[:]
                    uv_layer = me.uv_layers.active.data[:]
            #else:
            #    faceuv = False
            if not faceuv:
                raise ValueError("У активного объекта отсутствует UV-карта!")

            me_verts = me.vertices[:]

            # Make our own list so it can be sorted to reduce context switching
            face_index_pairs = [(face, index) for index, face in enumerate(me.polygons)]
            # faces = [ f for f in me.tessfaces ]

            if not (len(face_index_pairs) + len(me.vertices)):  # Make sure there is something to write
                # clean up
                bpy.data.meshes.remove(me)
                raise ValueError("Активный объект не имеет данных!")  # dont bother with this mesh.

            if face_index_pairs:    # EXPORT_NORMALS and 
                me.calc_normals_split()
                # No need to call me.free_normals_split later, as this mesh is deleted anyway!

            loops = me.loops

            if (EXPORT_SMOOTH_GROUPS or EXPORT_SMOOTH_GROUPS_BITFLAGS) and face_index_pairs:
                smooth_groups, smooth_groups_tot = me.calc_smooth_groups(EXPORT_SMOOTH_GROUPS_BITFLAGS)
                if smooth_groups_tot <= 1:
                    smooth_groups, smooth_groups_tot = (), 0
            else:
                smooth_groups, smooth_groups_tot = (), 0            

            # Set the default mat to no material and no image.
            contextMat = 0, 0  # Can never be this, so we will label a new material the first chance we get.
            contextSmooth = None  # Will either be true or false,  set bad to force initialization switch.

            # Vert
            s_v = []
            for v in me_verts:
                s_v.append('v %.6f %.6f %.6f\n' % v.co[:])
            print("s_v=", s_v)
            subprogress2.step()
            
            # UV
            s_vt, uv_face_mapping = raschet_uv(faceuv, face_index_pairs, loops, uv_layer)
            subprogress2.step()

            # NORMAL, Smooth/Non smoothed.
            s_vn, loops_to_normals = raschet_normaley (face_index_pairs, loops) # EXPORT_NORMALS, 
            subprogress2.step()

            # Грани
            s_f= raschet_graney(face_index_pairs, faceuv, me_verts, uv_texture, # EXPORT_NORMALS, 
                                uv_face_mapping, loops_to_normals, smooth_groups, face_vert_index)
            subprogress2.step()

            vershiny=[]
            normali=[]
            texcoord=[]
            unicalny_troiki, grani = pereschet_graney(s_f)
            for i in unicalny_troiki:
                vershiny.append(s_v[int(i[0])-1])
                texcoord.append(s_vt[int(i[1])-1])
                normali.append(s_vn[int(i[2])-1])
            print("vershiny=", vershiny)
            #print ("Vershin="+str(len(vershiny))+"  Graney="+str(len(grani)))

            rezult="DDD ".encode("utf8")
            rezult+=array('H',[VERSION, len(unicalny_troiki),len(grani)]).tobytes()
            rezult+=StrokiVFloat(vershiny)
            rezult+=StrokiVFloat(texcoord)
            rezult+=StrokiVFloat(normali)
            rezult+=StrokiVUShort(grani)
            
            with open(filepath, "wb") as fil:
                fil.write(rezult)
            
            bpy.data.meshes.remove(me)
            
        subprogress1.leave_substeps("Закончили выгружать геометрию объекта '%s'. Вершин: %s, граней: %s" % 
                                    (obj.name, len(vershiny), len(grani)))

