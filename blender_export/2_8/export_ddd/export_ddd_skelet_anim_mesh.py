# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

import os

import bpy
import mathutils
import bpy_extras.io_utils

from array import array

#from progress_report import ProgressReport, ProgressReportSubstep
from bpy_extras.wm_utils.progress_report import ProgressReport, ProgressReportSubstep

from .export_ddd_funkcii import mesh_triangulate, veckey3d, veckey2d, StrokiVFloat, StrokiVUShort, \
    StrokiVBytes, ZapisiVBytes, \
    raschet_uv, raschet_normaley, raschet_graney, pereschet_graney, gruppy_vershin, poluchit_kosti0, \
    poluchit_animacii_skeleta, pereschet_animaciy
    
from .export_ddd_static_mesh import write_file_static_mesh

VERSION = 2 
SKELET_EXT = ".sk"   
    
def write_file_skelet_anim_mesh(filepath, obj, scene, context, depsgraph0,
               EXPORT_SMOOTH_GROUPS=False,
               EXPORT_SMOOTH_GROUPS_BITFLAGS=False,
               EXPORT_APPLY_MODIFIERS=True,
               #EXPORT_POLYGROUPS=False,
               EXPORT_GLOBAL_MATRIX=None,
               EXPORT_PATH_MODE='AUTO',
               progress=ProgressReport(),
               ): 
    """Выгрузка объекта вместе со скелетом и скелетной анимацией. 
    Модификатор арматуры должен действовать через группы вершин.
    Если включена опция "Применить модификаторы", то желательно, чтобы модификатор арматуры
    был верхним в списке, иначе возможны расхождения в форме объекта между Блендером и DDD."""
    
    if EXPORT_GLOBAL_MATRIX is None:
        EXPORT_GLOBAL_MATRIX = mathutils.Matrix()
    
    with ProgressReportSubstep(progress, 2, "DDD Export path: %r" % filepath, "DDD Export Finished") as subprogress1:
        
        # Initialize totals, these are updated each object
        #totverts = totuvco = totno = 1
        face_vert_index = 1
        
        subprogress1.enter_substeps(1)
        with ProgressReportSubstep(subprogress1, 6) as subprogress2:
            #uv_unique_count = no_unique_count = 0
            
            ob, ob_mat = obj, obj.matrix_world
            ob_for_convert0 = ob.evaluated_get(depsgraph0) if EXPORT_APPLY_MODIFIERS else ob.original
            vertGroupNames, vgroupsMap, vesa = gruppy_vershin(ob, ob_for_convert0.to_mesh().vertices[:])
            ob_for_convert0.to_mesh_clear()
            
            armature_ob = None
            for modifier in ob.modifiers.values():
                if modifier.type == 'ARMATURE' and modifier.use_vertex_groups and modifier.object and modifier.show_viewport:
                    armature_ob = modifier.object
                    modifier.show_viewport = False # При применении модификаторов этот не должен влиять
                    break
            if armature_ob == None:
                raise ValueError("Активный объект не имеет скелета в модификаторах.!")
                subprogress1.leave_substeps("Активный объект не имеет скелета в модификаторах. Выгружаем статический меш.")
                return write_file_static_mesh(full_path, obj, scene,
                       EXPORT_SMOOTH_GROUPS,
                       EXPORT_SMOOTH_GROUPS_BITFLAGS,
                       EXPORT_APPLY_MODIFIERS,
                       #EXPORT_POLYGROUPS,
                       EXPORT_GLOBAL_MATRIX,
                       EXPORT_PATH_MODE,
                       progress,
                       )
            #armature_ob = armature_ob.copy()
            # try:
                # me = ob.to_mesh(scene, EXPORT_APPLY_MODIFIERS, calc_tessface=False,
                                # settings='PREVIEW')
                                
            depsgraph1 = context.evaluated_depsgraph_get()
            ob_for_convert = ob.evaluated_get(depsgraph1) if EXPORT_APPLY_MODIFIERS else ob.original

            try:
                me = ob_for_convert.to_mesh()
            except RuntimeError:
                me = None
                raise ValueError("Активный объект не преобразуется в меш!")

            # _must_ do this before applying transformation, else tessellation may differ
            if True: #EXPORT_TRI:
                # _must_ do this first since it re-allocs arrays
                mesh_triangulate(me)

            #me.transform(EXPORT_GLOBAL_MATRIX * ob_mat)
            me.transform(EXPORT_GLOBAL_MATRIX @ ob_mat)
            # If negative scaling, we have to invert the normals...
            if ob_mat.determinant() < 0.0:
                me.flip_normals()

            if True: #EXPORT_UV:
                #faceuv = len(me.uv_textures) > 0
                faceuv = len(me.uv_layers) > 0
                if faceuv:
                    #uv_texture = me.uv_textures.active.data[:]
                    uv_layer = me.uv_layers.active.data[:]
            #else:
            #    faceuv = False
            if not faceuv:
                raise ValueError("У активного объекта отсутствует UV-карта!")

            me_verts = me.vertices[:]

            # Make our own list so it can be sorted to reduce context switching
            face_index_pairs = [(face, index) for index, face in enumerate(me.polygons)]
            # faces = [ f for f in me.tessfaces ]

            if not (len(face_index_pairs) + len(me.vertices)):  # Make sure there is something to write
                # clean up
                # bpy.data.meshes.remove(me)
                ob_for_convert.to_mesh_clear()
                raise ValueError("Активный объект не имеет данных!")  # dont bother with this mesh.

            if face_index_pairs:    # EXPORT_NORMALS and 
                me.calc_normals_split()
                # No need to call me.free_normals_split later, as this mesh is deleted anyway!

            loops = me.loops

            if (EXPORT_SMOOTH_GROUPS or EXPORT_SMOOTH_GROUPS_BITFLAGS) and face_index_pairs:
                smooth_groups, smooth_groups_tot = me.calc_smooth_groups(EXPORT_SMOOTH_GROUPS_BITFLAGS)
                if smooth_groups_tot <= 1:
                    smooth_groups, smooth_groups_tot = (), 0
            else:
                smooth_groups, smooth_groups_tot = (), 0            

            # Set the default mat to no material and no image.
            contextMat = 0, 0  # Can never be this, so we will label a new material the first chance we get.
            contextSmooth = None  # Will either be true or false,  set bad to force initialization switch.

            # Вершины
            s_v = []
            for v in me_verts:
                s_v.append('v %.6f %.6f %.6f\n' % v.co[:])
            subprogress2.step()
            
            # UV
            s_vt, uv_face_mapping = raschet_uv(faceuv, face_index_pairs, loops, uv_layer)
            subprogress2.step()

            # NORMAL, Smooth/Non smoothed.
            s_vn, loops_to_normals = raschet_normaley (face_index_pairs, loops) # EXPORT_NORMALS, 
            subprogress2.step()

            # Грани
            s_f= raschet_graney(face_index_pairs, faceuv, me_verts, #uv_texture,     # EXPORT_NORMALS, 
                                uv_face_mapping, loops_to_normals, smooth_groups, face_vert_index)
            subprogress2.step()

            # Группы вершин
            # vertGroupNames, vgroupsMap, vesa = gruppy_vershin(ob, me_verts)
            print()
            print("len(me_verts)=", len(me_verts))
            #print("vesa.keys()=", list(vesa.keys()))
            print("len(vesa)=", len(list(vesa.keys())))
            print("vertGroupNames=", vertGroupNames)
            #print("vesa=", vesa)
            
            # Кости скелета
            # Изначальные (в режиме редактирования)
            skelet = poluchit_kosti0(armature_ob, EXPORT_GLOBAL_MATRIX)
            # Анимации скелета
            animacii1 = poluchit_animacii_skeleta(scene, armature_ob, EXPORT_GLOBAL_MATRIX)
            #animacii2 = {}
            kosti = list(skelet.keys())
            kosti.sort()
            animacii2 = pereschet_animaciy(kosti, animacii1)
            """
            kosti_dict = {}
            for ind, kost in enumerate(kosti):
                kosti_dict[kost] = ind
            for actionName, ((frameBegin, frameEnd), dispatcher1, action1) in animacii1.items():
                action2 = []
                dispatcher2 = []
                for kost, napravlenie in dispatcher1:
                    #action2.append(action1[(kost, napravlenie)])
                    dispatcher2.append((kosti_dict[kost], napravlenie))
                for kadr in range(frameEnd-frameBegin+1):
                    action2.append([])
                    for typ_krivoi in dispatcher1:
                        action2[-1].append(action1[typ_krivoi][kadr])
                animacii2[actionName] = (frameBegin, frameEnd), dispatcher2, action2
            """
            ierarhia = [(kosti.index(skelet[kost][3]) if skelet[kost][3] else -1) for kost in kosti]
            print("ierarhia =", ierarhia)
            skelet = [(skelet[kost][0].to_tuple() + \
                       skelet[kost][1].to_tuple() + \
                       (skelet[kost][2].w, skelet[kost][2].x, 
                        skelet[kost][2].y, skelet[kost][2].z)
                        ) for kost in kosti]
            action_names = list(animacii2.keys())
                    
            
            
            vershiny=[]
            normali=[]
            texcoord=[]
            vesa_vershin = []
            unicalny_troiki, grani = pereschet_graney(s_f)
            for ind, troika in enumerate(unicalny_troiki):
                num_vert = int(troika[0])-1
                vershiny.append(s_v[num_vert])
                texcoord.append(s_vt[int(troika[1])-1])
                normali.append(s_vn[int(troika[2])-1])
                #print("vesa[num_vert]=", vesa[num_vert])
                for group_ind, ves in vesa[num_vert]:
                    vesa_vershin.append((group_ind, ind, ves))


            print ("Vershin="+str(len(vershiny))+"  Graney="+str(len(grani)))
            #print ("vershiny=", vershiny)
            print ("Vesov=", len(vesa_vershin))
            #print ("Vesa vershin=", vesa_vershin)

           
            """
            rezult="DDD "   #.encode("utf8")
            rezult+=str(VERSION) + '\n'
            rezult_skelet = rezult
            
            rezult+="вершин:%s, граней:%s, групп:%s, весов:%s\n" % \
                (len(vershiny), len(grani), len(vertGroupNames), len(vesa_vershin))
            rezult+='vershiny=' + repr(vershiny) + '\n'
            rezult+='normali=' + repr(normali) + '\n'
            rezult+='texcoord=' + repr(texcoord) + '\n'
            rezult+='vertGroupNames=' + repr(vertGroupNames) + '\n'
            rezult+='vesa_vershin=' + repr(vesa_vershin) + '\n'

            rezult_skelet+='kosti=' + repr(kosti) + '\n'
            rezult_skelet+='skelet=' + repr(skelet) + '\n'
            rezult_skelet+='animacii=' + repr(animacii2) + '\n'
            
            
            """
            rezult_skelet = rezult = "DDD ".encode("utf8")
            rezult+=array('H',[VERSION, len(unicalny_troiki), len(grani), 
                               len(vertGroupNames), len(vesa_vershin)]).tobytes()
            rezult+=StrokiVFloat(vershiny)
            rezult+=StrokiVFloat(texcoord)
            rezult+=StrokiVFloat(normali)
            rezult+=StrokiVUShort(grani)
            rezult+=StrokiVBytes(vertGroupNames)
            rezult+=ZapisiVBytes(vesa_vershin, ['H', 'H', 'f'])
            
            rezult_skelet+=array('H',[VERSION, len(kosti), len(animacii2)]).tobytes()
            rezult_skelet+=StrokiVBytes(kosti)
            rezult_skelet+=array('h', ierarhia).tobytes()
            for nachalnaja_kost in skelet:
                rezult_skelet+=array('f', list(nachalnaja_kost)).tobytes()
            # print("skelet=", skelet)
            rezult_skelet+=StrokiVBytes(action_names)
            for actionName in action_names:
                predely_kadrov, dispatcher, krivye = animacii2[actionName]
                rezult_skelet+=array('H', [len(dispatcher), len(krivye)]).tobytes()
                rezult_skelet+=array('h', list(predely_kadrov)).tobytes()
                rezult_skelet+=ZapisiVBytes(dispatcher, ['H', 'H'])
                rezult_skelet+=array('f', sum(krivye, [])).tobytes()
            
            #"""
            
            
            with open(filepath, "wb") as fil:    # wb
                fil.write(rezult)

            skelet_filepath = os.path.splitext(filepath)[0] + SKELET_EXT
            with open(skelet_filepath, "wb") as fil:    # wb
                fil.write(rezult_skelet)
            
            #bpy.data.meshes.remove(me)
            modifier.show_viewport = True # Возвращаем работоспособность модификатора арматуры
            ob_for_convert.to_mesh_clear()
                
        subprogress1.leave_substeps("Закончили выгружать геометрию объекта '%s'. Вершин: %s, граней: %s" % 
                                    (obj.name, len(vershiny), len(grani)))

