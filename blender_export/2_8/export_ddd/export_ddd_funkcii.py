# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8 compliant>

#import os

import bpy
import mathutils
#import bpy_extras.io_utils

from array import array
from collections import defaultdict


def name_compat(name):
    if name is None:
        return 'None'
    else:
        return name.replace(' ', '_')


def mesh_triangulate(me):
    import bmesh
    bm = bmesh.new()
    bm.from_mesh(me)
    bmesh.ops.triangulate(bm, faces=bm.faces)
    bm.to_mesh(me)
    bm.free()


def test_nurbs_compat(ob):
    if ob.type != 'CURVE':
        return False

    for nu in ob.data.splines:
        if nu.point_count_v == 1 and nu.type != 'BEZIER':  # not a surface and not bezier
            return True

    return False


def veckey3d(v):
    return round(v.x, 4), round(v.y, 4), round(v.z, 4)


def veckey2d(v):
    return round(v[0], 4), round(v[1], 4)


def StrokiVFloat(stroki):
    d=[list(x.split()[1:]) for x in stroki]
    e=[[float(x) for x in y] for y in d]
    ret=[]
    for i in e:
        ret+=i
    #print (sum(ret))
    ret=array('f',ret).tobytes()
    return ret


def StrokiVUShort(stroki):
    d=[list(x.split()[1:]) for x in stroki]
    e=[[int(x) for x in y] for y in d]
    ret=[]
    for i in e:
        ret+=i
    ret=array('H', ret).tobytes()
    return ret
  

def StrokiVBytes(stroki):
    ret = b''
    for stroka in stroki:
        bytes = stroka.encode("utf8")
        ret += array('H',[len(bytes)]).tobytes() + bytes
    return ret
    
    
def ZapisiVBytes(spisok, types):
    ret = b''
    for zapis in spisok:
        for ind, typ in enumerate(types):
            ret += array(typ, [zapis[ind]]).tobytes()
    return ret
    
    
def ConvertVesa(vertGroupNames, vgroupsMap):
    ret = b''
    for ind_vert, ind_groupgroup, ves in enumerate(vgroupsMap):
        ret += array('H',[ind_group, ind_vert]).tobytes()
        ret += array('f',[ves]).tobytes()
    return ret
    
    
def raschet_uv(faceuv, face_index_pairs, loops, uv_layer):
    #global uv_unique_count
    # UV
    uv_unique_count = 0
    s_vt=[]
    if faceuv:
        # in case removing some of these dont get defined.
        uv = f_index = uv_index = uv_key = uv_val = uv_ls = None

        uv_face_mapping = [None] * len(face_index_pairs)

        uv_dict = {}
        uv_get = uv_dict.get
        for f, f_index in face_index_pairs:
            uv_ls = uv_face_mapping[f_index] = []
            for uv_index, l_index in enumerate(f.loop_indices):
                uv = uv_layer[l_index].uv
                # include the vertex index in the key so we don't share UV's between vertices,
                # allowed by the OBJ spec but can cause issues for other importers, see: T47010.

                # this works too, shared UV's for all verts
                #~ uv_key = veckey2d(uv)
                uv_key = loops[l_index].vertex_index, veckey2d(uv)

                uv_val = uv_get(uv_key)
                if uv_val is None:
                    uv_val = uv_dict[uv_key] = uv_unique_count
                    #s_vt.append('vt %.6f %.6f\n' % uv[:])
                    s_vt.append('vt %.6f %.6f\n' % (uv[0], 1.0-uv[1]))  # Если не сделать 1-y, то получается зеркальное наложение текстуры
                    uv_unique_count += 1
                uv_ls.append(uv_val)

        del uv_dict, uv, f_index, uv_index, uv_ls, uv_get, uv_key, uv_val
        # Only need uv_unique_count and uv_face_mapping
    return s_vt, uv_face_mapping


def raschet_normaley(face_index_pairs, loops):  # EXPORT_NORMALS, 
    no_unique_count = 0
    s_vn=[]
    if True:    #EXPORT_NORMALS:
        no_key = no_val = None
        normals_to_idx = {}
        no_get = normals_to_idx.get
        loops_to_normals = [0] * len(loops)
        for f, f_index in face_index_pairs:
            for l_idx in f.loop_indices:
                no_key = veckey3d(loops[l_idx].normal)
                no_val = no_get(no_key)
                if no_val is None:
                    no_val = normals_to_idx[no_key] = no_unique_count
                    s_vn.append('vn %.4f %.4f %.4f\n' % no_key)
                    no_unique_count += 1
                loops_to_normals[l_idx] = no_val
        del normals_to_idx, no_get, no_key, no_val
    #else:
    #    loops_to_normals = []
    return s_vn, loops_to_normals


def raschet_graney(face_index_pairs, faceuv, me_verts, # uv_texture,    # EXPORT_NORMALS, 
                   uv_face_mapping, loops_to_normals, smooth_groups, face_vert_index):
    s_f=[]
    totverts = totuvco = totno = 1
    for f, f_index in face_index_pairs:
        f_smooth = f.use_smooth
        if f_smooth and smooth_groups:
            f_smooth = smooth_groups[f_index]

        """
        if faceuv:
            tface = uv_texture[f_index]
            f_image = tface.image

    
        if f_smooth != contextSmooth:
            if f_smooth:  # on now off
                if smooth_groups:
                    f_smooth = smooth_groups[f_index]
                    s_f.append('s %d\n' % f_smooth)
                else:
                    s_f.append('s 1\n')
            else:  # was off now on
                s_f.append('s off\n')
            contextSmooth = f_smooth
        """

        f_v = [(vi, me_verts[v_idx], l_idx)
               for vi, (v_idx, l_idx) in enumerate(zip(f.vertices, f.loop_indices))]

        gran = 'f'
        if faceuv:
            if True:    #EXPORT_NORMALS:
                for vi, v, li in f_v:
                    gran += (" %d/%d/%d" % (totverts + v.index,
                                      totuvco + uv_face_mapping[f_index][vi],
                                      totno + loops_to_normals[li],
                                      ))  # vert, uv, normal
            #else:  # No Normals
            #    for vi, v, li in f_v:
            #       gran += (" %d/%d" % (totverts + v.index,
            #                       totuvco + uv_face_mapping[f_index][vi],
            #                       ))  # vert, uv

            face_vert_index += len(f_v)

        else:  # No UV's
            if True: # EXPORT_NORMALS:
                for vi, v, li in f_v:
                    gran += (" %d//%d" % (totverts + v.index, totno + loops_to_normals[li]))
            #else:  # No Normals
            #    for vi, v, li in f_v:
            #        gran += (" %d" % (totverts + v.index))
        s_f.append(gran+'\n')   
    return s_f


def pereschet_graney(s_f):
    a=[x.split() for x in s_f]
    b=[[tuple(x.split('/')) for x in y[1:]] for y in a]
    c=set()
    for i in b:
        c=c | set(i)
    c=list(c)
    grani=[]
    for i in b:
        stroka=[]
        for j in i:
            stroka.append(str(c.index(j)))
        grani.append("f "+' '.join(stroka))
    #print ("c=")
    #print (c)
    #print ("grani=")
    #print (grani)
    return c, grani 
    
    
def gruppy_vershin(ob, me_verts):
    from collections import defaultdict
    vertGroupNames = ob.vertex_groups.keys()[:]
    vgroupsMap = [[] for _i in range(len(me_verts))]
    vesa = defaultdict(list)
    if vertGroupNames:
        for v_idx, v_ls in enumerate(vgroupsMap):
            v_ls[:] = [(vertGroupNames[g.group], g.weight) for g in me_verts[v_idx].groups]
            for group_name, ves in v_ls:
                group_ind = vertGroupNames.index(group_name)
                vesa[v_idx].append((group_ind, ves))
                print(".", end='')
    return vertGroupNames, vgroupsMap, vesa


def poluchit_kosti0(arm_ob, global_matrix):
    arm_data = arm_ob.data.copy()
    # arm_data.transform(global_matrix * arm_ob.matrix_world)
    arm_data.transform(global_matrix @ arm_ob.matrix_world)
    kosti = {}
    for name, bone in arm_data.bones.items():
        parent = bone.parent.name if bone.parent else None
        kosti[name] = (mathutils.Vector(bone.head_local), 
                       mathutils.Vector(bone.tail_local), 
                       bone.matrix_local.to_quaternion(), 
                       parent)
    bpy.data.armatures.remove(arm_data)
    print("kosti0=", kosti)
    return kosti
    
    
NAPRAVLENIA = {'location0':1, 'location1':2, 'location2':4, 
               'rotation_quaternion0':8, 'rotation_quaternion1':16, 'rotation_quaternion2':32, 'rotation_quaternion3':64, 
               'scale0':128, 'scale1':256, 'scale2':512}

NAPR_LOC = [1, 2, 4]
NAPR_ROT = [8, 16, 32, 64]
NAPR_SCALE = [128, 256, 512]

EPSILON = 1e-6

               
def poluchit_dannye_animacii_curve(arm, action, frameBegin, frameEnd):
    #from collections import defaultdict
    fcu = action.fcurves
    curves = defaultdict(list)
    dispatcher = []
    for fc in fcu:
        dp = fc.data_path
        skobka1 = dp.find('["') 
        skobka2 = dp.find('"]')
        bone = dp[skobka1+2:skobka2]
        index = fc.array_index
        napravlenie = NAPRAVLENIA[dp[skobka2+3:] + str(index)]
        dispatcher.append((bone, napravlenie))
        for frame in range(int(frameBegin), int(frameEnd)+1):
            curves[(bone, napravlenie)].append(fc.evaluate(frame)) #((bone, napravlenie), frame, fc.evaluate(frame))) #
    return (int(frameBegin), int(frameEnd)), dispatcher, dict(curves)

    
def okolo_1(value):
    return abs(value-1)
    
# Ограничения: отрицательный масштаб правильно не работает.
def poluchit_dannye_animacii_pose(scene, arm, frameBegin, frameEnd, transform_matrix):
    animations1 = defaultdict(list)
    for frame in range(int(frameBegin), int(frameEnd)+1):
        scene.frame_set(frame)
        for bone in arm.pose.bones:
            bone_data = arm.data.bones[bone.name]
            if not(bone_data.use_deform):
                continue
            #matrix = transform_matrix * bone.matrix_channel
            #translation = transform_matrix * (bone.matrix.to_translation() - bone_data.head_local)
            translation = transform_matrix @ (bone.matrix.to_translation() - bone_data.head_local)
            rot0 = bone.matrix_channel.to_quaternion()
            rotation = mathutils.Quaternion((rot0.w, rot0.x, rot0.z, -rot0.y))
            scale0 = bone.matrix_channel.to_scale()
            scale = mathutils.Vector((scale0.x, scale0.z, scale0.y))
            for index, value in enumerate(translation):
                animations1[(bone.name, NAPR_LOC[index])].append(value)
            for index, value in enumerate(rotation):
                animations1[(bone.name, NAPR_ROT[index])].append(value)
            for index, value in enumerate(scale):
                animations1[(bone.name, NAPR_SCALE[index])].append(value)
    animations2 = {}
    dispatcher = []
    # Исключаем постоянные
    
    for (bone_name, napravlenie), curve in animations1.items():
        if (napravlenie in NAPR_LOC) or (napravlenie in NAPR_ROT[1:]):
            if max(map(abs, curve)) > EPSILON:
                dispatcher.append((bone_name, napravlenie))
                animations2[(bone_name, napravlenie)] = curve
        elif (napravlenie in NAPR_SCALE) or (napravlenie == NAPR_ROT[0]):
            if max(map(okolo_1, curve)) > EPSILON:
                dispatcher.append((bone_name, napravlenie))
                animations2[(bone_name, napravlenie)] = curve
    """
    for bone in arm.pose.bones:
        for stepen in range(10):
            dispatcher.append((bone.name, 2**stepen))
    """
    return (int(frameBegin), int(frameEnd)), dispatcher, dict(animations2)
        

    
def poluchit_animacii_skeleta(scene, arm, global_matrix):
    #transform_matrix = global_matrix * arm.matrix_world
    transform_matrix = global_matrix @ arm.matrix_world
    animations = {}
    if arm.animation_data and not arm.animation_data.nla_tracks:
        # write a single animation from the blender timeline
        action = arm.animation_data.action
        animations[action.name] = poluchit_dannye_animacii_pose(scene, 
                                                                arm,
                                                                bpy.context.scene.frame_start, 
                                                                bpy.context.scene.frame_end, 
                                                                transform_matrix)
    elif arm.animation_data:
        savedUseNla = arm.animation_data.use_nla
        savedAction = arm.animation_data.action
        arm.animation_data.use_nla = False
        if not len( arm.animation_data.nla_tracks ):
            Report.warnings.append('you must assign an NLA strip to armature (%s) that defines the start and end frames' %arm.name)

        actions = {}  # actions by name
        # the only thing NLA is used for is to gather the names of the actions
        # it doesn't matter if the actions are all in the same NLA track or in different tracks
        for nla in arm.animation_data.nla_tracks:        # NLA required, lone actions not supported
            print('NLA track:',  nla.name)

            for strip in nla.strips:
                action = strip.action
                actions[ action.name ] = action
                print('   strip name:', strip.name)
                print('   action name:', action.name)

        actionNames = sorted( actions.keys() )  # output actions in alphabetical order
        for actionName in actionNames:
            action = actions[ actionName ]
            arm.animation_data.action = action  # set as the current action
            animations[actionName] = poluchit_dannye_animacii_pose(scene, 
                                                                   arm, 
                                                                   action.frame_range[0], 
                                                                   action.frame_range[1], 
                                                                   transform_matrix)
            # restore suppressed bones
        # restore these to what they originally were
        arm.animation_data.action = savedAction
        arm.animation_data.use_nla = savedUseNla   
    return animations

    
def pereschet_animaciy(kosti, animacii1):
    kosti_dict = {}
    for ind, kost in enumerate(kosti):
        kosti_dict[kost] = ind
    animacii2 = {}
    for actionName, ((frameBegin, frameEnd), dispatcher1, action1) in animacii1.items():
        action2 = []
        dispatcher2 = []
        for kost, napravlenie in dispatcher1:
            #action2.append(action1[(kost, napravlenie)])
            dispatcher2.append((kosti_dict[kost], napravlenie))
        for kadr in range(frameEnd-frameBegin+1):
            sostojanie = []
            #action2.append([])
            for typ_krivoi in dispatcher1:
                sostojanie.append(action1[typ_krivoi][kadr])
            action2.append(sostojanie)
        animacii2[actionName] = (frameBegin, frameEnd), dispatcher2, action2
    return animacii2