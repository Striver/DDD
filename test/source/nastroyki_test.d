module nastroyki_test;

import my_utils.nastroyki_struct;
//import std.meta: AliasSeq;

alias Кортеж_Настроек1 = AliasSeq!( 
//  Тип             наименование    значение по-умолчанию
    string,         "общий_уровень","\"Информация\"",
    string,         "файл_журнала", "\"журнал.log\"",
    string[string], "уровни_модулей", "[\"nastroyki_test\":\"Информация\"]",
    string[],       "ресурсы",     "[\".\"]",
);

enum ИМЯ_ФАЙЛА_С_НАСТРОЙКАМИ = "настройки.cfg";

Настройки!Кортеж_Настроек1 настройки;

