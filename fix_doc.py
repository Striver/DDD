import sys, os,  re     #
#from bs4 import BeautifulSoup

def исправить_description(ddoc_description):
    return 
    
НОВЫЕ_ЗАГОЛОВКИ = {
    "<h4>Declaration</h4>":"",
    "<h4>Discussion</h4>":"",
    "<h4>Parameters</h4>":"<h4>Параметры:</h4>",
    "<h4>Note:</h4>":"<h4>Примечание:</h4>",
    '<span class="ddoc_section_h">Note:</span>':'<span class="ddoc_section_h">Замечание:</span>',
    "<h4>Return Value</h4>":"<h4>Возвращаемое значение:</h4>",
    "<h4>Examples</h4>":"<h4>Примеры:</h4>",
    "<h4>Throws</h4>":"<h4>Исключения:</h4>",

}

def исправить_документацию(имя_файла, изменить_имя_файла=False):
    текст = open(имя_файла, encoding="utf8").read()
    
    #Исправляем косяк вложенности тега <section> в тэг <p>. Браузеры такое не поддерживают
    reg = re.compile('(<section class="code_listing">.+?</section>)', re.DOTALL)
    соответсвия = reg.finditer(текст)
    границы = []
    for соответсвие in соответсвия:
         границы.append((соответсвие.start(), соответсвие.end()))
    границы.reverse()
    #print("границы = ", границы)
    for начало, конец in границы:
        текст3 = текст[конец:]
        if текст3.lstrip()[:2] == "</":
            continue
        текст1 = текст[:начало]
        текст2 = текст[начало:конец]
        текст = текст1 + "  </p>\n" + текст2 + '\n  <p class="para">' + текст3
    
    #Исправляем косяк со ссылками. Случается внутри косяка вложенности тега <section> в тэг <p>
    reg=re.compile(r'<a href="#<code class="code">(?P<объект>[\w_]+)</code>\.(?P<член>[\w_]+)">(?P<текст>[\w_]+)</a>')
    текст=reg.sub(r'<a href="#\g<объект>.\g<член>">\g<текст></a>',  текст)
    
    #Удаляем изменение размера шрифта в тексте
    текст=текст.replace("font-size: 1.4em;", "")
    
    #немного увеличиваем размер шрифта в тексте 
    #... к сожалению, так просто не прокатывает, убираю
    текст=текст.replace("font-size: 100%;", "font-size: 12px;") \
        .replace("font-size: 0.85em;", "font-size: 1em;")
    
    #Перевод заголовков на русский и удаление ненужных.
    for старый_заголовок, новый_заголовок in НОВЫЕ_ЗАГОЛОВКИ.items():
        текст = текст.replace(старый_заголовок, новый_заголовок)

    #Сохраняем результат
    if изменить_имя_файла:
        разделённое_имя_файла = os.path.split(имя_файла)   
        новое_имя_файла = разделённое_имя_файла[0] + os.sep + "_" + разделённое_имя_файла[1]
    else:
        новое_имя_файла = имя_файла
    open(новое_имя_файла, "w", encoding="utf8").write(текст)
    #print(новое_имя_файла)
    

def main():
    try:
        имя_каталога = sys.argv[1]
    except KeyError:
        print("введите каталог!")
        sys.exit(1)
    for имя_файла in os.listdir(имя_каталога):
        исправить_документацию(os.path.join(имя_каталога, имя_файла), изменить_имя_файла=False)
    
if __name__ == "__main__":
    main()

