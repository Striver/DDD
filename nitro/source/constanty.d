enum auto ConfigVars=  ["ConfigVarsD":[],
                        "ConfigVarsL":["eee", "window_width", "window_height", "fullscreen", "desktopmode",
							"tip_rendera", "vsync"],
                        "ConfigVarsS":["obchii_uroven", "window_name"],
                        "ConfigVarsAS":["dirresources"],
                        "ConfigVarsAL":[],
                        "ConfigVarsAD":[],
                        "ConfigVarsDictS":["moduluroven"]
                        ];

auto filenameconfig="vars_app.cfg";
enum logfilename="logapp.log"; 
enum dirresources_default=["res"];

enum 
    window_width_default=1280,
    window_height_default=768,
    flag_fullscreen_default=0,
    desktopmode_default=1,
	flag_vsync_default=0,
    window_name_default="Super-puper", 
    log_obchii_uroven_default="Warning";
    
enum string[string] log_moduluroven_default=["main":"Debug"];
enum string NamePustajaTextura="Pustaja_textura";