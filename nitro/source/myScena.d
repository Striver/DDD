﻿
import derelict3.sdl2.sdl; 
import  std.datetime, std.string;

import windowSDL2;
import scena, node, entity, mesh, vector, quaternion, nitromanager; 
import light, texture, event, overlay, overlayelement, overlaytextfield;
import logging;

enum    log_module="myScena";

enum filenamemesh="res\\meshes\\bibizjana2.me";
enum meshname="bibizjana2";
enum meshneboname="Nebo03";
enum meshzemljaname="Zemlja02";
enum delta=5.00;
enum KoefPovorota=-2.0; //-1.00;


class MyScena : Scena {
	
	Node node1, node0, node3, nodeCamera, nodeCamera0, nodeZemlja;
    float x, deltax, deltay, povorotx, povoroty;
	int mousex, mousey;
	long predAppTick;
	float ticksPerSecf, timeFrame, timeinsecond;
	int framesinsecond;
	Overlay ov, ov_stats;
	OverlaySDLTextField tf;
	//OverlayElement ovel2;
	

    this(NitroManager NM){
        super(NM);
        ColorBackgroud=[0.2,0.0,0.2,0.0];
        
        //auto me=new Mesh(filenamemesh);
        auto me=NM.getMesh(meshname);
        auto me2=NM.getMesh(meshneboname);
        auto meZemlja=NM.getMesh( meshzemljaname);
        log(log_module, LogUrovni.Info, "сформировали меш");
        auto ent1=new Entity("ent1", me2);
        node1= NM.mBackgroundBaseNode.CreateChild(
                "Node1",
         		new Vector3(0,0,0),
         		new Quaternion(0.0f,  new Vector3(1,0,0)));
        //node1.setPosition(0,0,0);
        //node1.setScale(5f,5f,5f);
        node1.attachObject(ent1);
        ent1.getMat().setDiffuse([0.9,0.0,0.2,1.0]);
        ent1.getMat().setSpecular([0.1,0.1,0.1,1.0]);
        ent1.getMat().setTexDuffuse(NM.getTexture("sky_midafternoon_2048"));
        
        nodeZemlja= NM.mBaseNode.CreateChild(
                "NodeZemlja",
         		new Vector3(0,0,0),
         		new Quaternion(0.0f,  new Vector3(1,0,0)));
        auto entZemlja=new Entity("Zemlja2", meZemlja);
        nodeZemlja.attachObject(entZemlja);
        entZemlja.getMat().setTexDuffuse(NM.getTexture("Zemlja2"));
        entZemlja.getMat().setAmbient([0.5,0.5,0.5,1.0]);
        //nodeZemlja.setScale(0.2f,0.2f,0.2f);
        nodeZemlja.setScale(5f,5f,5f);
		

		
        node0= NM.mBaseNode.CreateChild(
                "Node0",
         		new Vector3(0,0,0),
         		new Quaternion(0.0f,  new Vector3(1,0,0)));
				
        auto ent3=new Entity("ent3", me);
        node3= node0.CreateChild(
                "Node1",
         		new Vector3(2,0,0),
         		new Quaternion(0.0f,  new Vector3(1,0,0)));
        //node1.setPosition(0,0,0);
        //node1.setScale(0.5f,0.5f,0.5f);
        node3.attachObject(ent3);
        //ent3.getMat().setDiffuse([0.0,1.0,0.2,1.0]);
        ent3.getMat().setTexDuffuse(NM.getTexture("55555"));
		

        auto node2= node0.CreateChild(
                "Node2",
         		new Vector3(10,10,3),
         		new Quaternion(0.0f,  new Vector3(1,0,0)));
        //node0.setPosition(3,-1.5f,0);
        auto l0=new Light("light1", 0);
        node2.attachObject(l0);
        //l0.setDiffuse([0.5,0.5,0.5, 1.0]);
        l0.setDiffuse([1,1,1, 1.0]);
        l0.setSpecular([0.1,0.1,0.1, 1.0]);
		l0.setAmbient([ 0.1f, 0.1f, 0.1f, 1.0f ]);
        log(log_module, LogUrovni.Info, "сформировали свет0");
		
		/*
		for(int xx=-10; xx<10; xx+=5)
			for (int yy=-10; yy<10; yy+=5)
				for (int zz=-10; zz<10; zz+=5)
					newBibizjana(NM, xx, yy, zz);
		*/
		ov_stats=new Overlay("text1", NM);
		ov_stats.setKoord(TypeKoord.Relative, 0.5, 0.0, 0.5, 0.5);
		OverlaySDLTextField tf0= new OverlaySDLTextField("text1", false, 0,0.5,1,0.5);
		tf0.setText(NM, "Супер-пупер!", "LiberationSerif-Italic_50", [0.8,0,0.4] );
		ov_stats.attachElement(tf0);
		tf0.resize();
		tf= new OverlaySDLTextField("fps", false, 0,0.0,1,0.5);
		tf.setText(NM, "0", "LiberationSerif-Italic_50", [0.8,0,0.4] );
		ov_stats.attachElement(tf);
		tf.resize();
		NM.addOverlay(ov_stats);

		
		Overlay ov_textext=new Overlay("text2", NM);
		ov_textext.setKoord(TypeKoord.Relative, 0.5, 0.5, 0.5, 0.5);
		OverlayTextureTextField tf1= new OverlayTextureTextField("text2", 0, 0 ,0.5, 0.2);
		ov_textext.attachElement(tf1);
		//tf1.setText(NM, "ЩПупер - супер!", "DejaVuSansMono-BoldOblique_32");
		tf1.setText(NM, "ЩПупер - супер!", "LiberationSerif-Italic_32");
		NM.addOverlay(ov_textext);
		
		
		/*
		float texratio=tex1.getRatio();
		if (texratio>0) {
			OverlayElement ovel1;
			log(log_module, LogUrovni.Info, format("соотношение в тексте = %s", texratio));
			if (texratio>1)
				ovel1=new OverlayElement("text1", false, 0,0,1,1/texratio);
			else
				ovel1=new OverlayElement("text1", false, 0,0,texratio,1);
			ovel1.setTexture(tex1);
			ov2.attachElement(ovel1);
			NM.addOverlay(ov2);
		}
		*/
		
		ov=NM.getOverlay("overlay1");
		if (ov) NM.addOverlay(ov);
		
		nodeCamera=NM.mNodeCamera;
        x=0;
        deltax=0.0;
        deltay=0.0;
		mousex=-1;
		mousey=-1;
		povorotx=0;
		povoroty=0;
		timeFrame=0;
        //nodeCamera0= NM.mBaseNode.CreateChild(
        //        "NodeCamera0",
        // 		new Vector3(x,0, 1),
        // 		new Quaternion(0.0f,  new Vector3(1,0,0)));
		//nodeCamera0.addChild(NM.mBaseNode.removeChild("NodeCamera"));
        nodeCamera.setPosition(new Vector3(x,0, 1));
        RegisterEvents(NM);
		predAppTick=Clock.currAppTick().length;
        ticksPerSecf=TickDuration.ticksPerSec;
		timeinsecond=0;
		framesinsecond=0;
		WarpMouseInCenterWindow();
		//SDL_GetMouseState(&mousex, &mousey);
		//GetCenterWindow(mousex, mousey);
		//log(log_module, LogUrovni.Info, format("Координаты мыши mousex=%s, mousey=%s", mousex, mousey));
    } 
    
	void newBibizjana(NitroManager NM, float x, float y, float z) {
		auto me=NM.getMesh(meshname);
		string namesuffix=format("%s%s%s", x,y,z);
		auto ent=new Entity("ent" ~ namesuffix, me);
		auto nod= NM.mBaseNode.CreateChild(
                "Node" ~ namesuffix,
         		new Vector3(x,y,z),
         		new Quaternion(0.0f,  new Vector3(1,0,0)));
		nod.attachObject(ent);	
		ent.getMat().setTexDuffuse(NM.getTexture("bibizjana"));
	}
	
    void RegisterEvents(NitroManager NM) {
        ObrEvents ob;
        if (NM.ObrEvents_not_enabled()) {
            ob= new ObrEvents();
            NM.setObrEvents(ob);
        }
        else
            ob=NM.getObrEvents();
        ob.Register_Fun2(SDL_KEYDOWN, SDLK_RIGHT, &Ob_VpravoVlevo);
        ob.Register_Fun2(SDL_KEYDOWN, SDLK_LEFT, &Ob_VpravoVlevo);
        ob.Register_Fun2(SDL_KEYUP, SDLK_RIGHT, &Ob_VpravoVlevo);
        ob.Register_Fun2(SDL_KEYUP, SDLK_LEFT, &Ob_VpravoVlevo);
        ob.Register_Fun2(SDL_KEYDOWN, SDLK_UP, &Ob_VperedVazad);
        ob.Register_Fun2(SDL_KEYDOWN, SDLK_DOWN, &Ob_VperedVazad);
        ob.Register_Fun2(SDL_KEYUP, SDLK_UP, &Ob_VperedVazad);
        ob.Register_Fun2(SDL_KEYUP, SDLK_DOWN, &Ob_VperedVazad);
        ob.Register_Fun1(SDL_MOUSEMOTION, &Ob_MouseMotion);
        ob.Register_Fun2(SDL_KEYUP, SDLK_SPACE, &Ob_Overlay);
    }
    
	override public void Update(){
		long currAppTick=Clock.currAppTick().length;
		timeFrame=(currAppTick-predAppTick)/ticksPerSecf;
		predAppTick=currAppTick;
		//node1.Roll(0.1f);
        node0.Pitch(2.0f*timeFrame);
		node3.Pitch(1.0f*timeFrame);
		Vector3 transVector1= new Vector3(deltax*timeFrame, 0, -deltay*timeFrame);
		Vector3 transVector2=nodeCamera.getOrientation().mul(transVector1);
        nodeCamera.Translate(transVector2);
		updateStats(timeFrame);
		string timeframestr=format("%s",timeFrame);
		
        //nodeCamera0.Translate(transVector2);
		//nodeCamera.Roll(0.01f);
        /*
        x+=deltax;
        if (x>1.0 || x<-1.0) 
            deltax=-deltax;
        nodeCamera.Translate(deltax,0,0);
        float sc=1.0f+deltax;
        nodeCamera.Scale(sc,sc,sc);
        */
	}
    
    bool Ob_VpravoVlevo(SDL_Event e) {
        log(log_module, LogUrovni.Debug, "Двигаем камеру вправо-влево");
		deltax=0;
		auto state= SDL_GetKeyboardState(null);
		if (state[SDL_SCANCODE_RIGHT])
			deltax+=delta;
		if (state[SDL_SCANCODE_LEFT])
			deltax-=delta;
        return false;
    }
	
    bool Ob_VperedVazad(SDL_Event e) {
        log(log_module, LogUrovni.Debug, "Двигаем камеру вперёд-назад");
		deltay=0;
		auto state= SDL_GetKeyboardState(null);
		if (state[SDL_SCANCODE_UP])
			deltay+=delta;
		if (state[SDL_SCANCODE_DOWN])
			deltay-=delta;
        return false;
    }

	bool Ob_MouseMotion(SDL_Event e) {
		//log(log_module, LogUrovni.Debug, format("Координаты мыши Event x=%s, y=%s", e.motion.x, e.motion.y));
		//if (mousex<0 || mousey<0) {
		//	WarpMouseInCenterWindow();
		//	SDL_GetMouseState(&mousex, &mousey);
		//	log(log_module, LogUrovni.Debug, format("Координаты мыши mousex=%s, mousey=%s", mousex, mousey));
		//	return false;
		//}
		SDL_GetRelativeMouseState(&mousex, &mousey);
		log(log_module, LogUrovni.Debug, format("Relative Координаты мыши x=%s, y=%s", mousex, mousey));
		povorotx=KoefPovorota*timeFrame*mousex; //(e.motion.x); // - mousex);
		povoroty=KoefPovorota*timeFrame*mousey; //(e.motion.y); // - mousey);
        log(log_module, LogUrovni.Debug, format("Поворачиваем камеру, timeFrame=%s,
			povorotx=%s, povoroty=%s", timeFrame, povorotx,  povoroty));
		//WarpMouseInCenterWindow();
		//SDL_GetMouseState(&mousex, &mousey);
		mousex=e.motion.x;
		mousey=e.motion.y;
		nodeCamera.Yaw(povorotx);
		nodeCamera.Pitch(povoroty);
		return false;
	}
	
    bool Ob_Overlay(SDL_Event e) {
		ov.flipVisibility();
		return false;
	}
	
	void updateStats(float timeFrame) {
		
		enum timeupdate=1.0; // Обновлять 1 раз в timeupdate секунд
	
		framesinsecond++;
		timeinsecond+=timeFrame;
		if (timeinsecond>=timeupdate) {
			timeinsecond=0;
			if (ov_stats.getVisible()) {
				tf.setText(NM, format("Текущее FPS: %s",framesinsecond), "LiberationSerif-Italic_50", [0.8,0,0.4] );
				tf.resize();
			}
			framesinsecond=0;
		}
	}
}