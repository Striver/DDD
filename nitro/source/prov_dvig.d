import std.string;
import vector, matrix, quaternion;
import logging;


enum    log_module="prov_dvig";

void poverka1() {
    Vector3 vector=new Vector3(0,1,2);
    log(log_module, LogUrovni.Info, format("vector=%s", vector));
    Quaternion quat=new Quaternion([1,0.5,0.7,-0.4]);
    log(log_module, LogUrovni.Info, format("quat=%s",  quat));
    Matrix4 mat=new Matrix4();
    log(log_module, LogUrovni.Info, format("mat1=%s",  mat));
    mat.makeTransform(vector, Vector3.UNIT_Y, quat);
    log(log_module, LogUrovni.Info, format("mat2=%s",  mat));
}