﻿import std.stdio, std.conv, std.string;
//import derelict.opengl.gl, derelict3.sdl2.sdl; 
//import derelict.opengl.gl, derelict.sdl.sdl; 
import constanty, logging, loadconfig;

import risovanie, windowSDL2, renderer, resources, myScena, nitromanager;    //prov_dvig,  

enum    log_module="main";
    

void main()
{
    readcfg(filenameconfig);
    //logging.loginitstr(logfilename, configvarss["obchii_uroven"], configvarsdicts["moduluroven"]);
    loginit();
    if (errorvars.length)
        log(log_module, LogUrovni.Warning, format("Незагруженные переменные конфигурации: %s" , errorvars)); 
        
    //auto res=new Resources(configvarsas.get("dirresources", dirresources_default));
    
    NitroManager NM=new NitroManager();
    NM.setScena(new MyScena(NM));
    NM.rend.render();
    
}

void loginit() {
    string obchii_uroven = configvarss.get("obchii_uroven", log_obchii_uroven_default);
    string[string] moduluroven = configvarsdicts.get("moduluroven", log_moduluroven_default);
    logging.loginitstr(logfilename, obchii_uroven, moduluroven);
}

/*
int[string] LoadConfig(){
    int[string] ret = [ "window_width":     window_width, 
                        "window_height":    window_height, 
                        "flag_fullscreen":  flag_fullscreen ];
                        //"window_name":      window_name];
    return ret;
}
*/

//Наличие этой функции предотвращает ошибку линковки,
//хотя сама эта функция не вызывается.
void fun_fake() {
    LogUrovni[string] ModulUroven =["main":LogUrovni.Debug];
    logging.loginit(logfilename, LogUrovni.Warning, ModulUroven);
}
