module camera;

import std.string;
import derelict.opengl.gl;

import movableobject, matrix, quaternion, vector;
private import logging;

enum  log_module="camera";


class Camera : MovableObject {
	
	private Matrix4 mat;

	override void draw(){};
	
	public this(string name) {
		super(name);
		mat=new Matrix4();
	}	

	void setRotateCamera(bool update) {
		if (update) 
			UpdateMatrix();
        //Matrix3 matrot=mat.extract3x3Matrix();
		Matrix4 matrot=new Matrix4(mat.extract3x3Matrix());
        //matrot.makeTransform(Vector3.UNIT_ZERO, Vector3.UNIT_SCALE, quat);
		glLoadMatrixf(&(matrot.m[0]));
		
	}
	
	void setCamera(bool update) {
        // Текущее положение в матричном виде
		if (update) 
			UpdateMatrix();
        log(log_module, LogUrovni.Debug, format("Матрица камеры %s", mat.toString()));
		glLoadMatrixf(&(mat.m[0]));
		
	}
	
	private void UpdateMatrix() {
		mat=new Matrix4(mParent.getDerivedMatrix());
		mat=mat.inverse();
	}
}
