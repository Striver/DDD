import derelict.opengl.gl;
import std.file, std.string, std.conv, std.math;
private import logging;

enum    log_module="node";

import quaternion, vector, matrix, movableobject;


public class Node {
	public enum float RadtoGrad=to!(float)(180.0/PI);
	
	private Node mParent;
    /// Collection of pointers to direct children; hashmap for efficiency
	private Node[string] mChildren;
    
	private MovableObject[string] mObjects;
    
    /// Friendly name of this node, can be automatically generated if you don't care
	private string mName;
    
    /// Stores the orientation of the node relative to it's parent.
	private Quaternion mOrientation;
    /// Stores the position/translation of the node relative to its parent.
	private Vector3 mPosition;
    /// Stores the scaling factor applied to this node
	private Vector3 mScale;
	private bool mEnabled=true; 
	
	private Matrix4 mCachedTransform;
	private bool mCachedTransformOutOfDate=true;

    private static Node[string] PustoymChildren;
    private static MovableObject[string] PustoymObjects;
    
    public this(string name){
    	mName=name;
    	mPosition=new Vector3();
    	mScale=new Vector3(Vector3.UNIT_SCALE);
    	mOrientation=new Quaternion();
    	//mChildren=new HashMap<String, Node>();
    	//mObjects=new HashMap<String, MovableObject>();
    	mParent=null;
    	mCachedTransform=new Matrix4();
    }
    
    public void setParent(Node parentNode){
    	mParent=parentNode;
    }

    public string getName(){
    	return mName;
    }
    
    public Node getParent(){
    	return mParent;
    }
    
    public Quaternion getOrientation(){
    	return mOrientation;
    }
    
    public void setOrientation(const(Quaternion) q){
    	mOrientation=new Quaternion(q);
    	mOrientation.normalise();
    	mCachedTransformOutOfDate=true;
    }
    
    public void resetOrientation(){
    	mOrientation=new Quaternion();
    	mCachedTransformOutOfDate=true;
    }
    
    public void setPosition(const(Vector3) pos){
    	mPosition=new Vector3(pos);
    	mCachedTransformOutOfDate=true;
    }
    
    public void setPosition(float x, float y, float z){
    	mPosition=new Vector3(x,y,z);
    	mCachedTransformOutOfDate=true;
    }
    
    public Vector3 getPosition(){
    	return mPosition;
    }
    
    public void setScale(const(Vector3) scl){
    	mScale=new Vector3(scl);
    	mCachedTransformOutOfDate=true;
    }
    
    public void setScale(float x, float y, float z){
    	mScale=new Vector3(x,y,z);
    	mCachedTransformOutOfDate=true;
    }
    
    public Vector3 getScale(){
    	return mScale;
    }

    public void Scale(const(Vector3) pos){
    	mScale=mScale.mul(pos);
    	mCachedTransformOutOfDate=true;
    }

    public void Scale(float x, float y, float z){
    	mScale=new Vector3(mScale.x*x,mScale.y*y,mScale.z*z);
    	mCachedTransformOutOfDate=true;
    }
    
    public void Translate(const(Vector3) d){
    	mPosition=mPosition.plus(d);
    	mCachedTransformOutOfDate=true;
    }
    
    public void Translate(float x, float y, float z){
    	mPosition=new Vector3(mPosition.x+x,mPosition.y+y,mPosition.z+z);
    	mCachedTransformOutOfDate=true;
    }
    
    public void Roll(float angle)
    {
        Rotate(Vector3.UNIT_Z, angle);
    }
    //-----------------------------------------------------------------------
    public void Pitch(float angle)
    {
        Rotate(Vector3.UNIT_X, angle);
    }
    //-----------------------------------------------------------------------
    public void Yaw(float angle)
    {
        Rotate(Vector3.UNIT_Y, angle);

    }
    
    public void Rotate(const(Vector3) axis, float angle)
    {
        Quaternion q=new Quaternion();
        q.FromAngleAxis(angle,axis);
        Rotate(q);
    }
    
    public void Rotate(const(Quaternion) q){
    	Quaternion qnorm = new Quaternion(q);
		qnorm.normalise();
		//System.out.println("qnorm="+qnorm.toString());
    	mOrientation=mOrientation.mul(qnorm);
		//System.out.println("mOrientation="+mOrientation.toString());
    	mCachedTransformOutOfDate=true;
    }

    public Node CreateChild(string name, const(Vector3) translate, const(Quaternion) rotate){
    	Node newNode = new Node(name);
        newNode.Translate(translate);
        newNode.Rotate(rotate);
        addChild(newNode);
        return newNode;
    }
    
    public void addChild(Node child){
        if (child.mParent !is null) return;
        mChildren[child.getName]=child;
        child.setParent(this);
    }
    
    public short numChildren(){
    	return to!(short) (mChildren.length);
    }
    
    public Node getChild(string name){
    	return mChildren[name];
    }
    
    public Node removeChild(Node child){
    	return removeChild(child.getName());
    }
    
    public Node removeChild(string childname){
        try {
          	Node ret=mChildren[childname];
            mChildren.remove(childname);
            ret.setParent(null);
            return ret;
        }
        catch
            return null;
    }
    
    public void removeAllChildren(){
        foreach (name, node; mChildren) 
            node.setParent(null);
        mChildren=PustoymChildren.dup;    
    }
    
    protected Matrix4 _getFullTransform() {
        if (mCachedTransformOutOfDate)
        {
            this.mCachedTransform.makeTransform(
            		mPosition,
            		mScale,
            		mOrientation);
            mCachedTransformOutOfDate = false;
        }
        log(log_module, LogUrovni.Debug, format("getFullTransform %s %s", this.toString(), this.mCachedTransform.toString()));
        return mCachedTransform;
    }
    
    public void setVisible(bool enabled){
    	mEnabled=enabled;
    }

    public bool getVisible(){
    	return mEnabled;
    }

    public void flipVisibility(){
    	mEnabled= ! mEnabled;
    }
    
    public void attachObject(MovableObject mo){
        if (mo.mParent!is null) return;
        mObjects[mo.getName]= mo;
        mo.setParent(this);
    }
    
    public short numObjects(){
    	return to!(short) (mObjects.length);
    }
    
    public MovableObject getObject(string name){
    	return mObjects[name];
    }
    
    public MovableObject detachObject(MovableObject mo){
    	return detachObject(mo.getName());
    }
    
    public MovableObject detachObject(string objectname){
        if (objectname in mObjects) {
            MovableObject ret=mObjects[objectname];
            mObjects.remove(objectname);
            ret.setParent(null);
            return ret;
        }
        else
            return null;
    }
    
    public void detachAllObjects(){
        foreach (string name, MovableObject mo; mObjects) 
            mo.setParent(null);
        mObjects=PustoymObjects.dup;    
    }
    
    public void draw(){  
    	if (mEnabled){
    		glPushMatrix();
    		
    		glMultMatrixf(&(_getFullTransform().m[0]));
    		//gl.glTranslatef(mPosition.x, mPosition.y, mPosition.z);
    		//gl.glScalef(mScale.x, mScale.y, mScale.z);
    		//float rot[]=mOrientation.ToAngleAxis();
    		//System.out.println("rot="+rot[0]+" "+rot[1]+" "+rot[2]+" "+rot[3]);
    		//gl.glRotatef (rot[0]*RadtoGrad, rot[1], rot[2], rot[3]);
    		
    		//System.out.println("draw Node "+mName);
    		
            log(log_module, LogUrovni.Debug, format("draw %s", this.toString()));
            
    		foreach (mo; mObjects.byValue()) 
                mo.draw();
    		foreach (child; mChildren.byValue()) 
                child.draw();
			glPopMatrix();
    	}
    }
    
    public int Graney(){
    	if (!mEnabled) return 0;
    	int ret=0;
		//Set<String> keys = mObjects.keySet();
		foreach (mo; mObjects.byValue()) 
			ret+=mo.Graney();
		//keys = mChildren.keySet();
		foreach (child; mChildren.byValue()) 
			ret+=child.Graney();
	    return ret;
    }
    
    public Matrix4 getDerivedMatrix(){
    	if (mParent is null)
    		return _getFullTransform();
    	else
    		//return _getFullTransform().mul(mParent.getDerivedMatrix());
			return mParent.getDerivedMatrix().mul(_getFullTransform());
    }
    
    const public override string toString() {
        string enbl = mEnabled ? "Enabled" : "Disabled";
        return "Node(" ~ mName ~ ": " ~ enbl ~ " pos=" ~ mPosition.toString() ~ 
			" orientation=" ~ mOrientation.toString() ~ " scale=" ~ mScale.toString() ~ ")";
    }

}
