package com.striver.opengl1;

import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;

/**
 * Render a pair of tumbling cubes.
 */

class Renderer implements GLSurfaceView.Renderer {

	public final int NUMTEXTURES=5;
	public final int meshresources[]={R.raw.bibizjana3};

	public int[] textures = new int[NUMTEXTURES];

	//private Vector<Mesh> Meshi=new Vector<Mesh>();
    private float mAngle;
    private Activity context;
    private SceneManager SM;
    private Scena scena;

    private float[] lightAmbient = { 0.0f, 0.0f, 0.0f, 1.0f };
    private float[] lightDiffuse = { 1.0f, 0.50f, 0.50f, 1.0f };
    private float[] lightSpecular = { 1.0f, 1.0f, 1.0f, 1.0f };
    private float[] lightPosition = { 0.0f, -2.0f, -4.0f, 1.0f };
    private float[] lightAmbient1 = { 0.1f, 0.1f, 0.1f, 1.0f };
    private float[] lightDiffuse1 = { 0.5f, 0.5f, 1.00f, 1.0f };
    private float[] lightPosition1 = { 0.0f, 2.0f, -2.0f, 1.0f };
    
    private float ColorBackgroud=0.3f;
    
	
    public Renderer(Activity activity) {
        this.context=activity;
        SM=new SceneManager((Context) context);
        SM.LoadMeshes(meshresources);
        //mesh1=SM.mMeshi.get(meshresources[0]);
        
        
    }

    public void onDrawFrame1(GL10 gl) {
        /*
         * Usually, the first thing one might want to do is to clear
         * the screen. The most efficient way of doing this is to use
         * glClear().
         */

        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

        /*
         * Now we're ready to draw some 3D objects
         */

        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glTranslatef(0, 0, -4.0f);
        gl.glRotatef(mAngle,        0, 1, 0);
        gl.glRotatef(mAngle*0.25f,  1, 0, 0);

		//gl.glFrontFace(GL10.GL_CCW);
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);

        //mCube.draw(gl);

        gl.glRotatef(mAngle*2.0f, 0, 1, 1);
        gl.glTranslatef(0.5f, 0.5f, 0.5f);

		//gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0]);
		//mesh1.draw(gl);
        //ent1.draw(gl);

        mAngle += 1.2f;
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		//gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glDisableClientState(GL10.GL_NORMAL_ARRAY);

    }
    
    public void onDrawFrame(GL10 gl) {
        /*
         * Usually, the first thing one might want to do is to clear
         * the screen. The most efficient way of doing this is to use
         * glClear().
         */
    	
    	scena.Update();

        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

        /*
         * Now we're ready to draw some 3D objects
         */

        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
        
        gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);

		//gl.glFrontFace(GL10.GL_CCW);
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
        gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);
        
        gl.glMaterialfv(GL10.GL_FRONT, GL10.GL_SPECULAR,lightSpecular,0);
        gl.glMaterialf(GL10.GL_FRONT, GL10.GL_SHININESS,20.0f);
        

        SM.mBaseNode.draw(gl);

		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
		gl.glDisableClientState(GL10.GL_NORMAL_ARRAY);

    }
    public void onSurfaceChanged(GL10 gl, int width, int height) {
         gl.glViewport(0, 0, width, height);

         /*
          * Set our projection matrix. This doesn't have to be done
          * each time we draw, but usually a new projection needs to
          * be set when the viewport is resized.
          */

         float ratio = (float) width / height;
         gl.glMatrixMode(GL10.GL_PROJECTION);
         gl.glLoadIdentity();
         gl.glFrustumf(-ratio, ratio, -1, 1, 1, 10);
    }

    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        /*
         * By default, OpenGL enables features that improve quality
         * but reduce performance. One might want to tweak that
         * especially on software renderer.
         */
        gl.glDisable(GL10.GL_DITHER);

        /*
         * Some one-time OpenGL initialization can be made here
         * probably based on features of this particular context
         */
         gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT,
                 GL10.GL_FASTEST);

         gl.glClearColor(ColorBackgroud,ColorBackgroud,ColorBackgroud,0);
         
         //gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_AMBIENT, lightAmbient,0);
         //gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_DIFFUSE, lightDiffuse,0);
         //gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_SPECULAR, lightSpecular,0);
         //gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, lightPosition,0);
         //gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_AMBIENT, lightAmbient1,0);
         //gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_DIFFUSE, lightDiffuse1,0);
         ///gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_SPECULAR, lightSpecular,0);
         //gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_POSITION, lightPosition1,0);
         gl.glEnable(GL10.GL_LIGHTING);
         //gl.glEnable(GL10.GL_LIGHT0); 
         //gl.glEnable(GL10.GL_LIGHT1); 
         gl.glLightModelfv(GL10.GL_LIGHT_MODEL_AMBIENT, lightAmbient1, 0);
         
         
         //gl.glLightModelf(GL10.GL_LIGHT_MODEL_TWO_SIDE, GL10.GL_TRUE);
         gl.glEnable(GL10.GL_CULL_FACE);
         gl.glShadeModel(GL10.GL_SMOOTH);
         gl.glEnable(GL10.GL_DEPTH_TEST);
         
 		 gl.glEnable(GL10.GL_TEXTURE_2D);			//Enable Texture Mapping ( NEW )
         loadGLTextures(gl, (Context) context);

         scena=new Scena(SM, this);

        
    }
    
	public void loadGLTexture(GL10 gl, Context context, int numtextury, int resource) {
		//Get the texture from the Android resource directory
		InputStream is = context.getResources().openRawResource(resource);
		Bitmap bitmap = null;
		try {
			//BitmapFactory is an Android graphics utility for images
			bitmap = BitmapFactory.decodeStream(is);

		} finally {
			//Always clear and close
			try {
				is.close();
				is = null;
			} catch (IOException e) {
			}
		}

		//������������ ��� �������� 
		//...and bind it to our array
		gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[numtextury]);
		
		//Create Nearest Filtered Texture
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

		//Different possible texture parameters, e.g. GL10.GL_CLAMP_TO_EDGE
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT);
		gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT);
		
		//Use the Android GLUtils to specify a two-dimensional texture image from our bitmap
		GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
		
		//Clean up
		bitmap.recycle();
	}
	
	public void loadGLTextures(GL10 gl, Context context) {
		gl.glGenTextures(NUMTEXTURES, textures, 0);
		loadGLTexture(gl, context, 0, R.drawable.bibizjana);
		loadGLTexture(gl, context, 1, R.drawable.cube_tex);
		loadGLTexture(gl, context, 2, R.drawable.prozrachnost1);
		loadGLTexture(gl, context, 3, R.drawable.prozrachnost2);
		loadGLTexture(gl, context, 4, R.drawable.prozrachnost3);
	}

	
}
