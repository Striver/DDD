package com.striver.opengl1;

import javax.microedition.khronos.opengles.GL10;

public class Entity2 extends Entity {

	public Entity2(String name, Mesh me) {
		super(name, me);
		// TODO Auto-generated constructor stub
	}

	public void draw(GL10 gl){
		gl.glPushMatrix();
        gl.glLoadMatrixf(mParent.getDerivedMatrix().m,0);
		
		super.draw(gl);
		//System.out.println("draw Entity2 "+mName);
		gl.glPopMatrix();
		
	}

}
