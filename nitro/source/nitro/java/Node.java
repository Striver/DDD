package com.striver.opengl1;

import java.util.HashMap;
import java.util.Set;

import javax.microedition.khronos.opengles.GL10;

public class Node {
	public final float RadtoGrad=(float)(180.0/Math.PI);
	
	private Node mParent;
    /// Collection of pointers to direct children; hashmap for efficiency
	private HashMap<String, Node> mChildren;
    
	private HashMap<String, MovableObject> mObjects;
    
    /// Friendly name of this node, can be automatically generated if you don't care
	private String mName;
    
    /// Stores the orientation of the node relative to it's parent.
	private Quaternion mOrientation;
    /// Stores the position/translation of the node relative to its parent.
	private Vector3 mPosition;
    /// Stores the scaling factor applied to this node
	private Vector3 mScale;
	private boolean mEnabled=true; 
	
	private Matrix4 mCachedTransform;
	private boolean mCachedTransformOutOfDate=true;
    
    public Node(String name){
    	mName=name;
    	mPosition=new Vector3();
    	mScale=new Vector3(Vector3.UNIT_SCALE);
    	mOrientation=new Quaternion();
    	mChildren=new HashMap<String, Node>();
    	mObjects=new HashMap<String, MovableObject>();
    	mParent=null;
    	mCachedTransform=new Matrix4();
    }
    
    public void setParent(Node parentNode){
    	mParent=parentNode;
    }

    public String getName(){
    	return mName;
    }
    
    public Node getParent(){
    	return mParent;
    }
    
    public Quaternion getOrientation(){
    	return mOrientation;
    }
    
    public void setOrientation(Quaternion q){
    	mOrientation=q;
    	mOrientation.normalise();
    	mCachedTransformOutOfDate=true;
    }
    
    public void resetOrientation(){
    	mOrientation=new Quaternion();
    	mCachedTransformOutOfDate=true;
    }
    
    public void setPosition(Vector3 pos){
    	mPosition=pos;
    	mCachedTransformOutOfDate=true;
    }
    
    public void setPosition(float x, float y, float z){
    	mPosition=new Vector3(x,y,z);
    	mCachedTransformOutOfDate=true;
    }
    
    public Vector3 getPosition(){
    	return mPosition;
    }
    
    public void setScale(Vector3 pos){
    	mScale=pos;
    	mCachedTransformOutOfDate=true;
    }
    
    public void setScale(float x, float y, float z){
    	mScale=new Vector3(x,y,z);
    	mCachedTransformOutOfDate=true;
    }
    
    public Vector3 getScale(){
    	return mScale;
    }

    public void Scale(Vector3 pos){
    	mScale=mScale.mul(pos);
    	mCachedTransformOutOfDate=true;
    }

    public void Scale(float x, float y, float z){
    	mScale=new Vector3(mScale.x*x,mScale.y*y,mScale.z*z);
    	mCachedTransformOutOfDate=true;
    }
    
    public void Translate(Vector3 d){
    	mPosition=mPosition.plus(d);
    	mCachedTransformOutOfDate=true;
    }
    
    public void Translate(float x, float y, float z){
    	mPosition=new Vector3(mScale.x+x,mScale.y+y,mScale.z+z);
    	mCachedTransformOutOfDate=true;
    }
    
    public void Roll(float angle)
    {
        Rotate(Vector3.UNIT_Z, angle);
    }
    //-----------------------------------------------------------------------
    public void Pitch(float angle)
    {
        Rotate(Vector3.UNIT_X, angle);
    }
    //-----------------------------------------------------------------------
    public void Yaw(float angle)
    {
        Rotate(Vector3.UNIT_Y, angle);

    }
    
    public void Rotate(Vector3 axis, float angle)
    {
        Quaternion q=new Quaternion();
        q.FromAngleAxis(angle,axis);
        Rotate(q);
    }
    
    public void Rotate(Quaternion q){
    	Quaternion qnorm = new Quaternion(q);
		qnorm.normalise();
		//System.out.println("qnorm="+qnorm.toString());
    	mOrientation=mOrientation.mul(qnorm);
		//System.out.println("mOrientation="+mOrientation.toString());
    	mCachedTransformOutOfDate=true;
    }

    public Node CreateChild(String name, Vector3 translate, Quaternion rotate){
    	Node newNode = new Node(name);
        newNode.Translate(translate);
        newNode.Rotate(rotate);
        addChild(newNode);
        return newNode;
    }
    
    public void addChild(Node child){
        if (child.mParent!=null) return;
        mChildren.put(child.getName(), child);
        child.setParent(this);
    }
    
    public short numChildren(){
    	return (short) mChildren.size();
    }
    
    public Node getChild(String name){
    	return mChildren.get(name);
    }
    
    public Node removeChild(Node child){
    	return removeChild(child.getName());
    }
    
    public Node removeChild(String childname){
    	Node ret=mChildren.remove(childname);
    	if (ret!=null) ret.setParent(null);
    	return ret;
    }
    
    public void removeAllChildren(){
    	Set<String> keys = mChildren.keySet();
    	for (String childname:keys){
    		mChildren.get(childname).setParent(null);
    	}
    	mChildren.clear();
    }
    
    protected Matrix4 _getFullTransform() {
        if (mCachedTransformOutOfDate)
        {
            mCachedTransform.makeTransform(
            		mPosition,
            		mScale,
            		mOrientation);
            mCachedTransformOutOfDate = false;
        }
        return mCachedTransform;
    }
    
    public void setVisible(boolean enabled){
    	mEnabled=enabled;
    }

    public boolean getVisible(){
    	return mEnabled;
    }

    public void flipVisibility(){
    	mEnabled= ! mEnabled;
    }
    
    public void attachObject(MovableObject mo){
        if (mo.mParent!=null) return;
        mObjects.put(mo.getName(), mo);
        mo.setParent(this);
    }
    
    public short numObjects(){
    	return (short) mObjects.size();
    }
    
    public MovableObject getObject(String name){
    	return mObjects.get(name);
    }
    
    public MovableObject detachObject(MovableObject mo){
    	return detachObject(mo.getName());
    }
    
    public MovableObject detachObject(String objectname){
    	MovableObject ret=mObjects.remove(objectname);
    	if (ret!=null) ret.setParent(null);
    	return ret;
    }
    
    public void detachAllObjects(){
    	Set<String> keys = mObjects.keySet();
    	for (String objectname:keys){
    		mObjects.get(objectname).setParent(null);
    	}
    	mObjects.clear();
    }
    
    public void draw(GL10 gl){  
    	if (mEnabled){
    		gl.glPushMatrix();
    		
    		gl.glMultMatrixf(_getFullTransform().m, 0);
    		//gl.glTranslatef(mPosition.x, mPosition.y, mPosition.z);
    		//gl.glScalef(mScale.x, mScale.y, mScale.z);
    		//float rot[]=mOrientation.ToAngleAxis();
    		//System.out.println("rot="+rot[0]+" "+rot[1]+" "+rot[2]+" "+rot[3]);
    		//gl.glRotatef (rot[0]*RadtoGrad, rot[1], rot[2], rot[3]);
    		
    		//System.out.println("draw Node "+mName);
    		
    		Set<String> keys = mObjects.keySet();
    		for (String objectname:keys) mObjects.get(objectname).draw(gl);
    		keys = mChildren.keySet();
    	    for (String childname:keys) mChildren.get(childname).draw(gl);
			gl.glPopMatrix();
    	}
    }
    
    public int Graney(){
    	if (!mEnabled) return 0;
    	int ret=0;
		Set<String> keys = mObjects.keySet();
		for (String objectname:keys) 
			ret+=mObjects.get(objectname).Graney();
		keys = mChildren.keySet();
	    for (String childname:keys) 
	    	ret+=mChildren.get(childname).Graney();
	    return ret;
    }
    
    public Matrix4 getDerivedMatrix(){
    	if (mParent==null)
    		return _getFullTransform();
    	else
    		//return _getFullTransform().mul(mParent.getDerivedMatrix());
			return mParent.getDerivedMatrix().mul(_getFullTransform());
    }
    

}
