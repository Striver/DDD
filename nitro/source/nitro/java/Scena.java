package com.striver.opengl1;

public class Scena {

    //public Mesh mesh1;
    //public Entity ent1;
    public Node node0,node1,node2,node3,node4,node5;
	
	public Scena(SceneManager SM, Renderer Rend){
		Entity ent;
        node0= SM.mBaseNode.CreateChild(
         		"Node0",
         		new Vector3(0,0,-4),
         		new Quaternion(0.0f,  new Vector3(1,0,0)));
        node1= SM.mBaseNode.CreateChild(
         		"Node1",
         		new Vector3(0,0,-4),
         		new Quaternion(0.0f,  new Vector3(1,0,0)));
        ent=SM.createEntity("Ent1", Rend.meshresources[0], Rend.textures[0]);
        if (ent!=null){
        	 node1.attachObject(ent);
     		 System.out.println("attach Entity "+ent.mName);
         }
         
         node2= node1.CreateChild(
          		"Node2",
          		new Vector3(2,0,0),
          		new Quaternion(0.0f,  new Vector3(0,1.0f,0)));
         node2.Scale(0.3f, 0.3f, 0.5f);
         ent=SM.createEntity("Ent2", Rend.meshresources[0], Rend.textures[2]);
         if (ent!=null){
        	 node2.attachObject(ent);
     		 System.out.println("attach Entity "+ent.mName);
         }
         node3= node1.CreateChild(
           		"Node3",
           		new Vector3(-2,0,0),
           		new Quaternion(0.0f,  new Vector3(0,1.0f,0)));
          node3.Scale(0.3f, 0.3f, 0.5f);
          ent=SM.createEntity("Ent3", Rend.meshresources[0], Rend.textures[3]);
          if (ent!=null){
         	 node3.attachObject(ent);
      		 System.out.println("attach Entity "+ent.mName);
          }
          node4= node1.CreateChild(
             		"Node4",
             		new Vector3(1,2,0),
             		new Quaternion(0.0f,  new Vector3(0,1.0f,0)));
            node4.Scale(0.3f, 0.3f, 0.5f);
    		Mesh mesh=SM.getMesh(Rend.meshresources[0]);
    		if (mesh!=null){
    			ent=new Entity2("Ent4", mesh);
    			ent.setTexture(Rend.textures[4]);
    			node4.attachObject(ent);
    			System.out.println("attach Entity2 "+ent.mName);
    		}

    		node5=node0.CreateChild(
    				"Node5", 
    				new Vector3(1,1.5f,0),
    				new Quaternion());
    		Light lt1=SM.createLight("Light0");
    		if (lt1!=null)
    			node5.attachObject(lt1);
            //ent=SM.createEntity("Ent4", Rend.meshresources[0], Rend.textures[4]);
            //if (ent!=null){
           	// node4.attachObject(ent);
        	//	 System.out.println("attach Entity "+ent.mName);
            //}
	}
	
	public void Update(){
        node0.Yaw(0.2f);
        node2.Pitch(0.1f);
        node3.Yaw(0.2f);
        //node4.Roll(0.3f);
	}
}
