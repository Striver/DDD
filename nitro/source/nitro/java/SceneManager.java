package com.striver.opengl1;

import java.io.IOException;
import java.util.HashMap;
//import java.util.Vector;

import android.content.Context;
import android.content.res.Resources.NotFoundException;

public class SceneManager {
	// ��������� �����
	// ���� ����
	public final int MAXLIGHTS=4;
	public Light ligths[]={null,null,null,null,null,null,null,null}; 
	
	// ����
	public HashMap<Integer, Mesh> mMeshi;
	
	// ����������� ���� �����
	public Node mBaseNode;
	private Context context;
	
	
	
	public SceneManager(Context context){
		mBaseNode=new Node("BaseNode");
		mMeshi=new HashMap<Integer, Mesh>();
		this.context=context;
	}
	
	public void LoadMeshes(int[] resources){
		for (int resource:resources){
	        try {
	        	mMeshi.put(resource, new Mesh(context, resource));
			} catch (NotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public Mesh getMesh(int resource){
		if (mMeshi.containsKey(resource)) 
			return mMeshi.get(resource);
		else 
			return null;
	}
	
	public Entity createEntity(String name, int meshresource, int texture){
		Mesh mesh=this.getMesh(meshresource);
		if (mesh!=null){
			Entity ent=new Entity(name, mesh);
			ent.setTexture(texture);
			return ent;
		}
		else return null;
	}
	
	private int getNewNumLight(){
		for (int i=0; i<MAXLIGHTS;i++)
			if (ligths[i]==null){
				return i;
			}
		return -1;
	}
	
	public Light createLight(String name){
		int num=getNewNumLight();
		if (num<0) return null;
		ligths[num]=new Light(name, num);
		return ligths[num];
	}
	public void freeLight(int num){
		if (num<MAXLIGHTS){
			
			ligths[num]=null;
		}
	}

}
