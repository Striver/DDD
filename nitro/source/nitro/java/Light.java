package com.striver.opengl1;

import javax.microedition.khronos.opengles.GL10;

public class Light extends MovableObject {

	public static final int GLLIGHTS[]=
		{GL10.GL_LIGHT0,
		 GL10.GL_LIGHT1,
		 GL10.GL_LIGHT2,
		 GL10.GL_LIGHT3,
		 GL10.GL_LIGHT4,
		 GL10.GL_LIGHT5,
		 GL10.GL_LIGHT6,
		 GL10.GL_LIGHT7};
		
	private int numlight;
    private float[] lightAmbient = { 0.2f, 0.2f, 0.2f, 1.0f };
    private float[] lightDiffuse = { 1.0f, 1.0f, 1.0f, 1.0f };
    private float[] lightSpecular = { 1.0f, 1.0f, 1.0f, 1.0f };
    private float[] lightPosition = { 0.0f, 0.0f, 0.0f, 1.0f };
    
    private boolean izmAmbient=true;
    private boolean izmDiffuse=true;
    private boolean izmSpecular=true;
    
    private boolean mEnabled=true;
	
	public Light(String name, int numLight) {
		super(name);
		numlight=numLight;
	}
	
	public void setAmbient(float ambient[]){
		lightAmbient=ambient;
		izmAmbient=true;
	}

	public void setDiffuse(float diffuse[]){
		lightDiffuse=diffuse;
		izmDiffuse=true;
	}

	public void setSpecular(float specular[]){
		lightDiffuse=specular;
		izmSpecular=true;
	}
	
    public void setEnable(boolean enabled){
    	mEnabled=enabled;
    }

    public boolean getEnable(){
    	return mEnabled;
    }

    public void flipVisibility(){
    	mEnabled= ! mEnabled;
    }
	
	//public void setPosition(float Position[]){
	//	lightDiffuse=Position;
	//}
	
	public void draw(GL10 gl){
		if (mEnabled){
	         gl.glEnable(GLLIGHTS[numlight]); 
	         Matrix4 mat=mParent.getDerivedMatrix();
	         Vector3 pos=mat.Position();
	         lightPosition[0]=pos.x;
	         lightPosition[1]=pos.y;
	         lightPosition[2]=pos.z;
	         gl.glLightfv(GLLIGHTS[numlight], GL10.GL_POSITION, lightPosition,0);
	         if (izmAmbient){
	        	 gl.glLightfv(GLLIGHTS[numlight], GL10.GL_AMBIENT, lightAmbient,0);
	        	 izmAmbient=false;
	         }
	         if (izmDiffuse){
	        	 gl.glLightfv(GLLIGHTS[numlight], GL10.GL_DIFFUSE, lightDiffuse,0);
	        	 izmDiffuse=false;
	         }
	         if (izmSpecular){
	        	 gl.glLightfv(GLLIGHTS[numlight], GL10.GL_SPECULAR, lightSpecular,0);
	        	 izmSpecular=false;
	         }
		}
		else
	         gl.glDisable(GLLIGHTS[numlight]); 
		
	}

}
