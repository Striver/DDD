package com.striver.opengl1; 

import android.opengl.GLSurfaceView;
import android.app.Activity;
import android.os.Bundle;
 
public class Android_OpenGLActivity extends Activity {
  
    private GLSurfaceView glView;
 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 
        glView = new GLSurfaceView(this);
        glView.setRenderer(new Renderer(this));
        setContentView(glView);
    }
}