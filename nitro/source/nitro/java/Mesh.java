package com.striver.opengl1;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.content.res.Resources.NotFoundException;
//import android.util.Log;

public class Mesh {
	protected  FloatBuffer vertexBuffer;
	protected  FloatBuffer textureBuffer;
	protected  ShortBuffer indexBuffer;
	protected ByteBuffer   mColorBuffer;
	protected FloatBuffer normalBuffer;
	
	protected int graney;
	
    //public float x,y,z,
    //			xrazmer,yrazmer,zrazmer,
    //			xpovorot,ypovorot,zpovorot;

    private float[] diffusematerial = { 0.5f, 1.0f, 0.5f, 1.0f };

    public Mesh(Context context, int resource_mesh) throws NotFoundException, IOException{
    	System.out.println("Nachinaem otkryvat fail");
    	ByteBuffer mbb= OpenRawBuffer(context, resource_mesh);
    	//String text=readRawTextFile(context, R.raw.kubik2);
    	
    	int numvershiny=mbb.getInt();
    	graney=mbb.getInt();
    	System.out.println("vershin="+numvershiny+" graney="+graney);
    	
    	vertexBuffer=GetFloatBuffer(mbb, numvershiny,3);
    	textureBuffer=GetFloatBuffer(mbb, numvershiny,2);
    	normalBuffer=GetFloatBuffer(mbb, numvershiny,3);
    	indexBuffer=GetShortBuffer(mbb, graney,3);
    	
    	//x=y=z=0.0f;
    	//xrazmer=yrazmer=zrazmer=1.0f;
    }
    

    
	public void draw(GL10 gl){ 
		
		//Enable the vertex and texture state
		gl.glNormalPointer(GL10.GL_FLOAT, 0, normalBuffer);
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer);
		//gl.glMaterialfv(GL10.GL_FRONT, GL10.GL_DIFFUSE, diffusematerial,0);
		gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer);
		
		//Draw the vertices as triangles, based on the Index Buffer information
		gl.glDrawElements(GL10.GL_TRIANGLES, graney*3, GL10.GL_UNSIGNED_SHORT, indexBuffer);
		//gl.glDrawElements(GL10.GL_TRIANGLES, graney, GL10.GL_UNSIGNED_BYTE, indexBuffer);
		
	}    

	public static FloatBuffer GetFloatBuffer(ByteBuffer bytebuffer, int vershin, int elementov){
		int lenarray=vershin*elementov*4;
	    byte chisla1[]= new byte[lenarray];
	    bytebuffer.get(chisla1, 0, lenarray);
	    ByteBuffer bb = ByteBuffer.wrap(chisla1);
	    bb.order(ByteOrder.LITTLE_ENDIAN);
	    FloatBuffer bf1 = bb.asFloatBuffer();
	    float koordinaty[]=new float[vershin*elementov];
	    bf1.get(koordinaty);
		ByteBuffer bb2 = ByteBuffer.allocateDirect(koordinaty.length * 4);
		bb2.order(ByteOrder.nativeOrder());
		FloatBuffer bf2 = bb2.asFloatBuffer();
		bf2.put(koordinaty);
		bf2.position(0);
	    return bf2;
		
	}
	
	public static ShortBuffer GetShortBuffer(ByteBuffer bytebuffer, int graney, int elementov){
		int lenarray=graney*elementov*2;
	    byte chisla1[]= new byte[graney*elementov*2];
	    bytebuffer.get(chisla1, 0, lenarray);
	    ByteBuffer bb = ByteBuffer.wrap(chisla1);
	    bb.order(ByteOrder.LITTLE_ENDIAN);
	    ShortBuffer bh1 = bb.asShortBuffer();
	    short indexy[]=new short[graney*elementov];
	    bh1.get(indexy);
		ByteBuffer bb2 = ByteBuffer.allocateDirect(indexy.length * 2);
		bb2.order(ByteOrder.nativeOrder());
	    ShortBuffer bh2=bb2.asShortBuffer();
	    bh2.put(indexy);
	    bh2.position(0);
	    return bh2;
		
	}

	public static ByteBuffer OpenRawBuffer(Context ctx, int resId) throws NotFoundException, IOException{
		int razmerbloka=5000;
		InputStream inputStream =   ctx.getResources().openRawResource(resId);
		//InputStream inputStream =   ctx.getResources().openRawResourceFd(resId).createInputStream();
	    //FileChannel fc = inputStream.getChannel();

		ByteArrayOutputStream baos= new ByteArrayOutputStream();
	    byte[] ReadedBytes = new byte[razmerbloka];
	    int retread=1;
	    //ByteBuffer bb = ByteBuffer.allocate(100000);
	    while (retread>0){
	    	retread=inputStream.read(ReadedBytes,0,razmerbloka);
	    	//System.out.println("fail " + retread);
	    	if (retread>0)
	    		baos.write(ReadedBytes, 0, retread);
	    		//bb.put(ReadedBytes,0,retread);
	    }
	    byte[] barray2=baos.toByteArray();
	    ByteBuffer bb = ByteBuffer.wrap(barray2);
	    bb.order(ByteOrder.LITTLE_ENDIAN);
	    bb.position(0);
	    // ���������� ���� � �������� �����

	   // MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());

	    // ���������� ������ ������� ������

	    //mbb.order(ByteOrder.LITTLE_ENDIAN);
	    return bb;
	}
	
	  public static String readRawTextFile(Context ctx, int resId) throws NotFoundException, IOException //������ ����� �� raw - ��������� �������� � ������������� �������
	  {
	     InputStream inputStream =ctx.getResources().openRawResource(resId);
	 
	      InputStreamReader inputreader = new InputStreamReader(inputStream);
	      BufferedReader buffreader = new BufferedReader(inputreader);
	       String line;
	       StringBuilder text = new StringBuilder();
	       try {
	        while (( line = buffreader.readLine()) != null) {
	          text.append(line);
	          text.append('\n');
	         }
	      } catch (IOException e) {
	      }
	       return text.toString();
	  }
	  
	  public int Graney(){
		  return graney;
	  }

}
