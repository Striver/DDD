package com.striver.opengl1;

import javax.microedition.khronos.opengles.GL10;

public class Entity extends MovableObject {
	
	protected Mesh mMesh;
	protected int mTexture;
	private boolean prozrachnost=false;

	public Entity(String name, Mesh me) {
		super(name);
		// TODO Auto-generated constructor stub
		
		mMesh=me;
		mTexture=0;
	}
	
	public void newMesh(Mesh me){
		mMesh=me;
	}
	
	public void setTexture(int texture){
		mTexture=texture;
	}
	
	public void setProzracnost(boolean pr){
		prozrachnost=pr;
	}
	
	public void draw(GL10 gl){
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTexture);
		mMesh.draw(gl);
		
		//System.out.println("draw Entity "+mName);
		
	}
	
    @Override
	public int Graney(){
		return mMesh.Graney();
	}
}
