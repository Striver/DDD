package com.striver.opengl1;

import javax.microedition.khronos.opengles.GL10;

public class MovableObject {

	public Node mParent=null;
	protected String mName;

	public MovableObject(String name){
		mName=name;
	}

	public String getName(){
		return mName;
	}
	
	public void setParent(Node nd){
		mParent=nd;
	}
	
	public void draw(GL10 gl){}

	public int Graney(){
		return 0;
	}
	
}
