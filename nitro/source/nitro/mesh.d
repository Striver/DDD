module mesh;

import derelict.opengl.gl;
import std.file, std.string;
import logging;
import resources;


enum    log_module="mesh";
enum    Tip_resursa="mesh";

enum sizeFloatVector3=3*4;
enum sizeFloatVector2=2*4;
enum sizeShortVector3=3*2;

struct PolnyVertex {
	float tx, ty, nx, ny, nz, vx, vy, vz;
}


class Mesh : LoadableResource {
	protected void* vertexBuffer;
	protected void* textureBuffer;
	protected void* indexBuffer;
	protected void* normalBuffer;
	
	protected byte[] mBuffer;
	protected float[] mFInterBuffer;
	protected void*  mInterBuffer;
	
	
	protected int graney, numvershiny;
    
    this(string filenamemesh) {
        log(log_module, LogUrovni.Info, format("Начинаем загрузку меша %s", filenamemesh));
		enum sizeZagolovok=8;
        auto bytes1= read(filenamemesh);
        int[] dliny=cast(int[]) bytes1[0..sizeZagolovok];
        numvershiny=dliny[0];
        graney=dliny[1];
        log(log_module, LogUrovni.Debug, format(":bytes1=%s",&bytes1[0]));
        log(log_module, LogUrovni.Info, format("numvershiny=%s  graney=%s", numvershiny,graney));
		int sizeBuffer=numvershiny*(2*sizeFloatVector3+sizeFloatVector2)+graney*sizeShortVector3;
		mBuffer=cast(byte[]) bytes1[sizeZagolovok..(sizeZagolovok+sizeBuffer)];
		_assignBuffers2();
		
        log(log_module, LogUrovni.Info, "Меш, типа, загружен");
        //glGenBuffers( 1, &triangleVBO );
        //glBindBuffer(GL_ARRAY_BUFFER, triangleVBO );
        //glBufferData(GL_ARRAY_BUFFER, 4*3*numvershiny, vertexBuffer, GL_STATIC_DRAW);
        //glBindBuffer(GL_ARRAY_BUFFER,0);
    }
	
	this(float[] vertKoords, float[] texKoords, float[] normKoords, ushort[] ind) {
		 log(log_module, LogUrovni.Debug, "Начинаем формирование меша.");
		numvershiny=vertKoords.length/3;
		graney=ind.length/3;
        log(log_module, LogUrovni.Debug, format("numvershiny=%s  graney=%s", numvershiny,graney));
		byte[] vertKbyte=cast(byte[]) vertKoords;
		byte[] texKbyte=cast(byte[]) texKoords;
		byte[] normKbyte=(normKoords is null) ? new byte[numvershiny*sizeFloatVector3] : cast(byte[]) normKoords;
		//byte[] normKbyte=(normKoords is null) ? new byte[numvershiny*sizeFloatVector3] : cast(byte[]) normKoords;
		byte[] indbyte=cast(byte[]) ind;
		mBuffer = vertKbyte ~ texKbyte ~ normKbyte ~ indbyte;
		_assignBuffers2();
        log(log_module, LogUrovni.Debug, "Меш, типа, сфомирован.");
		
	}
    
	private void _assignBuffers1() {
		int vB0=0;
        int tB0=vB0+numvershiny*sizeFloatVector3;
        int nB0=tB0+numvershiny*sizeFloatVector2;
        int ib0=nB0+numvershiny*sizeFloatVector3;
        int iB1=ib0+graney*sizeShortVector3;
        log(log_module, LogUrovni.Debug, format("vB0=%s    tB0=%s    nB0=%s    ib0=%s    iB1=%s", vB0,  tB0,  nB0, ib0, iB1));
        vertexBuffer=  cast(void*) mBuffer[vB0..tB0];
        textureBuffer= cast(void*) mBuffer[tB0..nB0];
        normalBuffer=  cast(void*) mBuffer[nB0..ib0];
        indexBuffer=   cast(void*) mBuffer[ib0..iB1];
        log(log_module, LogUrovni.Debug, format(":vertexBuffer=%s",vertexBuffer));
        log(log_module, LogUrovni.Debug, format(":textureBuffer=%s",textureBuffer));
        log(log_module, LogUrovni.Debug, format(":normalBuffer=%s",normalBuffer));
		
	}

	private void _assignBuffers2() {
		float[] interBuf=new float[numvershiny*8];
		int vB0=0;
        int tB0=vB0+numvershiny*sizeFloatVector3;
        int nB0=tB0+numvershiny*sizeFloatVector2;
        int ib0=nB0+numvershiny*sizeFloatVector3;
        int iB1=ib0+graney*sizeShortVector3;
		for (int i=0; i<numvershiny; i++) {
			float[] tex=cast(float[]) mBuffer[tB0+i*sizeFloatVector2 .. tB0+i*sizeFloatVector2+sizeFloatVector2];
			float[] norm=cast(float[]) mBuffer[nB0+i*sizeFloatVector3 .. nB0+i*sizeFloatVector3+2*sizeFloatVector3];
			float[] vert=cast(float[]) mBuffer[vB0+i*sizeFloatVector3 .. vB0+i*sizeFloatVector3+2*sizeFloatVector3];
			interBuf[i*8+0]=tex[0];  interBuf[i*8+1]=tex[1]; 
			interBuf[i*8+2]=norm[0]; interBuf[i*8+3]=norm[1]; interBuf[i*8+4]=norm[2]; 
			interBuf[i*8+5]=vert[0]; interBuf[i*8+6]=vert[1]; interBuf[i*8+7]=vert[2]; 
		}
        indexBuffer=   cast(void*) mBuffer[ib0..iB1];
		log(log_module, LogUrovni.Debug, format("interBuf=%s",interBuf[0..24])); 
		mFInterBuffer=interBuf;
		mInterBuffer=cast(void*)(cast(byte[])( interBuf));
	}
	
    void draw1(){ 
		
		//GL_T2F_N3F_V3F
		
        //glBindBuffer(GL_ARRAY_BUFFER,triangleVBO);
		//Enable the vertex and texture state
		glVertexPointer(3, GL_FLOAT, 0, vertexBuffer);
        log(log_module, LogUrovni.Debug, "Загрузили вершины");
        
		glNormalPointer(GL_FLOAT, 0, normalBuffer);
        log(log_module, LogUrovni.Debug, "Загрузили нормали");
		//gl.glMaterialfv(GL10.GL_FRONT, GL10.GL_DIFFUSE, diffusematerial,0);
		glTexCoordPointer(2, GL_FLOAT, 0, textureBuffer);
        log(log_module, LogUrovni.Debug, "Загрузили текстурные координаты");
		
		//Draw the vertices as triangles, based on the Index Buffer information
        //glDrawArrays(GL_TRIANGLES , 0, graney);
		glDrawElements(GL_TRIANGLES, graney*3, GL_UNSIGNED_SHORT, indexBuffer);
		//gl.glDrawElements(GL10.GL_TRIANGLES, graney, GL10.GL_UNSIGNED_BYTE, indexBuffer);
        log(log_module, LogUrovni.Debug, "Отрисовали меш");
		
	}  

	void draw() {
		glInterleavedArrays(GL_T2F_N3F_V3F, 0, mInterBuffer);
		glDrawElements(GL_TRIANGLES, graney*3, GL_UNSIGNED_SHORT, indexBuffer);
        //glDisableClientState(GL_NORMAL_ARRAY);
        //glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        //glDisableClientState(GL_VERTEX_ARRAY);
	}
    
	public int Graney(){
		  return graney;
	}
	
	~this() {
        vertexBuffer=  null;
        textureBuffer= null;
        normalBuffer=  null;
        indexBuffer=   null;
	}

}

class LoadedMeshes : LoadedResources {
	
	this(ResourceNames resources) {
		super(resources);
	}
	
    override protected bool load1(string name) {
        string meshfilename;
        Mesh newmesh;
        try 
            meshfilename=resources.getfilenameres(name, Tip_resursa);
        catch (ResourseException) {
            log(log_module, LogUrovni.Error, format("Отсутствует файл меша %s", name));
            return false;
        }
        try 
            newmesh=new Mesh(meshfilename);
        catch {
            log(log_module, LogUrovni.Error, format("Ошибка в файле меша %s", meshfilename));
            return false;
        }
        //meshes[name]=newmesh;
		res[name]=newmesh;
        return true;
    }
	
	
    
    
}
