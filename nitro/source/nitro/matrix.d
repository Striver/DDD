module matrix;

import std.string;
import vector, quaternion;

public class Matrix3 {
	//------------Внимание!-------------
	// Матрица транспонирована!
	// Строки - это столбцы и наоборот!
	// А может и нет... Вобщем внимание!
	public float m[9]; 
            
    public override string toString(){
    	return format("Matrix3(%s)",m);
    }
}

public class Matrix4 {
	public float m[16];	

	public this(){}
	
	public this(
            float m00, float m01, float m02, float m03,
            float m10, float m11, float m12, float m13,
            float m20, float m21, float m22, float m23,
            float m30, float m31, float m32, float m33 )
        {
            m[0+0*4] = m00;
            m[0+1*4] = m01;
            m[0+2*4] = m02;
            m[0+3*4] = m03;
            m[1+0*4] = m10;
            m[1+1*4] = m11;
            m[1+2*4] = m12;
            m[1+3*4] = m13;
            m[2+0*4] = m20;
            m[2+1*4] = m21;
            m[2+2*4] = m22;
            m[2+3*4] = m23;
            m[3+0*4] = m30;
            m[3+1*4] = m31;
            m[3+2*4] = m32;
            m[3+3*4] = m33;
        }
        
    public this(const Matrix4 othermat) {
        m=othermat.m;
    }

    public this(const Matrix3 m3x3) {
        // Set up matrix with scale and rotation
        m[0+0*4] = m3x3.m[0+0*3]; m[0+1*4] = m3x3.m[0+1*3]; m[0+2*4] = m3x3.m[0+2*3]; 
        m[1+0*4] = m3x3.m[1+0*3]; m[1+1*4] = m3x3.m[1+1*3]; m[1+2*4] = m3x3.m[1+2*3]; 
        m[2+0*4] = m3x3.m[2+0*3]; m[2+1*4] = m3x3.m[2+1*3]; m[2+2*4] = m3x3.m[2+2*3]; 

		// No translation term
		m[0+3*4] = 0; m[1+3*4] = 0; m[2+3*4] = 0;
        // No projection term
        m[3+0*4] = 0; m[3+1*4] = 0; m[3+2*4] = 0; m[3+3*4] = 1;
    }
	
	
	
	void makeTransform(const(Vector3) position, const(Vector3) scale, const(Quaternion) orientation)
    {
        // Ordering:
        //    1. Scale
        //    2. Rotate
        //    3. Translate

        Matrix3 matrot3x3=orientation.ToRotationMatrix();
    	float rot3x3[]=matrot3x3.m;

        //orientation.ToRotationMatrix(rot3x3);

        // Set up final matrix with scale, rotation and translation
        m[0+0*4] = scale.x * rot3x3[0+0*3]; m[0+1*4] = scale.y * rot3x3[0+1*3]; m[0+2*4] = scale.z * rot3x3[0+2*3]; m[0+3*4] = position.x;
        m[1+0*4] = scale.x * rot3x3[1+0*3]; m[1+1*4] = scale.y * rot3x3[1+1*3]; m[1+2*4] = scale.z * rot3x3[1+2*3]; m[1+3*4] = position.y;
        m[2+0*4] = scale.x * rot3x3[2+0*3]; m[2+1*4] = scale.y * rot3x3[2+1*3]; m[2+2*4] = scale.z * rot3x3[2+2*3]; m[2+3*4] = position.z;

        // No projection term
        m[3+0*4] = 0; m[3+1*4] = 0; m[3+2*4] = 0; m[3+3*4] = 1;
    }
	
	public Matrix4 transpose() 
    {
        return new Matrix4(m[0+0*4], m[1+0*4], m[2+0*4], m[3+0*4],
                       m[0+1*4], m[1+1*4], m[2+1*4], m[3+1*4],
                       m[0+2*4], m[1+2*4], m[2+2*4], m[3+2*4],
                       m[0+3*4], m[1+3*4], m[2+3*4], m[3+3*4]);
    }
	
	public Matrix4 mul(Matrix4 m2) 
    {
        Matrix4 r=new Matrix4();
        r.m[0+0*4] = m[0+0*4] * m2.m[0+0*4] + m[0+1*4] * m2.m[1+0*4] + m[0+2*4] * m2.m[2+0*4] + m[0+3*4] * m2.m[3+0*4];
        r.m[0+1*4] = m[0+0*4] * m2.m[0+1*4] + m[0+1*4] * m2.m[1+1*4] + m[0+2*4] * m2.m[2+1*4] + m[0+3*4] * m2.m[3+1*4];
        r.m[0+2*4] = m[0+0*4] * m2.m[0+2*4] + m[0+1*4] * m2.m[1+2*4] + m[0+2*4] * m2.m[2+2*4] + m[0+3*4] * m2.m[3+2*4];
        r.m[0+3*4] = m[0+0*4] * m2.m[0+3*4] + m[0+1*4] * m2.m[1+3*4] + m[0+2*4] * m2.m[2+3*4] + m[0+3*4] * m2.m[3+3*4];

        r.m[1+0*4] = m[1+0*4] * m2.m[0+0*4] + m[1+1*4] * m2.m[1+0*4] + m[1+2*4] * m2.m[2+0*4] + m[1+3*4] * m2.m[3+0*4];
        r.m[1+1*4] = m[1+0*4] * m2.m[0+1*4] + m[1+1*4] * m2.m[1+1*4] + m[1+2*4] * m2.m[2+1*4] + m[1+3*4] * m2.m[3+1*4];
        r.m[1+2*4] = m[1+0*4] * m2.m[0+2*4] + m[1+1*4] * m2.m[1+2*4] + m[1+2*4] * m2.m[2+2*4] + m[1+3*4] * m2.m[3+2*4];
        r.m[1+3*4] = m[1+0*4] * m2.m[0+3*4] + m[1+1*4] * m2.m[1+3*4] + m[1+2*4] * m2.m[2+3*4] + m[1+3*4] * m2.m[3+3*4];

        r.m[2+0*4] = m[2+0*4] * m2.m[0+0*4] + m[2+1*4] * m2.m[1+0*4] + m[2+2*4] * m2.m[2+0*4] + m[2+3*4] * m2.m[3+0*4];
        r.m[2+1*4] = m[2+0*4] * m2.m[0+1*4] + m[2+1*4] * m2.m[1+1*4] + m[2+2*4] * m2.m[2+1*4] + m[2+3*4] * m2.m[3+1*4];
        r.m[2+2*4] = m[2+0*4] * m2.m[0+2*4] + m[2+1*4] * m2.m[1+2*4] + m[2+2*4] * m2.m[2+2*4] + m[2+3*4] * m2.m[3+2*4];
        r.m[2+3*4] = m[2+0*4] * m2.m[0+3*4] + m[2+1*4] * m2.m[1+3*4] + m[2+2*4] * m2.m[2+3*4] + m[2+3*4] * m2.m[3+3*4];

        r.m[3+0*4] = m[3+0*4] * m2.m[0+0*4] + m[3+1*4] * m2.m[1+0*4] + m[3+2*4] * m2.m[2+0*4] + m[3+3*4] * m2.m[3+0*4];
        r.m[3+1*4] = m[3+0*4] * m2.m[0+1*4] + m[3+1*4] * m2.m[1+1*4] + m[3+2*4] * m2.m[2+1*4] + m[3+3*4] * m2.m[3+1*4];
        r.m[3+2*4] = m[3+0*4] * m2.m[0+2*4] + m[3+1*4] * m2.m[1+2*4] + m[3+2*4] * m2.m[2+2*4] + m[3+3*4] * m2.m[3+2*4];
        r.m[3+3*4] = m[3+0*4] * m2.m[0+3*4] + m[3+1*4] * m2.m[1+3*4] + m[3+2*4] * m2.m[2+3*4] + m[3+3*4] * m2.m[3+3*4];

        return r;
    }
	
	public Vector3 Position(){
		return new Vector3(m[0+3*4],m[1+3*4],m[2+3*4]);
	}
            
    public override string toString(){
    	return format("Matrix4(%s)",m);
    }
    
    Matrix3 extract3x3Matrix() {
        Matrix3 m3x3=new Matrix3();
        m3x3.m[0+0*3] = m[0+0*4];
        m3x3.m[0+1*3] = m[0+1*4];
        m3x3.m[0+2*3] = m[0+2*4];
        m3x3.m[1+0*3] = m[1+0*4];
        m3x3.m[1+1*3] = m[1+1*4];
        m3x3.m[1+2*3] = m[1+2*4];
        m3x3.m[2+0*3] = m[2+0*4];
        m3x3.m[2+1*3] = m[2+1*4];
        m3x3.m[2+2*3] = m[2+2*4];
        return m3x3;
    }

	const float
        MINOR(const Matrix4 mat, const size_t r0, const size_t r1, const size_t r2, 
								const size_t c0, const size_t c1, const size_t c2)
    {
		float[] m=mat.m.dup;
        return m[r0+c0*4] * (m[r1+c1*4] * m[r2+c2*4] - m[r2+c1*4] * m[r1+c2*4]) -
            m[r0+c1*4] * (m[r1+c0*4] * m[r2+c2*4] - m[r2+c0*4] * m[r1+c2*4]) +
            m[r0+c2*4] * (m[r1+c0*4] * m[r2+c1*4] - m[r2+c0*4] * m[r1+c1*4]);
    }


    Matrix4 adjoint() const
    {
        return new Matrix4( MINOR(this, 1, 2, 3, 1, 2, 3),
            -MINOR(this, 0, 2, 3, 1, 2, 3),
            MINOR(this, 0, 1, 3, 1, 2, 3),
            -MINOR(this, 0, 1, 2, 1, 2, 3),

            -MINOR(this, 1, 2, 3, 0, 2, 3),
            MINOR(this, 0, 2, 3, 0, 2, 3),
            -MINOR(this, 0, 1, 3, 0, 2, 3),
            MINOR(this, 0, 1, 2, 0, 2, 3),

            MINOR(this, 1, 2, 3, 0, 1, 3),
            -MINOR(this, 0, 2, 3, 0, 1, 3),
            MINOR(this, 0, 1, 3, 0, 1, 3),
            -MINOR(this, 0, 1, 2, 0, 1, 3),

            -MINOR(this, 1, 2, 3, 0, 1, 2),
            MINOR(this, 0, 2, 3, 0, 1, 2),
            -MINOR(this, 0, 1, 3, 0, 1, 2),
            MINOR(this, 0, 1, 2, 0, 1, 2));
    }


    float determinant() const
    {
        return m[0+0*4] * MINOR(this, 1, 2, 3, 1, 2, 3) -
            m[0+1*4] * MINOR(this, 1, 2, 3, 0, 2, 3) +
            m[0+2*4] * MINOR(this, 1, 2, 3, 0, 1, 3) -
            m[0+3*4] * MINOR(this, 1, 2, 3, 0, 1, 2);
    }

    Matrix4 inverse() const
    {
        return adjoint().mul(1.0f / determinant());
    }

    Matrix4 mul(float scalar)
        {
            return new Matrix4(
                scalar*m[0+0*4], scalar*m[0+1*4], scalar*m[0+2*4], scalar*m[0+3*4],
                scalar*m[1+0*4], scalar*m[1+1*4], scalar*m[1+2*4], scalar*m[1+3*4],
                scalar*m[2+0*4], scalar*m[2+1*4], scalar*m[2+2*4], scalar*m[2+3*4],
                scalar*m[3+0*4], scalar*m[3+1*4], scalar*m[3+2*4], scalar*m[3+3*4]);
        }
		
}