module texture;

import derelict.opengl.gl, derelict3.sdl2.sdl, derelict3.sdl2.image, derelict3.sdl2.ttf;
import std.string, core.stdc.string, std.conv;
//import std.string : fromStringz;

import resources, font;
import logging;

enum log_module="texture";
enum Tip_resursa="picture";

inout(char)[] fromStringz(inout(char)* cString) @system pure {
    return cString ? cString[0 .. strlen(cString)] : null;
}

class Texture : LoadableResource {

private:

    int mWidth;
	int mHeight;
	int mFormat;
	int mIformat;

	uint mTexture;

public:   
    
	int getWidth() {
		return mWidth;
	}
	
	int getHeight() {
		return mHeight;
	}

	float getRatio() {
		if (mHeight > 0)
			return to!float(mWidth) / mHeight;
		else
			return -1;
	}
	
    void load_fromFile(string filename) {
    
        SDL_Surface* mysurface = null;
    
        // You should probably use CSurface::OnLoad ... ;)
        //-- and make sure the Surface pointer is good!
        log(log_module, LogUrovni.Debug, format("Щас текстуру %s грузить будем!", filename));
        mysurface = IMG_Load(toStringz(filename));
    
        if (mysurface is null)
        {
            log(log_module, LogUrovni.Error, format("Картинка %s не загрузилась" , filename));
			log(log_module, LogUrovni.Error, format("%s",fromStringz(IMG_GetError())));
            return;
        }   
        log(log_module, LogUrovni.Debug, "По крайней мере картинку загрузили!");
		
		_assignTexture(mysurface);
    
        SDL_FreeSurface(mysurface);
        //Any other glTex* stuff here
    
    }

	void load_fromText(SDLFont font, string textureText, float[3] color ) {
		
		SDL_Color textColor = SDL_Color(to!ubyte(color[0]*255), to!ubyte(color[1]*255), to!ubyte(color[2]*255));
		const(char)* textureText2=toStringz(textureText);
		//SDL_Surface* textSurface = TTF_RenderUTF8_Solid( gFont, textureText2, textColor );
		SDL_Surface* textSurface = TTF_RenderUTF8_Blended_Wrapped( font(), textureText2, textColor , 600);
        if (textSurface is null)
        {
            log(log_module, LogUrovni.Error, format("Текст \"%s\" не отрендерился" , textureText));
            return;
        }   
        log(log_module, LogUrovni.Debug, "По крайней мере текст отрендерили!");
		
		_assignTexture(textSurface);
		if (mTexture)
			log(log_module, LogUrovni.Debug, 
				format("Текст загружен в текстуру! Width=%s, Height=%s", mWidth, mHeight));
		
        SDL_FreeSurface(textSurface);
		//TTF_CloseFont( gFont );
	}
    
    void draw() {
        glBindTexture(GL_TEXTURE_2D, mTexture);
    }
    
	private void _assignTexture(SDL_Surface* surface) {
        GLuint texture = 0;
        glGenTextures(1, &texture);
		
        glBindTexture(GL_TEXTURE_2D, texture);
    
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    
        mFormat = GL_RGB; 
    
        //access violation on mysurface (until I added NULL test)
        if(surface.format.BytesPerPixel == 4) {
            mFormat = GL_RGBA;
        }
    
        log(log_module, LogUrovni.Debug, "Щас текстуру определять будем!");
        glTexImage2D(GL_TEXTURE_2D, 0, mFormat, surface.w, surface.h, 0, mFormat, GL_UNSIGNED_BYTE, surface.pixels); 
		GLenum err = glGetError(); 
		if (err != GL_NO_ERROR) {
			log(log_module, LogUrovni.Error, format("Ошибка копирования текстуры %s", err));
			glDeleteTextures(1, &texture);
			return;
		}
        log(log_module, LogUrovni.Debug, "Определили текстуру");
		
		mWidth=surface.w;
		mHeight=surface.h;
		
        mTexture= texture;
		
	}
	
	~this() {
		if (mTexture != 0)
			glDeleteTextures(1, &mTexture);
	}
	
}

class LoadedTextures : LoadedResources {
	
	this(ResourceNames resources) {
		super(resources);
	}
	
    override protected bool load1(string name) {
        string filename;
        Texture newtex;
        try 
            filename=resources.getfilenameres(name, Tip_resursa);
        catch (ResourseException) {
            log(log_module, LogUrovni.Error, format("Отсутствует файл текстуры %s", name));
            return false;
        }
        newtex=new Texture();
        try 
			newtex.load_fromFile(filename);
			//newtex.load_fromFile("..\\" ~ filename);
        catch {
            log(log_module, LogUrovni.Error, format("Ошибка в файле текстуры %s", filename));
            return false;
        }
        //meshes[name]=newmesh;
		res[name]=newtex;
        return true;
    }
    
}
