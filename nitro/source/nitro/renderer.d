﻿module ddd.vis;

import std.conv, std.string;
//import derelict.opengl.gl, derelict3.sdl2.sdl; 
//import scena, windowSDL2, node, mesh, resources, loadconfig, constanty, nitromanager, event;

import ddd.quaternion, ddd.vector, ddd.matrix, ddd.dvigimyi_object, ddd.zavisimost.graphika_gl;
import my_utils.journal;

private enum имя_модуля = "визуализатор";


class Визуализатор {
    
    //private Scena scena=null;
    //private Resources resources;
    //public LoadedMeshes meshmanager;
    
    
    //public Node mBaseNode;
    
    NitroManager nManager;
	float ratio;
    
    this(NitroManager NM) {
        //resources=new Resources(configvarsas.get("dirresources", dirresources_default));
        //meshmanager=new LoadedMeshes(resources);
        
        nManager=NM;
        
        auto initwindow=initGLwindow();
        if (initwindow) {
            onSurfaceCreated();
            //onSurfaceChanged(GetWindowWidth(), GetWindowHeight());
            onSurfaceChanged();
            //mBaseNode=new Node("BaseNode");
        }
    }
    
    void init(NitroManager NM) {
        nManager=NM;
    }
    
    public void onDrawFrame() {
        /*
         * Usually, the first thing one might want to do is to clear
         * the screen. The most efficient way of doing this is to use
         * glClear().
         */
    	

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        /*
         * Now we're ready to draw some 3D objects
         */

        
        glEnable(GL_BLEND);
 		glEnable(GL_TEXTURE_2D);			//Enable Texture Mapping ( NEW )
		glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

		//glFrontFace(GL_CCW);
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glEnableClientState(GL_NORMAL_ARRAY);
        
        //glMaterialf(GL_FRONT, GL_SHININESS,100.0f);
		
		//auto mat_dif=[0.0f, 0.8f, 0.2f, 1.0f];
		
        //glMaterialfv(GL_FRONT, GL_DIFFUSE,&mat_dif[0]);
		
		// --- рендерим Фон. Тест глубины отключен
		glDisable(GL_DEPTH_TEST);
        glDisable(GL_LIGHTING);
		nManager.mCamera.setRotateCamera(true);
		nManager.mBackgroundBaseNode.draw();
		
		
		// --- Включаем тест глубины, рендерим сцену
		glEnable(GL_DEPTH_TEST);
		nManager.mCamera.setCamera(false);
        glEnable(GL_LIGHTING);
        nManager.mBaseNode.draw();
		
		nManager.drawOverlays();

        glDisableClientState(GL_NORMAL_ARRAY);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        glDisableClientState(GL_VERTEX_ARRAY);
        
		}
    
    public void onSurfaceChanged() {
		int width=GetWindowWidth();
		int height=GetWindowHeight();
        glViewport(0, 0, width, height);
        ratio = to!(float)(width) / height;
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glFrustum(-ratio, ratio, -1, 1, 1, 100);
		glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

    }
    
    public void onSurfaceCreated() {
        /*
         * Some one-time OpenGL initialization can be made here
         * probably based on features of this particular context
         */
         glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);

         
         glEnable(GL_CULL_FACE);
         glShadeModel(GL_SMOOTH);
         glEnable(GL_DEPTH_TEST);
         
 		 glEnable(GL_TEXTURE_2D);			//Enable Texture Mapping ( NEW )
         //loadGLTextures(gl, (Context) context);

         //scena=new myScena();
         
         //glClearColor(ColorBackgroud,ColorBackgroud,ColorBackgroud,0);
         
         //glEnable(GL_LIGHTING);
         //glLightModelfv(GL_LIGHT_MODEL_AMBIENT, &scena.lightAmbient[0]);

        
    }

    void onDrawOverlays() {
		nManager.drawOverlays();
	}
	
    void render() {
             //onDrawFrame();
		long i=0;
        if (nManager.ObrEvents_not_enabled())
            nManager.setObrEvents(new ObrEvents());
        
		//for (i=0; i<500; i++) {
        while (true) {
            if (nManager.obr_event())
				break;
                //return;
            nManager.ScenaUpdate();
			onSurfaceChanged();
            onDrawFrame();
			//onDrawOverlays();
			SwapWindow();
			log(log_module, LogUrovni.Debug, "Отрисован  кадр");
            i++;
        }
		log(log_module, LogUrovni.Info, format("Закончили рендерить %s кадров", i));
        //derelict3.sdl2.sdl.SDL_Quit();
    }
    
}

//Renderer render1=null;
