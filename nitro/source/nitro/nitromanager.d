module nitromanager;

import std.string;

import scena, node, mesh, resources, loadconfig, constanty, renderer, event, overlay; 
import texture, camera, vector, quaternion, font;
private import logging;

enum log_module="nitromanager";

class NitroManager {
    public Renderer rend;
    //public 
    public Node mBaseNode;
    public Node mBackgroundBaseNode;
	public Camera mCamera;
	public Node mNodeCamera;
    
    private Scena scena=null;
    private ResourceNames resources;
    private LoadedMeshes loadedmeshes;
    private LoadedTextures loadedtextures;
    private LoadedOverlays loadedoverlays;
    private LoadedSDLFonts loadedSDLfonts;
    private LoadedTextureFontAttr loadedTexFontAttr;
	
    private ObrEvents obrevents=null;
    private Overlay[string] overlays;
	
    
    this() {
        // Инициализируем ресурсы
        resources=new ResourceNames(configvarsas.get("dirresources", dirresources_default));
        loadedmeshes=new LoadedMeshes(resources);
        loadedtextures=new LoadedTextures(resources);
		loadedoverlays=new LoadedOverlays(resources, this);
		loadedSDLfonts=new LoadedSDLFonts(resources);
		loadedTexFontAttr=new LoadedTextureFontAttr(resources);
    
        // Инициализируем рендер
        rend=new Renderer(this);
        //rend.init(this);
		
		// Корневой нод и камера
        mBaseNode=new Node("BaseNode");
		mBackgroundBaseNode=new Node("BackgroundBaseNode");
		mCamera=new Camera("DefaultCamera");
		mNodeCamera=mBaseNode.CreateChild("NodeCamera",
         		new Vector3(0,0,1),
         		new Quaternion(0.0f,  new Vector3(1,0,0)));
		mNodeCamera.attachObject(mCamera);

        
    }
    
    void setScena(Scena newscena) {
        if (newscena !is null)
            this.scena=newscena;
            this.scena.init();
    }
    
    void ScenaUpdate() {
        scena.Update();
    }

    Texture getTexture(string texname) {
		return cast(Texture)(loadedtextures.get(texname));
    }
	
	Mesh getMesh(string meshname) {
		return cast(Mesh)(loadedmeshes.get(meshname));
	}
    
    bool ObrEvents_not_enabled() {
        return (obrevents is null);
    }
    
    void setObrEvents(ObrEvents obr) {
        obrevents = obr; 
    }
    
    ref ObrEvents getObrEvents() {
        return obrevents;
    }
    
    bool obr_event() {
        return obrevents.obr_event();
    }
	
	void drawOverlays() {
		foreach (name, ovr; overlays) {
			ovr.draw();
		}
	}	
	
	void addOverlay(Overlay ov) {
		string name=ov.getName();
		overlays[name]= ov;
	}

	void removeOverlay(string name) {
		overlays.remove(name);
	}
	
	Overlay getOverlay(string ovlname) {
		return cast(Overlay)(loadedoverlays.get(ovlname));
    }

	SDLFont getSDLFont(string fontname) {
		return cast(SDLFont)(loadedSDLfonts.get(fontname));
    }
	
	TextureFontAttr getTexattr(string attrname) {
		return cast(TextureFontAttr)(loadedTexFontAttr.get(attrname));
    }

}