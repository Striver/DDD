module vector;

import std.stdio, std.string, std.math, core.exception;

public class Vector3 {

    public static const Vector3 ZERO=new Vector3();
    public static const Vector3 UNIT_X=new Vector3(1,0,0);
    public static const Vector3 UNIT_Y=new Vector3(0,1,0);
    public static const Vector3 UNIT_Z=new Vector3(0,0,1);
    public static const Vector3 NEGATIVE_UNIT_X=new Vector3(-1,0,0);
    public static const Vector3 NEGATIVE_UNIT_Y=new Vector3(0,-1,0);
    public static const Vector3 NEGATIVE_UNIT_Z=new Vector3(0,0,-1);
    public static const Vector3 UNIT_SCALE=new Vector3(1, 1, 1);

	public static const float EPSILON=1e-06f;
	
    public float x, y, z;
    
    public this(){
        x=y=z=0.0f;
     }

    public static this (){
        
    }
     
    public this( float fX, float fY, float fZ ){
    //writeln("Constructor Vector3", fX, fY, fZ);
    x=fX;
    y=fY;
    z=fZ;
    }
    
    public this( const(Vector3) rkVector ){
    x=rkVector.x;
    y=rkVector.y;
    z=rkVector.z;
    }

    public this(float massiv[]){
    	try {
    		x=massiv[0];
    		y=massiv[1];
    		z=massiv[2];
    	}
    	catch (RangeError e) {
    		x=y=z=0.0f;
    	}
    }
    
    public this(float scaler){
		x=y=z=scaler;
    }
    
    const public float length ()  {
        return cast(float) sqrt( x * x + y * y + z * z );
    }
    
    public Vector3  plus(const(Vector3) rkVector ) 
    {
        return new Vector3(
            x + rkVector.x,
            y + rkVector.y,
            z + rkVector.z);
    }

    public Vector3  mul(const(Vector3) rkVector ) 
    {
        return new Vector3(
            x * rkVector.x,
            y * rkVector.y,
            z * rkVector.z);
    }

    public Vector3  minus(const(Vector3) rkVector ) 
    {
        return new Vector3(
            x - rkVector.x,
            y - rkVector.y,
            z - rkVector.z);
    }
    
    public Vector3  mulScalar(float scalar ) 
    {
        return new Vector3(
            x * scalar,
            y * scalar,
            z * scalar);
    }
    
    public Vector3  Unominus() 
    {
        return new Vector3(
            -x,
            -y,
            -z);
    }
    
    public float distance(const(Vector3) rkVector)  {
        return this.minus(rkVector).length();
    }
    
    public float dotProduct(const(Vector3) rkVector)  {
        return ( x * rkVector.x + y * rkVector.y + z * rkVector.z );
    }
    
    public float normalise(){
    	float fLength=this.length();
    	if (fLength>0){
    		float fInvLength = 1.0f / fLength;
            x *= fInvLength;
            y *= fInvLength;
            z *= fInvLength;
    	}
    	return fLength;
    }
    
    public Vector3 crossProduct( const(Vector3) rkVector ) 
    {
        return new Vector3(
            y * rkVector.z - z * rkVector.y,
            z * rkVector.x - x * rkVector.z,
            x * rkVector.y - y * rkVector.x);
    }
    
    public float angleBetween( const(Vector3) dest)
	{
		float lenProduct = this.length() * dest.length();

		// Divide by zero check
		if(lenProduct < EPSILON)
			lenProduct = EPSILON;

		float f = this.dotProduct(dest) / lenProduct;

		if (f>1.0) f=1.0f;
		if (f<-1.0) f=-1.0f;
		
		return cast(float) acos(f);

	}
    
    public bool isZeroLength() 
    {
        float sqlen = (x * x) + (y * y) + (z * z);
        return (sqlen < (EPSILON * EPSILON));

    }
    
    public Vector3 normalisedCopy() 
    {
        Vector3 ret = new Vector3(this);
        ret.normalise();
        return ret;
    }

    const public override string toString(){
    	return format("Vector3(%s,%s,%s)",x,y,z);
    }
    
    /*
    public Quaternion getRotationTo(Vector3 dest,
		Vector3 fallbackAxis){
    	return new Quaternion();
    }
    */
}
