module overlayelement;

import std.conv, std.string, std.json;
import derelict.opengl.gl; 

import texture, nitromanager, mesh;

private import logging;

enum    log_module="ovElement";

enum JSON_TYPE[string] OverlayElement_JSON = [
	"name": JSON_TYPE.STRING,
	"container": JSON_TYPE.FALSE,
	"koord": JSON_TYPE.ARRAY,
	"texture": JSON_TYPE.STRING
	];

enum OverlayElement_koord_JSON = JSON_TYPE.FLOAT;

enum float[] MeshVerts=[0,0,0,1,1,0,0,1,0,0,0,0,1,0,0,1,1,0];

class OverlayElement {
	protected {
		bool mEnabled=true;
		float relx1, rely1, relx2, rely2, relwidth, relheight;
		float[4] texKoord; // Инициализируется в 0
		int x,y, width, height;
		string mName;
		bool mContainer;
		OverlayElement[string] mElements;
		Texture mTexture=null;
		Mesh mMesh;
	}
	
	public OverlayElement mParent=null;
	
	public void setParent(OverlayElement parent){
		mParent=parent;
		int w,h;
		parent.getAbsoluteSize(w,h);
		setAbsoluteKoord(w,h);
	}
	

	this(string nm, bool container) {
		mName=nm;
		mContainer=container;
		initTexKoord();
		setKoord(0,0,1,1);
		//mMaterial= new Material();
	}

	this(string nm, bool container, float x, float y, float width, float height) {
		mName=nm;
		mContainer=container;
		initTexKoord();
		//mMaterial= new Material();
		setKoord(x, y, width, height);
	}
	
	
	public void setKoord(float x, float y, float width, float height) {
		relx1= x>=0? x : 1+x;
		rely1= y>=0? y : 1+y;
		relwidth=width;
		relheight=height;
		relx2=relx1+relwidth;
		rely2=rely1+relheight;

		log(log_module, LogUrovni.Debug, 
			format("%s relx1=%s, rely1=%s, relx2=%s, rely2=%s",
				   mName, relx1, rely1, relx2, rely2)); 
	}

	
	public void setAbsoluteKoord(int parentWidth, int parentHeight) {
		this.x=to!int(relx1*parentWidth);
		this.y=to!int(rely1*parentHeight);
		this.width=to!int(relwidth*parentWidth);
		this.height=to!int(relheight*parentHeight);
	}
	
	public void getAbsoluteSize(out int Width, out int Height) {
		Width=this.width;
		Height=this.height;
	}
	
	public void setAbsoluteSize(int Width, int Height) {
		int parentwidth,parentheight;
		mParent.getAbsoluteSize(parentwidth, parentheight);
		if ((parentwidth <= 0) || (parentheight <= 0) )
			return;
			
		if (parentwidth >= (Width+relx1*parentwidth)) {
			relwidth=to!float(Width)/parentwidth;
			relx2=relx1+relwidth;
			this.width=Width;
		}
		else {
			relx2=1;
			relwidth=relx2-relx1;
			this.width=to!int(relwidth*parentwidth);
			texKoord[2]=relwidth /(Width/parentwidth);
		}
		
		if (parentheight >= (Height+rely1*parentheight)) {
			relheight=to!float(Height)/parentheight;
			rely2=rely1+relheight;
			this.height=Height;
		}
		else {
			rely2=1;
			relheight=rely2-rely1;
			this.height=to!int(relheight*parentheight);
			texKoord[3]=relheight / (Height/parentheight);
		}
	}
	
	public void setTexKoord(float x1, float y1, float x2, float y2) {
		texKoord[0]=x1;
		texKoord[1]=y1;
		texKoord[2]=x2;
		texKoord[3]=y2;
		log(log_module, LogUrovni.Debug, 
			format("Присваивание текстурных координат %s texKoord0=%s",
				   mName, texKoord)); 
		setMesh();
	}
	
	public void setTexKoord(float[4] TexKoord) {
		
		texKoord=TexKoord;
		setMesh();
	}
	
	protected void initTexKoord() {
		texKoord[0]=0;
		texKoord[1]=0;
		texKoord[2]=1;
		texKoord[3]=1;
	}
	
	protected void setMesh() {
		//if (mMesh !is null)
		//	clear(mMesh);
		
		float[] vertKoords=MeshVerts; //[0,0,0,1,1,0,0,1,0,0,0,0,1,0,0,1,1,0];
		float[] texKoords=[texKoord[0], texKoord[3], texKoord[2], texKoord[1], texKoord[0], texKoord[1],
						   texKoord[0], texKoord[3], texKoord[2], texKoord[3], texKoord[2], texKoord[1]];
		ushort[] ind=new ushort[6];
		for (uint i=0u; i<6; i++)
			ind[i]=to!(ushort)(i);

		mMesh=new Mesh(vertKoords, texKoords, null, ind);
	}
	
    public void setVisible(bool enabled){
    	mEnabled=enabled;
    }

    public bool getVisible(){
    	return mEnabled;
    }

    public void flipVisibility(){
    	mEnabled= ! mEnabled;
    }

	
    public string getName(){
    	return mName;
    }
	
    public void setTexture(Texture tex) {
        mTexture=tex;
		if (mMesh is null)
			setMesh();
    }
	
	void draw() {
		if (!mEnabled) return;
		//int width=300;
		//int height=300;
		float konec=1.0f;
    	glPushMatrix();
		glTranslatef(relx1, rely1, 0);
		glScalef(relwidth, relheight, 1);
		
		if (mTexture) {
		
			//glEnableClientState(GL_VERTEX_ARRAY);
			//glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			//glEnableClientState(GL_NORMAL_ARRAY);
		
		
			mTexture.draw();
			//nManager.getTexture("babochka").draw();
			
			/*
            glBegin(GL_QUADS);
                glTexCoord2f(texKoord[0],texKoord[3]); //0,1  
				glVertex2f(0, 0);
                glTexCoord2f(texKoord[2],texKoord[3]); //1,1  
				glVertex2f(1, 0);
                glTexCoord2f(texKoord[2],texKoord[1]); //1,0   
				glVertex2f(1, 1);
                glTexCoord2f(texKoord[0],texKoord[1]); //0,0   
				glVertex2f(0, 1);
            glEnd();
			*/
			
			mMesh.draw();
			//glDisableClientState(GL_NORMAL_ARRAY);
			//glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			//glDisableClientState(GL_VERTEX_ARRAY);
			

		/*
            glBegin(GL_QUADS);
                glTexCoord2f(texKoord[0],texKoord[3]); //0,1  
				glVertex2f(relx1, rely1);
                glTexCoord2f(texKoord[2],texKoord[3]); //1,1  
				glVertex2f(relx2, rely1);
                glTexCoord2f(texKoord[2],texKoord[1]); //1,0   
				glVertex2f(relx2, rely2);
                glTexCoord2f(texKoord[0],texKoord[1]); //0,0   
				glVertex2f(relx1, rely2);
            glEnd();
		   */
		}
		if (isContainer()) {
			foreach (element; mElements.byValue()) 
				element.draw();
		}
		glPopMatrix();

	}
	
	bool isContainer() {
		return mContainer; 
	}
	
    public void attachElement(OverlayElement elem){
		if (!mContainer) return;
        if (elem.mParent!is null) return;
        mElements[elem.getName()]= elem;
		elem.setParent(this);
    }
	
    public OverlayElement detachElement(OverlayElement elem){
		return detachElement(elem.getName());
	}
	
    public OverlayElement detachElement(string elemName){
        if (mContainer || (elemName in mElements)) {
            OverlayElement ret=mElements[elemName];
            mElements.remove(elemName);
            ret.setParent(null);
            return ret;
        }
        else
            return null;
    }
	
    public OverlayElement getElement(string elemName){
        if (mContainer || (elemName in mElements)) {
			return mElements[elemName];
		}
		else
            return null;
	}	
	
	static public OverlayElement fromJSON(JSONValue jsv, NitroManager NM) {
		JSONValue[string] jsv1;
		string name;
		bool container=false;
		JSONValue[] koord;
		JSONValue[] texkoord;
		string texname;
		OverlayElement overet;
		try {
			jsv1=jsv.object;
			name=jsv1["name"].str;
			koord=jsv1["koord"].array;
		}
		catch {
			log(log_module, LogUrovni.Error, "Ошибка получения данных для создания OverlayElement");
			return null;
		}
		if ("container" in jsv1) {
			if (jsv1["container"].type == JSON_TYPE.TRUE)
				container=true;
			else 
				if (jsv1["container"].type != JSON_TYPE.FALSE)
					log(log_module, LogUrovni.Error, 
					   "Ошибка определения наличия контейнера для OverlayElement с именем " ~ name);
		}
		try
			overet= new OverlayElement(name, container, koord[0].floating, 
		                    koord[1].floating, koord[2].floating, koord[3].floating);
		catch {
			log(log_module, LogUrovni.Error, "Ошибка создания OverlayElement с именем " ~ name);
			return null;
		}
		if ("texture" in jsv1) {
			Texture tex;
			try {
				texname=jsv1["texture"].str;
				tex=NM.getTexture(texname);
			}
			catch
				log(log_module, LogUrovni.Error, "Ошибка получения текстуры для OverlayElement с именем " ~ name); 
			if (tex !is null)
				overet.setTexture(tex);
		}
		try {
			texkoord=jsv1["texKoord"].array;
			overet.setTexKoord(texkoord[0].floating, texkoord[1].floating, 
								texkoord[2].floating, texkoord[3].floating);
		}
		catch
			{}
		if (container && ("Elements" in jsv1)) {
			if (jsv1["Elements"].type == JSON_TYPE.ARRAY) 
				//elements=new OverlayElement[jsv1["Elements"].array.length];
				foreach (JSONValue jsv2; jsv1["Elements"].array) {
					OverlayElement ovel=OverlayElement.fromJSON(jsv2, NM);
					if (ovel) {
						overet.attachElement(ovel);
					}
				}
			else
				log(log_module, LogUrovni.Error, 
				    "Ошибка получения элементов для контейнера OverlayElement с именем " ~ name);
		}
		return overet;
			
	}

}