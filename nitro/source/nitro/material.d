module material;

import derelict.opengl.gl;
import texture;

class Material {
private:
    
    float[4] diffuse=[0.5f, 0.5f, 0.5f, 1.0f];
    float[4] ambient=[0.1f, 0.1f, 0.1f, 1.0f];
    float[4] specular=[0.0f, 0.0f, 0.0f, 1.0f];
    float shininess=5.0f;
    
    bool zadan_ambient=false;
    
    Texture texDiffuse;
    Texture texNormal;
    Texture texSpecular;
        
public:
    
    this(){};
    
    void setTexDuffuse(Texture tex) {
        texDiffuse=tex;
    }
    
    void setTexNormal(Texture tex) {
        texNormal=tex;
    }
    
    void setTexSpecular(Texture tex) {
        texSpecular=tex;
    }
		
    void setDiffuse(const float[4] mat) {
        diffuse=mat;
        if (!zadan_ambient) {
            ambient[]=mat[]/5.0f ;
            ambient[3]=1.0f;
        }
        
    }
    
    void setAmbient(const float[4] mat) {
        ambient=mat;
        zadan_ambient=true;
    }
    
    void setSpecular(const float[4] mat) {
        specular=mat;
    }
    
    void setShininess(const float mat) {
        shininess=mat;
    }
    
    void draw() {
    
        glMaterialfv(GL_FRONT, GL_AMBIENT, &ambient[0]);
        
        if (texSpecular is null ) {
            glMaterialfv(GL_FRONT, GL_SPECULAR, &specular[0]);
        }
        
        if (texDiffuse is null ) {
            glMaterialfv(GL_FRONT, GL_DIFFUSE, &diffuse[0]);
        }
        else {
            texDiffuse.draw();
        }

        
        glMaterialf(GL_FRONT, GL_SHININESS, shininess);
        //if (!is null texNormal) {
       //     
        //}

        
    }
    
}