module quaternion;

import std.string, std.math, core.exception;
import vector, matrix;

public class Quaternion {
	float w, x, y, z;
	
    public this(){
    	w=1.0f;
        x=y=z=0.0f;
     }

    public this( float fW, float fX, float fY, float fZ ){
    	w=fW;
    	x=fX;
    	y=fY;
    	z=fZ;
    }
    
    public this( const(Quaternion) otherQuaternion ){
    	w=otherQuaternion.w;
    	x=otherQuaternion.x;
    	y=otherQuaternion.y;
    	z=otherQuaternion.z;
    }

    public this( Matrix3 rot)
    {
        this.FromRotationMatrix(rot);
    }
    

	/// Construct a quaternion from an angle/axis
    public  this(float rfAngle,  Vector3 rkAxis)
    {
        this.FromAngleAxis(rfAngle, rkAxis);
    }
    /// Construct a quaternion from 3 orthonormal local axes
    public  this( Vector3 xaxis,  Vector3 yaxis,  Vector3 zaxis)
    {
        this.FromAxes(xaxis, yaxis, zaxis);
    }
	/// Construct a quaternion from 4 manual w/x/y/z values
    public  this(float massiv[])
	{
    	try {
    		w=massiv[0];
    		x=massiv[1];
    		y=massiv[2];
    		z=massiv[3];
    	}
    	catch (RangeError e) {
    		w=1.0f;
    		x=y=z=0.0f;
    	}
	}
    
    public void FromRotationMatrix(const(Matrix3) kRotMat) {
    	const float kRot[]=kRotMat.m;
        float fTrace = kRot[0+0*3]+kRot[1+1*3]+kRot[2+2*3];
        float fRoot;

        if ( fTrace > 0.0 )
        {
            // |w| > 1/2, may as well choose w > 1/2
            fRoot = sqrt(fTrace + 1.0f);  // 2w
            w = 0.5f*fRoot;
            fRoot = 0.5f/fRoot;  // 1/(4w)
            x = (kRot[2+1*3]-kRot[1+2*3])*fRoot;
            y = (kRot[0+2*3]-kRot[2+0*3])*fRoot;
            z = (kRot[1+0*3]-kRot[0+1*3])*fRoot;
        }
        else
        {
            // |w| <= 1/2
            const int s_iNext[] = [ 1, 2, 0 ];
            int i = 0;
            if ( kRot[1+1*3] > kRot[0+0*3] )
                i = 1;
            if ( kRot[2+2*3] > kRot[i*i*3] )
                i = 2;
            int j = s_iNext[i];
            int k = s_iNext[j];

            fRoot = cast(float) sqrt(kRot[i+i*3]-kRot[j+j*3]-kRot[k+k*3] + 1.0f);
            //Real* apkQuat[3] = { &x, &y, &z };
            float apkQuat[] = [0.0f, 0.0f, 0.0f];
            
            apkQuat[i] = 0.5f*fRoot;
            fRoot = 0.5f/fRoot;
            w = (kRot[k+j*3]-kRot[j+k*3])*fRoot;
            apkQuat[j] = (kRot[j+i*3]+kRot[i+j*3])*fRoot;
            apkQuat[k] = (kRot[k+i*3]+kRot[i+k*3])*fRoot;
            x= apkQuat[0];
            y= apkQuat[1];
            z= apkQuat[2];
            
        }		
	}

    public void FromAngleAxis (float rfAngle, const(Vector3) rkAxis)
        {
            // assert:  axis[] is unit length
            //
            // The quaternion representing the rotation is
            //   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)

            float fHalfAngle = 0.5f*rfAngle ;
            float fSin = cast(float) sin(fHalfAngle);
            w = cast(float) cos(fHalfAngle);
            x = fSin*rkAxis.x;
            y = fSin*rkAxis.y;
            z = fSin*rkAxis.z;
        }
    
    public float[] ToAngleAxis(){
    	float ret[]=[0.0f,0.0f,0.0f,0.0f];
        float fSqrLength = x*x+y*y+z*z;
        if ( fSqrLength > 0.0 )
        {
            ret[0] = cast(float) (2.0*acos(w));
            float fInvLength = cast(float) (1.0/sqrt(fSqrLength));
            ret[1] = x*fInvLength;
            ret[2] = y*fInvLength;
            ret[3] = z*fInvLength;
        }
        else
        {
            // angle is 0 (mod 2*pi), so any axis will do
        	ret[0] = 0.0f;
        	ret[1] = 1.0f;
        	ret[2] = 0.0f;
        	ret[3] = 0.0f;
        }
        return ret;

    }
    
    public void FromAxes (Vector3 xaxis, Vector3 yaxis, Vector3 zaxis)
    {
        Matrix3 kRotMat=new Matrix3();
        float kRot[]=kRotMat.m;

        kRot[0+0*3] = xaxis.x;
        kRot[1+0*3] = xaxis.y;
        kRot[2+0*3] = xaxis.z;

        kRot[0+1*3] = yaxis.x;
        kRot[1*1*3] = yaxis.y;
        kRot[2+1*3] = yaxis.z;

        kRot[0+2*3] = zaxis.x;
        kRot[1+2*3] = zaxis.y;
        kRot[2+2*3] = zaxis.z;

        FromRotationMatrix(kRotMat);

    }
    
    const Matrix3 ToRotationMatrix()
    {
    	Matrix3 kRotMat=new Matrix3();
    	float kRot[]=kRotMat.m;
    	
        float fTx  = x+x;
        float fTy  = y+y;
        float fTz  = z+z;
        float fTwx = fTx*w;
        float fTwy = fTy*w;
        float fTwz = fTz*w;
        float fTxx = fTx*x;
        float fTxy = fTy*x;
        float fTxz = fTz*x;
        float fTyy = fTy*y;
        float fTyz = fTz*y;
        float fTzz = fTz*z;

        kRot[0+0*3] = 1.0f-(fTyy+fTzz);
        kRot[0+1*3] = fTxy-fTwz;
        kRot[0+2*3] = fTxz+fTwy;
        kRot[1+0*3] = fTxy+fTwz;
        kRot[1+1*3] = 1.0f-(fTxx+fTzz);
        kRot[1+2*3] = fTyz-fTwx;
        kRot[2+0*3] = fTxz-fTwy;
        kRot[2+1*3] = fTyz+fTwx;
        kRot[2+2*3] = 1.0f-(fTxx+fTyy);
        
        return kRotMat;
    }
    
    const Vector3 xAxis()
    {
        //float fTx  = 2.0f*x;
        float fTy  = 2.0f*y;
        float fTz  = 2.0f*z;
        float fTwy = fTy*w;
        float fTwz = fTz*w;
        float fTxy = fTy*x;
        float fTxz = fTz*x;
        float fTyy = fTy*y;
        float fTzz = fTz*z;

        return new Vector3(1.0f-(fTyy+fTzz), fTxy+fTwz, fTxz-fTwy);
    }
    
    const Vector3 yAxis()
    {
        float fTx  = 2.0f*x;
        float fTy  = 2.0f*y;
        float fTz  = 2.0f*z;
        float fTwx = fTx*w;
        float fTwz = fTz*w;
        float fTxx = fTx*x;
        float fTxy = fTy*x;
        float fTyz = fTz*y;
        float fTzz = fTz*z;

        return new Vector3(fTxy-fTwz, 1.0f-(fTxx+fTzz), fTyz+fTwx);
    }
    
    const Vector3 zAxis()
    {
        float fTx  = 2.0f*x;
        float fTy  = 2.0f*y;
        float fTz  = 2.0f*z;
        float fTwx = fTx*w;
        float fTwy = fTy*w;
        float fTxx = fTx*x;
        float fTxz = fTz*x;
        float fTyy = fTy*y;
        float fTyz = fTz*y;

        return new Vector3(fTxz+fTwy, fTyz-fTwx, 1.0f-(fTxx+fTyy));
    }
    
    const Quaternion mul (const(Quaternion) rkQ)
    {
        // NOTE:  Multiplication is not generally commutative, so in most
        // cases p*q != q*p.

        return new Quaternion
        (
            w * rkQ.w - x * rkQ.x - y * rkQ.y - z * rkQ.z,
            w * rkQ.x + x * rkQ.w + y * rkQ.z - z * rkQ.y,
            w * rkQ.y + y * rkQ.w + z * rkQ.x - x * rkQ.z,
            w * rkQ.z + z * rkQ.w + x * rkQ.y - y * rkQ.x
        );
    }
    
    const Quaternion mul (float fScalar) 
    {
        return new Quaternion(fScalar*w,fScalar*x,fScalar*y,fScalar*z);
    }
    
    const Quaternion Unominus ()
    {
        return new Quaternion(-w,-x,-y,-z);
    }
    
    const float Dot (const(Quaternion) rkQ) 
    {
        return w*rkQ.w+x*rkQ.x+y*rkQ.y+z*rkQ.z;
    }
    
    const float Norm () 
    {
        return w*w+x*x+y*y+z*z;
    }
    
    const Quaternion Inverse ()
    {
        float fNorm = w*w+x*x+y*y+z*z;
        if ( fNorm > 0.0 )
        {
            float fInvNorm = 1.0f/fNorm;
            return new Quaternion(w*fInvNorm,-x*fInvNorm,-y*fInvNorm,-z*fInvNorm);
        }
        else
        {
            // return an invalid result to flag the error
            return new Quaternion();
        }
    }
    
    float normalise()
    {
    	float len = Norm();
    	float factor = cast(float)(1.0f / sqrt(len));
    	w*=factor;
    	x*=factor;
    	y*=factor;
    	x*=factor;
    	return len;
    }
    
    const Quaternion UnitInverse ()
    {
        return new Quaternion(w,-x,-y,-z);
    }
    
    Vector3 mul (Vector3 v)
    {
		// nVidia SDK implementation
		Vector3 uv, uuv;
		Vector3 qvec=new Vector3(x, y, z);
		uv = qvec.crossProduct(v);
		uuv = qvec.crossProduct(uv);
		uv = uv.mulScalar(2.0f * w);
		uuv = uuv.mulScalar(2.0f);

		return v.plus(uv.plus(uuv)); //Блин, прям Лисп какой-то
    }
    
    bool equals(const(Quaternion) rhs, float tolerance) 
	{
    	//tolerance - максимальный угол между кватернионами
    	float fCos = Dot(rhs);
        double angle = acos(fCos);

		return (abs(angle) <= tolerance)
            || (abs(angle- PI) < tolerance);
	}
    
    const public override string toString(){
    	return format("Quaternion(%s,%s,%s,%s)",w,x,y,z);
    }
}
