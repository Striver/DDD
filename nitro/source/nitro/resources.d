module resources;

import std.file, std.string, std.path;
import logging;

enum log_module="resources";

enum auto TipResources0=["mesh":[".me"], 
						 "picture":[".jpg", ".png", ".tga", ".tif"], 
						 "object":[".ob"],
						 "overlay":[".ovl"],
						 "SDLfont":[".ttf", ".fon"],
						 "texfontattr":[".fnattr"]
						 ];

class ResourseException : Exception {
    this(string s) { 
        super(s); 
    }
};

class ResourceNames {
    protected string[string][string] resnames;
    protected string[] dirresources;
    protected string[][string] TipResources;
    protected string[string] loadedres;
    
    
    this(string[] dirresources) {
        this.dirresources=dirresources;
        TipResources=TipResources0.dup;
        loadfilenames();
        log(log_module, LogUrovni.Info, logformatmultiline(format("Загруженные имена ресурсов: %(\n%s %)" , resnames))); 
    }
    
    void loadfilenames() {
		string cwd=getcwd();
        int[string][string] typy;
        foreach (tip, exts; TipResources) {
            foreach (ext; exts) 
                typy[tip][ext]=0;
            }
        foreach (dirname; dirresources) 
            foreach (string filename; dirEntries(dirname, SpanMode.shallow)) {
                auto fileext=toLower(extension(filename));
                auto basename=baseName(filename, fileext);
                if (fileext.length==0) continue;
                foreach (tip, exts; TipResources) 
                    if (fileext in typy[tip]) {
                        //resnames[tip][basename]= cwd ~ dirSeparator ~ filename;
                        resnames[tip][basename]= filename;
                        break;
                    } 
                    
                }
    }

    string getfilenameres(string resname, string tip){
        if (resname in resnames[tip])
            return resnames[tip][resname];
        else
            throw new ResourseException(tip ~ "[" ~ resname ~ "]");
    }
}

interface LoadableResource {
 
}

// Класс - контейнер загруженных ресурсов
// Хранится в NitroManager в отдельной переменной для каждого типа ресурса
// Нужно делать потомка для каждого типа загружаемого ресурса
// Переопределять нужно только конструктор и функцию load1
class LoadedResources {

	// Ссылка на ранее загруженный массив с именами файлов
    protected ResourceNames resources;	
	// Сам контейнер
	protected LoadableResource[string] res; 

    this(ResourceNames resources) {
        this.resources=resources;
    };

    void load(string[] resnames) {
        foreach (string name; resnames) {
            if (name in res)
                continue;
            load1(name);
        }
    }
	
	abstract protected bool load1(string name);
	
    LoadableResource get(string name) {
        if (name in res)
            return res[name];
        if (load1(name))
            return res[name];
        log(log_module, LogUrovni.Error, format("Отсутствует ресурс с именем: %s" , name)); 
        return null;
    }
    
    void reload(string[] resnames) {
        load(resnames);
    }
	
}