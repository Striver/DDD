module overlaytextfield;

import std.conv, std.string, std.utf;
import derelict.opengl.gl; 

import texture, nitromanager, overlayelement, font, mesh;
private import logging;

enum    log_module="ovTxtField";

class OverlaySDLTextField : OverlayElement {
	
	this(string nm, bool container, float x, float y, float width, float height) {
		super(nm, container, x, y, width, height);
	}

	this(string nm, bool container) {
		super(nm, container);
	}

	
	void setText(NitroManager NM, string text, string strFont, float[3] color) {
		SDLFont font=NM.getSDLFont(strFont);
		if (font) {
			initTexKoord();
			Texture tex1=new Texture();
			tex1.load_fromText(font, text, color);
			setTexture(tex1);
		}
	}
	
	void resize() {
		if ((mTexture is null) || (mParent is null)) {
			log(log_module, LogUrovni.Warning, 
			    format("Изменение размеров не получилось %s mTexture=%s, mParent=%s", mName, mTexture, mParent));
			return;
		}
		int width=mTexture.getWidth();
		int height=mTexture.getHeight();
		log(log_module, LogUrovni.Debug, 
			format("Изменение размеров %s width=%s, height=%s", mName, width, height));
		setAbsoluteSize(width, height);
		log(log_module, LogUrovni.Debug, 
			format("Изменили размеры %s relx1=%s, rely1=%s, relx2=%s, rely2=%s",
				   mName, relx1, rely1, relx2, rely2)); 
	}
	
}

// --- Класс элеметов оверлеев с текстом, основанным на текстурном шрифте ----
// --- Размеры определяются по высоте, т.е. первоначальная ширина не используется,
// --- а пересчитывается в зависимости от высоты и полученного текста
class OverlayTextureTextField : OverlayElement {
	
	private string[] mText;
	private TextureFontAttr mAttr;
	private int textwidth;
	
	//private Mesh mMesh;
	
	this(string nm, float x, float y, float width, float height) {
		super(nm, false, x, y, width, height);
	}

	this(string nm) {
		super(nm, false);
	}

	/*
	void setText(NitroManager NM, string text, string strTextureFont) {
		log(log_module, LogUrovni.Debug, format("Начинаем строить текстурный текст %s", text));
		Texture tex1=NM.getTexture(strTextureFont);
		mAttr=NM.getTexattr(strTextureFont);
		if ((tex1 is null) || (mAttr is null)) {
			log(log_module, LogUrovni.Error, format("Не загрузился текстурный шрифт %s ", strTextureFont));
			return;
		}
		//fontHeight=mAttr.Height;
		string text1=text;
		mText=[];
		mElements=null;
		textwidth=0;
		while (text1.length>0) {
			try {
				string sym= toUTF8([decodeFront(text1)]);
				mText ~= sym;
				textwidth += mAttr[sym].width;
			}
			catch 
				continue;
		}
		float relpixel=relheight/mAttr.Height;
		if (mParent !is null) {
			int parW, parH;
			mParent.getAbsoluteSize(parW,parH);
			log(log_module, LogUrovni.Debug, format("parW=%s, parH=%s", parW, parH));
			//relwidth=(to!(float)(textwidth)/to!(float)(mAttr.Height))*(to!(float)(parH)*relheight)/to!(float)(parW);
			relwidth=to!(float)(textwidth*parH*relheight)/to!(float)(mAttr.Height*parW);
		}
		else
			relwidth=relpixel*textwidth;
		relx2=relx1+relwidth;
		
		int symX=0;
		foreach(sym; mText) {
			int symWydth=mAttr[sym].maxx-mAttr[sym].minx;
			if ((symX==0) && (mAttr[sym].minx<0))
				symX=-mAttr[sym].minx;
			float relsymX=to!(float)(symX+mAttr[sym].minx)/textwidth;
			float relsymWidth=to!(float)(symWydth)/textwidth;
			OverlayElement newovel= new OverlayElement(to!(string)(symX), 
				false, relsymX, 0, relsymWidth, 1);
			float texX1=to!(float)(mAttr[sym].x)/tex1.getWidth();
			float texY1=to!(float)(mAttr[sym].y)/tex1.getHeight();
			int texSymWidth= (mAttr[sym].width > symWydth) ? mAttr[sym].width : symWydth;
			float texX2=to!(float)(mAttr[sym].x+texSymWidth)/tex1.getWidth();
			//float texX2=to!(float)(mAttr[sym].x+mAttr[sym].width)/tex1.getWidth();
			float texY2=to!(float)(mAttr[sym].y+mAttr.Height)/tex1.getHeight();
			newovel.setTexKoord(texX1, texY1, texX2, texY2);
			//newovel.setTexKoord(mAttr[sym].x, 1-mAttr[sym].y, 
			//	mAttr[sym].x+symWydth, 1-mAttr[sym].y-mAttr.Height);
			log(log_module, LogUrovni.Debug, format("TexKoord %s = %s", sym, [texX1, texY1, texX2, texY2]));
			newovel.setTexture(tex1);
			this.attachElement(newovel);
			symX+=mAttr[sym].width;
		}
		log(log_module, LogUrovni.Debug, format("Сформировали текстурный текст в оверлее. %s", mText));
		log(log_module, LogUrovni.Debug, 
			format("relpixel=%s, Height=%s, textwidth=%s", relpixel, mAttr.Height, textwidth));
		log(log_module, LogUrovni.Debug, format("relwidth=%s, relheight=%s", relwidth, relheight));
		//log(log_module, LogUrovni.Debug, format("Элементы: %s", mElements));
	}
	*/
	
	void setText(NitroManager NM, string text, string strTextureFont) {
		log(log_module, LogUrovni.Debug, format("Начинаем строить текстурный текст %s", text));
		Texture tex1=NM.getTexture(strTextureFont);
		mAttr=NM.getTexattr(strTextureFont);
		if ((tex1 is null) || (mAttr is null)) {
			log(log_module, LogUrovni.Error, format("Не загрузился текстурный шрифт %s ", strTextureFont));
			return;
		}
		
		setTexture(tex1);
		
		//fontHeight=mAttr.Height;
		string text1=text;
		mText=[];
		mElements=null;
		textwidth=0;
		while (text1.length>0) {
			try {
				string sym= toUTF8([decodeFront(text1)]);
				mText ~= sym;
				textwidth += mAttr[sym].width;
			}
			catch 
				continue;
		}
		float relpixel=relheight/mAttr.Height;
		if (mParent !is null) {
			int parW, parH;
			mParent.getAbsoluteSize(parW,parH);
			log(log_module, LogUrovni.Debug, format("parW=%s, parH=%s", parW, parH));
			//relwidth=(to!(float)(textwidth)/to!(float)(mAttr.Height))*(to!(float)(parH)*relheight)/to!(float)(parW);
			relwidth=to!(float)(textwidth*parH*relheight)/to!(float)(mAttr.Height*parW);
		}
		else
			relwidth=relpixel*textwidth;
		relx2=relx1+relwidth;
		
		enum Verhin_v_Bukve=6;
		enum Graney_v_Bukve=2;
		enum sizeVector3=3;
		enum sizeTexKoord=2;
		
		int lenText=mText.length;
		float[] vertKoords=new float[lenText*sizeVector3*Verhin_v_Bukve];
		vertKoords[]=0;
		float[] texKoords=new float[lenText*sizeTexKoord*Verhin_v_Bukve];
		ushort[] ind=new ushort[lenText*sizeVector3*Graney_v_Bukve];
		for (uint i=0u; i<(lenText*sizeVector3*Graney_v_Bukve); i++)
			ind[i]=to!(ushort)(i);
		log(log_module, LogUrovni.Debug, 
			format("lenvertKoords=%s, lentexKoords=%s, lenind=%s", vertKoords.length, texKoords.length, ind.length));
		
		int symX=0;
		for(int numSym=0; numSym<mText.length; numSym++) {
			auto sym=mText[numSym];
			int symWydth=mAttr[sym].maxx-mAttr[sym].minx;
			if ((symX==0) && (mAttr[sym].minx<0))
				symX=-mAttr[sym].minx;
			float relsymX1=to!(float)(symX+mAttr[sym].minx)/textwidth;
			float relsymWidth=to!(float)(symWydth)/textwidth;
			float relsymX2=relsymX1+relsymWidth;
			float texX1=to!(float)(mAttr[sym].x)/tex1.getWidth();
			float texY1=to!(float)(mAttr[sym].y)/tex1.getHeight();
			int texSymWidth= (mAttr[sym].width > symWydth) ? mAttr[sym].width : symWydth;
			float texX2=to!(float)(mAttr[sym].x+texSymWidth)/tex1.getWidth();
			//float texX2=to!(float)(mAttr[sym].x+mAttr[sym].width)/tex1.getWidth();
			float texY2=to!(float)(mAttr[sym].y+mAttr.Height)/tex1.getHeight();

			int nachaloVert=numSym*Verhin_v_Bukve*sizeVector3;
			vertKoords[nachaloVert+0]=relsymX1;  vertKoords[nachaloVert+1]=0; // vertKoords[nachaloVert+2]=0; 
			vertKoords[nachaloVert+3]=relsymX2;  vertKoords[nachaloVert+4]=1; // vertKoords[nachaloVert+5]=0; 
			vertKoords[nachaloVert+6]=relsymX1;  vertKoords[nachaloVert+7]=1; // vertKoords[nachaloVert+8]=0; 
			vertKoords[nachaloVert+9]=relsymX1;  vertKoords[nachaloVert+10]=0; //vertKoords[nachaloVert+11]=0; 
			vertKoords[nachaloVert+12]=relsymX2; vertKoords[nachaloVert+13]=0; //vertKoords[nachaloVert+14]=0; 
			vertKoords[nachaloVert+15]=relsymX2; vertKoords[nachaloVert+16]=1; //vertKoords[nachaloVert+17]=0; 
			
			int nachaloTex=numSym*Verhin_v_Bukve*sizeTexKoord;
			texKoords[nachaloTex+0]=texX1;  texKoords[nachaloTex+1]=texY2;  
			texKoords[nachaloTex+2]=texX2;  texKoords[nachaloTex+3]=texY1;  
			texKoords[nachaloTex+4]=texX1;  texKoords[nachaloTex+5]=texY1;  
			texKoords[nachaloTex+6]=texX1;  texKoords[nachaloTex+7]=texY2;  
			texKoords[nachaloTex+8]=texX2;  texKoords[nachaloTex+9]=texY2;  
			texKoords[nachaloTex+10]=texX2; texKoords[nachaloTex+11]=texY1; 

			symX+=mAttr[sym].width;
		}
		mMesh=new Mesh(vertKoords, texKoords, null, ind);
		//log(log_module, LogUrovni.Debug, format("vertKoords=%s",vertKoords));
		log(log_module, LogUrovni.Debug, format("Сформировали текстурный текст в оверлее. %s", mText));
		log(log_module, LogUrovni.Debug, 
			format("relpixel=%s, Height=%s, textwidth=%s", relpixel, mAttr.Height, textwidth));
		log(log_module, LogUrovni.Debug, format("relwidth=%s, relheight=%s", relwidth, relheight));
		//log(log_module, LogUrovni.Debug, format("Элементы: %s", mElements));
	}
	
	override void draw() {
	//void draw1() {
		if (!mEnabled) return;
		float konec=1.0f;
    	glPushMatrix();
		glTranslatef(relx1, rely1, 0);
		glScalef(relwidth, relheight, 1);
		if (mTexture && mMesh) {
			//glEnableClientState(GL_VERTEX_ARRAY);
			//glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			//glEnableClientState(GL_NORMAL_ARRAY);
			
			mTexture.draw();
			mMesh.draw();
			
			//glDisableClientState(GL_NORMAL_ARRAY);
			//glDisableClientState(GL_TEXTURE_COORD_ARRAY);
			//glDisableClientState(GL_VERTEX_ARRAY);
		}
		glPopMatrix();
	}	
	
	override protected void setMesh() {}


}