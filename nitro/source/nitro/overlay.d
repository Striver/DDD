module overlay;

import std.conv, std.string, std.json, std.file;
import derelict.opengl.gl, derelict3.sdl2.sdl; 

private import logging;
import constanty, nitromanager, windowSDL2, overlayelement;
import resources;


enum log_module="overlay";
enum Tip_resursa="overlay";

// --- Типы координат местоположения оверлея -------------------------------
// Absolute - координаты в пикселях
// Relative - x относительно ширины окна, y относительно высоты окна
// RelativeX - x,y относительно ширины окна
// RelativeY - x,y относительно высоты окна
enum TypeKoord {Absolute, Relative, RelativeX, RelativeY};

private enum TypeKoord[string] ConvTypeKoord= [
	"Absolute":TypeKoord.Absolute, 
	"Relative":TypeKoord.Relative, 
	"RelativeX":TypeKoord.RelativeX, 
	"RelativeY":TypeKoord.RelativeY
	];

enum JSON_TYPE[string] Overlay_JSON = [
	"name": JSON_TYPE.STRING,
	"typekoord": JSON_TYPE.STRING,
	"koord": JSON_TYPE.ARRAY,
	"Elements": JSON_TYPE.ARRAY
	];
	
enum Overlay_koord_JSON = JSON_TYPE.FLOAT;
enum Overlay_Elements_JSON = JSON_TYPE.FLOAT;
	
class Overlay : LoadableResource {
	private {
    	NitroManager nManager;
		bool mEnabled=true;
		TypeKoord typekoord=TypeKoord.Absolute;
		float relx, rely, relwidth, relheight;
	    int x,y, width, height;
	    string mName;
		OverlayElement mContainer;
	}
	
	this(string nm, NitroManager NM) {
		mName=nm;
		nManager=NM;
		initContainer();
	}

	this(string nm, NitroManager NM, TypeKoord type, float x, float y, float width, float height) {
		mName=nm;
		nManager=NM;
		initContainer();
		setKoord(type, x, y, width, height);
	}
	
	private void initContainer() {
		mContainer=new OverlayElement(mName ~ "Container", true, 0,0,1,1);
		//mContainer.setTexure(nManager.getTexture(NamePustajaTextura));
	}
	
	
	void setKoord(TypeKoord type, float x, float y, float width, float height) {
		typekoord=type;
		int winwidth=GetWindowWidth();
		int winheight=GetWindowHeight();
		switch(type) {
			case TypeKoord.Absolute:
				this.x= to!(int)(x>=0? x : winwidth+x);
				this.y= to!(int)(y>=0? y : winheight+y);
				this.width=to!(int)(width);
				this.height=to!(int)(height);
				relx=this.x/winwidth;
				relwidth=width/winwidth;
				rely=this.y/winheight;
				relheight=height/winheight;
				break;
			case TypeKoord.Relative:
				relx= x>=0? x : 1+x;
				rely= y>=0? y : 1+y;
				this.x=to!(int)(relx*winwidth);
				this.y=to!(int)(rely*winheight);
				this.width=to!(int)(winwidth*width);
				this.height=to!(int)(winheight*height);
				relwidth=width;
				relheight=height;
				break;
			case TypeKoord.RelativeX:
				relx=x>=0? x : 1+x;
				this.x=to!(int)(winwidth*relx);
				relwidth=width;
				this.width=to!(int)(winwidth*width);
				this.y=to!(int)(y*this.x);
				this.height=to!(int)(height*this.width);
				rely=this.y/winheight;
				relheight=this.height/winheight;
				break;
			case TypeKoord.RelativeY:
				rely=y>=0? y : 1+y;
				this.y=to!(int)(winheight*rely);
				relheight=height;
				this.height=to!(int)(winheight*height);
				this.x=to!(int)(x*this.y);
				this.width=to!(int)(width*this.height);
				relx=this.x/winwidth;
				relwidth=this.width/winwidth;
				break;
			default:
				return;
		}
		log(log_module, LogUrovni.Debug, 
			format("%s x=%s, y=%s, ширина=%s, высота=%s relx=%s, rely=%s, relw=%s, relh=%s",
				   mName, this.x, this.y, this.width, this.height, relx, rely, relwidth, relheight)); 
		mContainer.setAbsoluteKoord(this.width, this.height);
	}

    public string getName(){
    	return mName;
    }

	
	void draw() {
		if (!mEnabled) return;
		//int width=300;
		//int height=300;
		
        glViewport(x, y, width, height);
        float ratio = to!(float)(width) / height;
        glMatrixMode(GL_PROJECTION);
        //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glLoadIdentity();
        //glOrtho(-width, width, -height, height,-1,1);
        glOrtho(0, 1, 0, 1,-1,1);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		//glEnable(GL_BLEND);
		
		glDisable(GL_LIGHTING);
        glDisable(GL_DEPTH_TEST);

 		//glDisable(GL_TEXTURE_2D);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		
		mContainer.draw();
	}	
		
    public void setVisible(bool enabled){
    	mEnabled=enabled;
    }

    public bool getVisible(){
    	return mEnabled;
    }

    public void flipVisibility(){
    	mEnabled= ! mEnabled;
    }

    public void attachElement(OverlayElement elem){
		mContainer.attachElement(elem);
	}
	
    public OverlayElement detachElement(OverlayElement elem){
		return mContainer.detachElement(elem);
	}

    public OverlayElement detachElement(string elemName){
		return mContainer.detachElement(elemName);
	}
	
    public OverlayElement getElement(string name){
    	return mContainer.getElement(name);
    }

	static public TypeKoord convTypeKoord(string strtype) {
		// По-умолчанию TypeKoord.Absolute
		return ConvTypeKoord.get(strtype, TypeKoord.Absolute);
	}
	
	// --- принимаем оверлей из файла конфигурации в формате JSON ----
	static public Overlay fromJSON(JSONValue jsv, NitroManager NM) {
		JSONValue[string] jsv1;
		string name;
		JSONValue[] koord;
		TypeKoord typekoord=TypeKoord.Absolute;
		Overlay ovlret;
		//OverlayElement[] elements;
		try {
			jsv1=jsv.object;
			name=jsv1["name"].str;
			koord=jsv1["koord"].array;
		}
		catch 
			return null;
		try
			typekoord=ConvTypeKoord[jsv1["typekoord"].str];
		catch
			{}
		try
			ovlret= new Overlay(name, NM, typekoord, koord[0].floating, 
		                    koord[1].floating, koord[2].floating, koord[3].floating);
		catch
			return null;
		if ("Elements" in jsv1)
			if (jsv1["Elements"].type == JSON_TYPE.ARRAY) {
				//elements=new OverlayElement[jsv1["Elements"].array.length];
				foreach (JSONValue jsv2; jsv1["Elements"].array) {
					OverlayElement ovel=OverlayElement.fromJSON(jsv2, NM);
					if (ovel) {
						ovlret.attachElement(ovel);
					}
				}
				//elements=elements[0 .. i];
			}
		return ovlret;
	}
}

class LoadedOverlays : LoadedResources {

	 NitroManager NM;
	
	this(ResourceNames resources, NitroManager NM) {
		super(resources);
		this.NM=NM;
	}
	
    override protected bool load1(string name) {
        string filename;
        Overlay newovl;
		JSONValue jsv;
        try 
            filename=resources.getfilenameres(name, Tip_resursa);
        catch (ResourseException) {
            log(log_module, LogUrovni.Error, format("Отсутствует файл оверлея %s", name));
            return false;
        }
		string text1=readText(filename);
		try 
			jsv=parseJSON(text1);
        catch {
            log(log_module, LogUrovni.Error, format("Ошибка в файле оверлея %s", filename));
            return false;
        }
		newovl=Overlay.fromJSON(jsv, NM);
		if (newovl) {
			res[name]=newovl;
			return true;
		}
		else
			return false;
	}
    
}
