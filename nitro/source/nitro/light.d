import std.string;
import derelict.opengl.gl;
import matrix, vector, movableobject;
import logging;

enum    log_module="light";

public class Light : MovableObject {

	public static const int GLLIGHTS[]=
		[GL_LIGHT0,
		 GL_LIGHT1,
		 GL_LIGHT2,
		 GL_LIGHT3,
		 GL_LIGHT4,
		 GL_LIGHT5,
		 GL_LIGHT6,
		 GL_LIGHT7];
		
	private int numlight;
    private float[] lightAmbient = [ 0.2f, 0.2f, 0.2f, 1.0f ];
    private float[] lightDiffuse = [ 1.0f, 1.0f, 1.0f, 1.0f ];
    private float[] lightSpecular = [ 1.0f, 1.0f, 1.0f, 1.0f ];
    private float[] lightPosition = [ 0.0f, 0.0f, 0.0f, 1.0f ];
    
    private bool izmAmbient=true;
    private bool izmDiffuse=true;
    private bool izmSpecular=true;
    
    private bool mEnabled=true;
	
	public this(string name, int numLight) {
		super(name);
		this.numlight=numLight;
	}
	
	public void setAmbient(float ambient[]){
		lightAmbient=ambient;
		izmAmbient=true;
	}

	public void setDiffuse(float diffuse[]){
		lightDiffuse=diffuse;
		izmDiffuse=true;
	}

	public void setSpecular(float specular[]){
		lightSpecular=specular;
		izmSpecular=true;
	}
	
    public void setEnable(bool enabled){
    	mEnabled=enabled;
    }

    public bool getEnable(){
    	return mEnabled;
    }

    public void flipVisibility(){
    	mEnabled= ! mEnabled;
    }
	
	//public void setPosition(float Position[]){
	//	lightDiffuse=Position;
	//}
	
	override public void draw(){
		if (mEnabled){
	         glEnable(GLLIGHTS[numlight]); 
	         Matrix4 mat=mParent.getDerivedMatrix();
             log(log_module, LogUrovni.Debug, format("Матрица светильника %s", mat.toString()));
	         Vector3 pos=mat.Position();
	         lightPosition[0]=pos.x;
	         lightPosition[1]=pos.y;
	         lightPosition[2]=pos.z;
	         glLightfv(GLLIGHTS[numlight], GL_POSITION, &lightPosition[0]);
	         if (izmAmbient){
	        	 glLightfv(GLLIGHTS[numlight], GL_AMBIENT, &lightAmbient[0]);
	        	 izmAmbient=false;
	         }
	         if (izmDiffuse){
	        	 glLightfv(GLLIGHTS[numlight], GL_DIFFUSE, &lightDiffuse[0]);
	        	 izmDiffuse=false;
	         }
	         if (izmSpecular){
	        	 glLightfv(GLLIGHTS[numlight], GL_SPECULAR, &lightSpecular[0]);
	        	 izmSpecular=false;
	         }
             log(log_module, LogUrovni.Debug, format("Включен свет с позицией %s", lightPosition));
		}
		else
	         glDisable(GLLIGHTS[numlight]); 
		
	}

}
