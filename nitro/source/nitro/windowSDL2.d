﻿module windowSDL2;

import std.stdio, std.conv, std.string, std.file, std.path, std.process;
import derelict.opengl.gl, derelict3.sdl2.sdl, derelict3.sdl2.image, derelict3.sdl2.ttf;

import constanty, logging, loadconfig;

enum    log_module="windowSDL2";
  
private struct WindowVar {
    int window_width=window_width_default;
    int window_height=window_height_default;
    uint flag_fullscreen=0;
	int flag_vsync=0;
    int desktopmode=1;
    string window_name=window_name_default; 
    uint windowID=0;
};
  
shared private WindowVar windowvar;
//private SDL_Window* windowvar_window;
//shared private uint windowID;
    
void initializevars() {
	with (windowvar) {
		window_width = to!(int) (configvarsl.get("window_width", window_width_default));
		window_height= to!(int) (configvarsl.get("window_height", window_height_default));
		window_name=configvarss.get("window_name", window_name_default);
		desktopmode=configvarsl.get("desktopmode", desktopmode_default)? 1 : 0;
        uint flag_fullscreen_desktop=desktopmode ? SDL_WINDOW_FULLSCREEN_DESKTOP : SDL_WINDOW_FULLSCREEN;
		flag_fullscreen=configvarsl.get("fullscreen", flag_fullscreen_default) ? flag_fullscreen_desktop : 0;
		flag_vsync=configvarsl.get("vsync", flag_vsync_default) ? 1 : 0 ;
	}
}    
    
void initSDL() {
	//string cwd=getcwd();
	//log(log_module, LogUrovni.Info, format("Текущий каталог %s", cwd));
    //DerelictSDL2.load("lib\\SDL2.dll"); 
    //DerelictSDL2Image.load("lib\\SDL2_image.dll, lib\\libpng16-16.dll, lib\\libjpeg-9.dll");
    DerelictSDL2.load(); 
    DerelictSDL2Image.load();
	log(log_module, LogUrovni.Info, "Шаг 1. Загружен SDL");
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        throw new Exception(": Could not init Video Mode. Error in SDL_Init().");
	log(log_module, LogUrovni.Info, "Шаг 2. Инициализирован SDL");
	//chdir("lib");
	DerelictSDL2ttf.load();
	int retttf=TTF_Init();
	if (retttf<0)
		log(log_module, LogUrovni.Error, "Библиотека SDL2ttf не проинициализировалась!");
	//chdir(cwd);
	
}
	
	
 //Для Derelict3
bool initGLwindow() {
	initSDL();
    DerelictGL.load();
    initializevars();
    //int window_width = to!(int) (configvarsl.get("window_width", window_width_default));
    //int window_height= to!(int) (configvarsl.get("window_height", window_height_default));
    //const(char)* window_name2=toStringz(window_name1);
    const(char)* window_name2=toStringz(windowvar.window_name);
    auto mainwindow = SDL_CreateWindow(window_name2, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        windowvar.window_width, windowvar.window_height, 
        windowvar.flag_fullscreen | SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN ); //| SDL_WINDOW_RESIZABLE);
    //mainwindow = null;
    if (!mainwindow) // Die if creation failed 
        {
        log(log_module, LogUrovni.Error, "Окно не создалось. Выходим.");
        sdldie("Unable to create window");
        return false;
        }
	log(log_module, LogUrovni.Info, format("Шаг 4. Создано окно размером %sx%s, с именем %s", 
        windowvar.window_width, windowvar.window_height, windowvar.window_name));
    auto maincontext = SDL_GL_CreateContext(mainwindow);
	log(log_module, LogUrovni.Info, "Шаг 5. Создан SDL_GL-контекст");
    //Derelict3GL3_reload();
    Derelict2GL1_reload();  // Используем старый OpenGL
    SDL_GL_SetSwapInterval(windowvar.flag_vsync);
    //windowvar_window=mainwindow;
    windowvar.windowID=SDL_GetWindowID(mainwindow);
    //LogDisplayModes(mainwindow);
    
    // Выясняем, какой видеорежим получился на самом деле
    SDL_DisplayMode mode;
    auto display_index=SDL_GetWindowDisplayIndex(mainwindow);
	SDL_GetCurrentDisplayMode(display_index, &mode);
	log(log_module, LogUrovni.Info, format("Текущий видеорежим:  %sx%s %sHz %s bpp", mode.w, mode.h, mode.refresh_rate, SDL_BITSPERPIXEL(mode.format)));
    // Заполняем длину и ширину окна реальными значениями
    if (windowvar.flag_fullscreen) {
        windowvar.window_width=mode.w;
        windowvar.window_height=mode.h;
    }
	//SDL_ShowCursor(SDL_DISABLE);
	SDL_SetRelativeMouseMode(true);
	
	
    return true;
}

/*
void Derelict3GL3_reload() {
    GLVersion glver = DerelictGL3.reload();
    //writeln("GLVersion=",glver);
	log(log_module, LogUrovni.Info, format ("Шаг 6. Перезагружен GL3. Версия GL = %s ", glver));
}
*/

void Derelict2GL1_reload() {
    GLVersion glver;
    try
        glver=DerelictGL.loadClassicVersions(GLVersion.GL21);
    catch
        glver=GLVersion.GL11;
    //GLVersion glver=DerelictGL.maxVersion();
	log(log_module, LogUrovni.Info, format ("Версия GL = %s ", glver));
}

void SDL_Quit() {
    derelict3.sdl2.sdl.SDL_Quit();
}

void sdldie(const char *msg)
{
    //writeln("%s: %s\n", msg, SDL_GetError());
    SDL_Quit();
    throw new Exception("Error: " ~ to!(string)(msg));
}

void RestoreWindowSize() {
    log(log_module, LogUrovni.Info, "Восстанавливаем окно");
    auto window=SDL_GetWindowFromID(windowvar.windowID);
    SDL_SetWindowSize(window, windowvar.window_width, windowvar.window_height);
}

uint GetWindowFlags() {
    auto window=SDL_GetWindowFromID(windowvar.windowID);
    return SDL_GetWindowFlags(window);
}

void SwapWindow() {
    auto window=SDL_GetWindowFromID(windowvar.windowID);
	//auto window=windowvar_window;
    SDL_GL_SwapWindow(window);
}

int GetWindowWidth() {
    return windowvar.window_width;
}

int GetWindowHeight() {
    return windowvar.window_height;
}

void WarpMouseInCenterWindow() {
	int x=windowvar.window_width / 2;
	int y=windowvar.window_height / 2;
	SDL_WarpMouseInWindow(null, x, y);
}

void GetCenterWindow(ref int x, ref int y) {
	x=windowvar.window_width / 2;
	y=windowvar.window_height / 2;
} 

void LogDisplayModes(SDL_Window*  mainwindow) {
    SDL_DisplayMode mode;
	int displaymodes=SDL_GetNumDisplayModes(0);
	log(log_module, LogUrovni.Info, format("Доступно %s видеорежимов", displaymodes));
    for (int j = 0; j < displaymodes; ++j) {
       SDL_GetDisplayMode(0, j, &mode);
       log(log_module, LogUrovni.Info, format("  Mode %s:  %sx%s %sHz %s bpp", j, mode.w, mode.h, mode.refresh_rate, SDL_BITSPERPIXEL(mode.format)));
    }
	SDL_GetCurrentDisplayMode(0, &mode);
	log(log_module, LogUrovni.Info, format(" Current Mode:  %sx%s %sHz %s bpp", mode.w, mode.h, mode.refresh_rate, SDL_BITSPERPIXEL(mode.format)));
	
}

void ChangeDisplayMode(SDL_Window*  mainwindow, int nummode) {
    SDL_DisplayMode mode;
	SDL_GetDisplayMode(0, nummode, &mode);
    if (windowvar.flag_fullscreen) {
		int rez=SDL_SetWindowDisplayMode(mainwindow, &mode);
        SDL_SetWindowFullscreen(mainwindow, 0);
        //SDL_Delay(5);
        SDL_SetWindowFullscreen(mainwindow, windowvar.flag_fullscreen);
        //SDL_Delay(5);
		if (rez!=0) 
			log(log_module, LogUrovni.Info, "Не удалось установить режим экрана");
		else {
			SDL_GetCurrentDisplayMode(0, &mode);
			log(log_module, LogUrovni.Info, format(" New Mode:  %sx%s %sHz %s bpp", mode.w, mode.h, mode.refresh_rate, SDL_BITSPERPIXEL(mode.format)));
			}
	}	

}

// Для Derelict2
/*
bool initGLwindow() {
    DerelictSDL.load(); 
    DerelictGL.load();
	log(log_module, LogUrovni.Info, "Шаг 1. Загружены SDL и GL");
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        throw new Exception(": Could not init Video Mode. Error in SDL_Init().");
	log(log_module, LogUrovni.Info, "Шаг 3. Инициализирован SDL");
    int window_width = to!(int) (configvarsl.get("window_width", window_width_default));
    int window_height= to!(int) (configvarsl.get("window_height", window_height_default));
    string window_name1=configvarss.get("window_name", window_name_default);
    const(char)* window_name2=toStringz(window_name1);    
    if (SDL_SetVideoMode(window_width, window_width, 0, SDL_OPENGL | SDL_GL_DOUBLEBUFFER  ) is null) // | SDL_FULLSCREEN * Config["fullscreen"]
        throw new Exception(__FILE__ ~ ": Could not Set Video Mode. Error in SDL_SetVideoMode().");
    SDL_WM_SetCaption( window_name2, window_name2 );
	log(log_module, LogUrovni.Info, format("Шаг 4. Создано окно размером %sx%s, с именем %s", window_width, window_height, window_name1));
    GLVersion glver;
    try
        glver=DerelictGL.loadClassicVersions(GLVersion.GL12);
    catch
        glver=GLVersion.GL11;
    //GLVersion glver=DerelictGL.maxVersion();
	log(log_module, LogUrovni.Info, format ("Версия GL = %s ", glver));
    return true;
}
*/

