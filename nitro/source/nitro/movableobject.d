module movableobject;

import node;

public class MovableObject {

	public Node mParent=null;
	protected string mName;

	public this(string name){
		mName=name;
	}

	public string getName(){
		return mName;
	}
	
	public void setParent(Node nd){
		mParent=nd;
	}
	
	abstract public void draw();

	public int Graney(){
		return 0;
	}
	
}
