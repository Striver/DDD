module event;

import std.string;
import derelict3.sdl2.sdl; 

import windowSDL2;
private import logging;

enum log_module="event";


   
class ObrEvents {
    protected bool delegate(SDL_Event e) [SDL_EventType]    func1;
    protected bool delegate(SDL_Event e) [SDL_EventType][int]    func2;
    
    this() {
        _RegFunc1();
        _RegFunc2();
    }
    
    protected void _RegFunc1() {
        Register_Fun1(SDL_QUIT, &Ob_Quit);
        Register_Fun1(SDL_KEYUP, &Ob_Keyup);
        Register_Fun1(SDL_KEYDOWN, &Ob_Keydown);
        Register_Fun1(SDL_WINDOWEVENT, &Ob_Windowevent);
    }

    protected void _RegFunc2() {
        func2[SDL_KEYUP][SDLK_ESCAPE]=&Ob_Keyup_ESCAPE;
        //func2[SDL_KEYDOWN][-1]=&Ob_Zaglushka;
        func2[SDL_WINDOWEVENT][SDL_WINDOWEVENT_MINIMIZED]=&Ob_Windowevent_Minimized;
        func2[SDL_WINDOWEVENT][SDL_WINDOWEVENT_EXPOSED]=&Ob_Windowevent_Exposed;
    }

    bool obr_event() {
		return obr_event_all();
	}
    
    bool obr_event1() {
        SDL_Event e;
        if (SDL_PollEvent(&e))
            return func1.get(e.type, &Ob_Zaglushka)(e);
            //return func1.get(e, &Ob_Zaglushka)(e);
        return false;
    }
	
    bool obr_event_all() {
        SDL_Event e;
		bool ret = false;
        while (SDL_PollEvent(&e))
            ret = ret || func1.get(e.type, &Ob_Zaglushka)(e);
            //return func1.get(e, &Ob_Zaglushka)(e);
        return ret;
    }
    
    bool Ob_Zaglushka(SDL_Event e) {
        return false;
    }
    
    bool Ob_Keyup(SDL_Event e) {
        log(log_module, LogUrovni.Debug, format("Отжата клавиша Keycode=%s", e.key.keysym.sym));
        //log(log_module, LogUrovni.Info, format("SDL_USEREVENT =%s", SDL_USEREVENT));
        /*
        if (e.key.keysym.sym == SDLK_ESCAPE) {
             log(log_module, LogUrovni.Info, "Сработала клавиша Esc");
             return true;
        }        
        return false;
        */
        return func2[e.type].get(e.key.keysym.sym, &Ob_Zaglushka)(e);
    }
    
    bool Ob_Keyup_ESCAPE(SDL_Event e) {
        log(log_module, LogUrovni.Info, "Сработала клавиша Esc");
        return true;
    }
    
    bool Ob_Keydown(SDL_Event e) {
        log(log_module, LogUrovni.Debug, format("Нажата клавиша Keycode=%s", e.key.keysym.sym));
        //log(log_module, LogUrovni.Info, format("Код клавиши вправо=%s", SDLK_RIGHT));
        return func2[e.type].get(e.key.keysym.sym, &Ob_Zaglushka)(e);
        //return false;
    }
    
    bool Ob_Quit(SDL_Event e) {
        log(log_module, LogUrovni.Info, "Окно закрыто пользователем");
        return true;
    }
    
    bool Ob_Windowevent(SDL_Event e) {
        log(log_module, LogUrovni.Debug, format("Событие SDL_WINDOWEVENT №%s", e.window.event));
        //log(log_module, LogUrovni.Info, format("Тип события окна =%s", typeof(e.window.event)));
        /*
        switch (e.window.event) {
            case SDL_WINDOWEVENT_MINIMIZED:
                log(log_module, LogUrovni.Info, "Окно минимизировано");
                break;
            case SDL_WINDOWEVENT_EXPOSED:
                auto windowstate=GetWindowFlags();
                if (!(SDL_WINDOW_MINIMIZED & windowstate)) {
                   RestoreWindowSize();
                }
                break;
            default:
                break;
        }
        return false;
        */
        return func2[e.type].get(e.window.event, &Ob_Zaglushka)(e);
    }

    bool Ob_Windowevent_Minimized(SDL_Event e) {
        log(log_module, LogUrovni.Info, "Окно минимизировано");
        return false;
    }
    
    bool Ob_Windowevent_Exposed(SDL_Event e) {
        auto windowstate=GetWindowFlags();
        if (!(SDL_WINDOW_MINIMIZED & windowstate)) 
            RestoreWindowSize();
        return false;
    }
    
    //bool Ob_Userevent(SDL_Event e) {
    //    return false;
    //}

    void Register_Fun1(SDL_EventType typ, bool delegate(SDL_Event e) fun) {
        func1[typ]=fun;
        func2[typ][-1]=&Ob_Zaglushka;
    }
    
    void UnRegister_Fun1(SDL_EventType typ) {
        func1.remove(typ);
        func2.remove(typ);
    }
    
    void Register_Fun2(SDL_EventType typ, int eventcode, bool delegate(SDL_Event e) fun) {
        func2[typ][eventcode]=fun;
    }
    
    void UnRegister_Fun2(SDL_EventType typ, int eventcode) {
        func2[typ].remove(eventcode);
    }
}

/*
 private   bool obr_event() {
        //Our event structure
		SDL_Event e;
		//For tracking if we want to quit
		bool quit = false;
		if (SDL_PollEvent(&e)){
				//If user closes the window
				if (e.type == SDL_QUIT){
					quit = true;
				}
				//If user presses any key
				if (e.type == SDL_KEYUP){
                    log(log_module, LogUrovni.Info, format("Нажата клавиша Keycode=%s", e.key.keysym.sym));
                    if (e.key.keysym.sym == SDLK_ESCAPE) {
                        log(log_module, LogUrovni.Info, "Нажата клавиша Esc");
                        quit = true;
                    }
				}
				//If user clicks the mouse
				//if (e.type == SDL_MOUSEBUTTONDOWN){
				//	quit = true;
				//}
				//If user presses any key
                if (e.type == SDL_WINDOWEVENT) {
                    log(log_module, LogUrovni.Debug, format("Событие SDL_WINDOWEVENT №%s", e.window.event));
                    switch (e.window.event) {
                    case SDL_WINDOWEVENT_MINIMIZED:
                        log(log_module, LogUrovni.Info, "Окно минимизировано");
                        break;
                    case SDL_WINDOWEVENT_SHOWN, SDL_WINDOWEVENT_MAXIMIZED: 
                        break;
                    case SDL_WINDOWEVENT_EXPOSED:
                        //auto windowstate=SDL_GetWindowFlags(mainwindow1);
                        auto windowstate=GetWindowFlags();
                        if (!(SDL_WINDOW_MINIMIZED & windowstate)) {
                           //SDL_SetWindowSize(mainwindow1, 1024, 768);
                           RestoreWindowSize();
                            //SDL_RestoreWindow(mainwindow1);
                           //onDrawFrame();
                        }
                        break;
                    default:
                        break;
                    }
                }
            //log(log_module, LogUrovni.Debug, format("Нод mBaseNode:%s", mBaseNode.getPosition()));
			}
        return quit;
    }
 */