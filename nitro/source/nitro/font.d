module font;

import derelict3.sdl2.sdl, derelict3.sdl2.ttf;
import std.string, core.stdc.string, std.conv, std.json, std.file;
//import std.string : fromStringz;

import resources, texturesymbolattributes;
import logging;

enum log_module="font";
enum Tip_resursaSDL="SDLfont";
enum Tip_resursaTexattr="texfontattr";

enum Razdelitel = "_";

inout(char)[] fromStringz(inout(char)* cString) @system pure {
    return cString ? cString[0 .. strlen(cString)] : null;
}

class SDLFont : LoadableResource {

private:

	TTF_Font *gFont;
	
public:   
    
    void load_fromFile(string filename, int size) {
    
        log(log_module, LogUrovni.Debug, format("Щас шрифт %s грузить будем!", filename));
		gFont = TTF_OpenFont( toStringz(filename), size );
		if (gFont is null) {
			auto error = fromStringz(TTF_GetError());
			log(log_module, LogUrovni.Error, format("Шрифт не загрузился. Ошибка: %s ", error));
		}
    
        log(log_module, LogUrovni.Debug, format("Шрифт %s_%s загрузили!", filename, size));
   
    }

   
    public TTF_Font* opCall() {
		return gFont;
    }
	
	~this() {
		if (!(gFont is null))
			TTF_CloseFont( gFont );
	}
    
}

class LoadedSDLFonts : LoadedResources {
	
	this(ResourceNames resources) {
		super(resources);
	}
	
    override protected bool load1(string name) {
        string filename;
        SDLFont newfont;
		int size, ind_;
		ind_ = lastIndexOf(name, Razdelitel);
		if (ind_ < 0) {
			log(log_module, LogUrovni.Error, format("Имя шрифта %s неверно построено.", name));
			return false;
		}
		filename = name[0 .. ind_];
		try
			size=to!int(name[ind_+1 .. $]);
		catch {
			log(log_module, LogUrovni.Error, format("Размер шрифта %s не определяется.", name));
			return false;
		}
        try 
            filename=resources.getfilenameres(filename, Tip_resursaSDL);
        catch (ResourseException) {
            log(log_module, LogUrovni.Error, format("Отсутствует файл шрифта %s", name));
            return false;
        }
        newfont=new SDLFont();
        try 
			newfont.load_fromFile(filename, size);
        catch {
            log(log_module, LogUrovni.Error, format("Ошибка в файле шрифта %s", filename));
            return false;
        }
        //meshes[name]=newmesh;
		res[name]=newfont;
        return true;
    }
    
}

class TextureFontAttr : LoadableResource {

private: 
	TextureSymbolAttributes[string] attributes;
	int fontHeight;
	string name;
	
	// Приватный конструктор. Класс создаём только из JSON-файла
	this(string name, int Height, TextureSymbolAttributes[string] attr){
		attributes=attr;
		this.name=name;
		fontHeight=Height;
	
	
}
public:   
    
	// --- принимаем атрибуты из файла в формате JSON ----
	static public TextureFontAttr fromJSON(JSONValue jsv) {
		JSONValue[string] jsv1, jsv2;
		string fontname;
		int fontHeight;
		try {
			jsv1=jsv.object;
			fontname=jsv1["fontname"].str;
			fontHeight=to!int(jsv1["fontHeight"].integer);
			jsv2=jsv1["symbols"].object;
		}	
		catch {
			log(log_module, LogUrovni.Error, "Файл атрибутов шрифта не соответсвует формату");
			log(log_module, LogUrovni.Debug, format("fontname=%s, fontHeight=%s, symbols=%s", 
				fontname, fontHeight, jsv2));
			return null;
		}
		TextureSymbolAttributes[string] list;
		foreach (sym, attr; jsv2) {
			try {
				JSONValue[] arr=attr.array;
				short[7] a;
				for (int i=0; i<arr.length; i++) 
					a[i]=to!(short)(arr[i].integer);
				list[sym]=TextureSymbolAttributes(a);
			}
			catch
				log(log_module, LogUrovni.Warning, format("Атрибут символа \"%s\" не прочитался",sym));
		}
		TextureFontAttr ret= new TextureFontAttr(fontname, fontHeight, list);
		return ret;
	}

	TextureSymbolAttributes opIndex(string sym) {
		return attributes[sym];
	}
	
	@property int Height() {return fontHeight;}
    
}

class LoadedTextureFontAttr : LoadedResources {
	
	this(ResourceNames resources) {
		super(resources);
	}

    override protected bool load1(string name) {
        string filename;
        TextureFontAttr newTexattr;
		JSONValue jsv;
        try 
            filename=resources.getfilenameres(name, Tip_resursaTexattr);
        catch (ResourseException) {
            log(log_module, LogUrovni.Error, format("Отсутствует файл атрибутов шрифта %s", name));
            return false;
        }
		string text1=readText(filename);
		try 
			jsv=parseJSON(text1);
        catch {
            log(log_module, LogUrovni.Error, format("Ошибка в файле атрибутов шрифта %s", filename));
            return false;
        }
		newTexattr=TextureFontAttr.fromJSON(jsv);
		if (newTexattr) {
			res[name]=newTexattr;
			return true;
		}
		else
			return false;
	}
}