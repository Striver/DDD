import derelict.opengl.gl;
import movableobject, mesh, material;

public class Entity : MovableObject {
	
	protected Mesh mMesh;
    protected Material mMaterial;
	//protected int mTexture;
	//private bool prozrachnost=false;

	public this(string name, Mesh me) {
		super(name);
		// TODO Auto-generated constructor stub
		
		mMesh=me;
		//mTexture=0;
        mMaterial= new Material();
	}
	
	public void newMesh(Mesh me){
		mMesh=me;
	}
	
	//public void setTexture(int texture){
	//	mTexture=texture;
	//}
	
	//public void setProzracnost(bool pr){
	//	prozrachnost=pr;
	//}
	
	override public void draw(){
		//glBindTexture(GL_TEXTURE_2D, mTexture);
        mMaterial.draw();
		mMesh.draw();
		
		//System.out.println("draw Entity "+mName);
		
	}
	
    public Material getMat() {
        return mMaterial;
    }
    
	override public int Graney(){
		return mMesh.Graney();
	}
}
