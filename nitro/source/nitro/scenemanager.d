import derelict.opengl.gl;
import std.file, std.string;
import logging, loadconfig;
import node, entity, mesh, resources, light;

enum    log_module="scenemanager";

public class SceneManager {
	// Источники света
	public enum MAXLIGHTS=8;
	public Light ligths[MAXLIGHTS]=[null,null,null,null,null,null,null,null]; 
	
	// Меши
	public Mesh[string] mMeshi;
	
	// Прародитель всех нодов
	public Node mBaseNode;
	//private Context context;
	
    private ResourceNames res;
	
	
	public this() { //   Context context){
		mBaseNode=new Node("BaseNode");
        auto dirres=configvarsas["dirresources"];
        res=new ResourceNames(dirres);
		//mMeshi=new HashMap<Integer, Mesh>();
		//this.context=context;
	}
    
	public void LoadMeshes(string[] namemeshes){
		foreach (namemesh; namemeshes){
	        try {
                string filenamemesh=res.getfilenameres(namemesh, "mesh");
	        	mMeshi[namemesh]= new Mesh(filenamemesh);
			} catch (ResourseException e) 
				// TODO Auto-generated catch block
				log(log_module, LogUrovni.Error, format("Нет ресурса с именем %s", namemesh));
			//} catch (IOException e) {
			//	// TODO Auto-generated catch block
			//	e.printStackTrace();
			//}
		}
	}

	
	public Mesh getMesh(string resource){
        return mMeshi.get(resource, null);
	}
	
	public Entity createEntity(string name, string meshresource, int texture){
		Mesh mesh=this.getMesh(meshresource);
		if (mesh !is null){
			Entity ent=new Entity(name, mesh);
			//ent.setTexture(texture);
			return ent;
		}
		else return null;
	}
	
	private int getNewNumLight(){
		for (int i=0; i<MAXLIGHTS;i++)
			if (ligths[i] is null){
				return i;
			}
		return -1;
	}
	

	public Light createLight(string name){
		int num=getNewNumLight();
		if (num<0) return null;
		ligths[num]=new Light(name, num);
		return ligths[num];
	}
	public void freeLight(int num){
		if (num<MAXLIGHTS){
			
			ligths[num]=null;
		}
	}
}
