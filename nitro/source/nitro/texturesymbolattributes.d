﻿module texturesymbolattributes;

import std.stdio, std.string;

enum log_module="texSymAttr";
    
struct TextureSymbolAttributes {
	private short m[7];
	
	@property ref short x() {return m[0];}
	@property ref short y() {return m[1];}
	@property ref short minx() {return m[2];}
	@property ref short maxx() {return m[3];}
	@property ref short miny() {return m[4];}
	@property ref short maxy() {return m[5];}
	@property ref short width() {return m[6];}
	
	this(short[7] par) {
		m=par;
	}

	ref short opIndex(int ind) {
		return m[ind];
	}
	
	string toString() {
		return format("SymAttr(%s)",m);
	}
	
	short[] toArray() {
		return m;
	}
}

void proverkaSA() {
	TextureSymbolAttributes sym1=TextureSymbolAttributes([1,1,1,1,1,1,1]);
	TextureSymbolAttributes sym2=TextureSymbolAttributes([10,20,30,40,50,60,70]);
	TextureSymbolAttributes sym3;
	
	writeln("sym1=", sym1, ", sym2=", sym2);
	sym1.x=15;
	writeln("sym1=", sym1);
	writeln("sym1.x=", sym1.x);
	writeln("sym2.width=", sym2.width);
	writeln("sym3=", sym3);
	sym3=sym2;
	writeln("sym3=", sym3);
	sym2.width=5;
	writeln("sym2=", sym2);
	writeln("sym3=", sym3);
	writeln("sym1[0]=", sym1[0]);
	sym1[1]=25;
	writeln("sym1[1]=", sym1[1]);
	
}
