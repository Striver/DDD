﻿module logging;

import std.stdio, std.datetime, std.concurrency, std.string, std.conv, std.container, std.utf;

// --- Уровни журналирования -------------------------------
enum LogUrovni {CriticalError, Error, Warning, Info, Debug};

// --- Отображение уровня конкретного сообщения ------------
enum string[LogUrovni] Soobch_Uroven = [
    LogUrovni.CriticalError:"Крит.ошибка",
    LogUrovni.Error        :"Ошибка     ",
    LogUrovni.Warning      :"Предупрежд.",
    LogUrovni.Info         :"Информация ",
    LogUrovni.Debug        :"Отладка    "
    ];

	
// --- Основная функция инициализации системы журналирования -------------  
// logfilename   - имя файла журнала
// obchii_uroven - текущий уровень журналирования для большинства модулей
// moduluroven   - ассоциативный массив уровней для отдельных модулей
void loginit(string logfilename, LogUrovni obchii_uroven, LogUrovni[string] moduluroven) {
    with (logvar) {
        tek_obchii_uroven=obchii_uroven;
        //writeln("  tek_obchii_uroven=", tek_obchii_uroven);
        modul_uroven=cast(shared(LogUrovni[string])) moduluroven;
        //writeln("prisvoili peremennye");
        //logobject=new LogObject(logfilename, 40, 20);
        auto tid_logpotok1=spawn(&funlogpotok, logfilename, 200, 50);
        //writeln("zapushen potok");
        bool retreg=register(namelogpotok, tid_logpotok1);
        tid_logpotok=tid_logpotok1;
        //writeln("prisvoili peremennye");
        setMaxMailboxSize(tid_logpotok1, 300, OnCrowding.block);
        ticksPerSecf=TickDuration.ticksPerSec;
        long currAppTick=Clock.currAppTick().length;
        float seconds=currAppTick/ticksPerSecf;
		
		tid_logpotok1.send(thisTid, currAppTick, format("%10.5f  ",seconds) ~ zagolovok());
		
    }                    
}

// --- Массив названий модулей, у которых при конвертации в функции
// --- convmoduluroven произошла ошибка
string[] modulurovenconverror;

// --- Функция конвертации из массива сос строковым написанием уровней 
// --- в массив с уровнями LogUrovni, пригодный для функции loginit
// ---  string[string] -> LogUrovni[string]
// --- побочное действие: в случае ошибок названия соотвествующих модулей
// --- сохраняется в массиве converror 
LogUrovni[string] convmoduluroven(string[string] strmoduluroven) {
    LogUrovni[string] ret;
    foreach (modul, uroven; strmoduluroven) {
        try 
            ret[modul]=ConvLogUrovni[uroven];
        catch //(AttributeError)
            modulurovenconverror ~= modul;
    }
    return ret;
}

// --- Функция инициализации с аргументами со строковым написанием --------
void loginitstr(string logfilename, string str_obchii_uroven, string[string] strmoduluroven) {
    LogUrovni[string] moduluroven=convmoduluroven(strmoduluroven);
    LogUrovni obchii_uroven=obchii_uroven_default;
    bool error_obchii_uroven=false;
    
    try
        obchii_uroven=ConvLogUrovni[str_obchii_uroven];
    catch { //(AttributeError) {
        obchii_uroven=obchii_uroven_default;
        error_obchii_uroven=true;
        }
    
    loginit(logfilename, obchii_uroven, moduluroven);
    //init(logfilename, obchii_uroven_default, moduluroven);
    if (error_obchii_uroven)
        tid_logpotok.send(thisTid, Clock.currAppTick().length, 
             "Конвертация общего уровня журналирования прошла с ошибкой, используется уровень по-умолчанию. \n");
    if (modulurovenconverror.length>0)
        tid_logpotok.send(thisTid, Clock.currAppTick().length, 
             format("Следующие уровни будут журналироваться общим уровнем %s. \n", modulurovenconverror));
    
}

// --- Основная функция записи события в журнал -------------------
// logmodule - предполагается имя вызывающего модуля. Влияет только в том случае,
//       если такое имя присутствует в массиве moduluroven в функции инициализации;
//       но в любом случае записывается в графу "модуль".
// uroven - уровень события. Если уровень ниже определённого для этого модуля,
//       то запись в журнал не производится.
// zapis - любой текст, попадающий в графу "Сообщение"
void log(string logmodule, LogUrovni uroven, lazy string zapis) {
    with (logvar) {
        LogUrovni tek_uroven=modul_uroven.get(logmodule, tek_obchii_uroven);
        //writeln("tek_uroven=", tek_uroven, "  tek_obchii_uroven=", tek_obchii_uroven);
        //writeln("modul_uroven=", modul_uroven);
        if (uroven<=tek_uroven) {
            long currAppTick=Clock.currAppTick().length;
            //float ticksPerSecf=TickDuration.ticksPerSec;
            float seconds=currAppTick/ticksPerSecf;
            //tid_logpotok.send(thisTid, currAppTick, (format("%10.5f| ", seconds)) ~ logmodule ~ "\t|" ~ Soobch_Uroven[uroven] ~ " | " ~ zapis ~ "\n");
            locate(namelogpotok).send(thisTid, currAppTick, 
			  (format("%10.5f| ", seconds)) ~ format(format_name_modul, logmodule) ~ 
			     "|" ~ Soobch_Uroven[uroven] ~ " | " ~ zapis ~ "\n");
        }
    }
}

// --- Форматирование многострочных записей
// --- Для красоты журнала
string logformatmultiline(string zapis) {
	string[] lines=splitLines(zapis);
	string retstr=lines[0];
	foreach (line; lines[1 .. $])
		retstr~= logvar.sdvigZapisey ~ line;
	return retstr;
}

// --- Формирование заголовка журнала -----
// --- дополнительные действия - инициализация переменных 
// --- logvar.format_name_modul и logvar.nachaloZapisey    
private string zagolovok() {

	enum WidthNameModul=12;	

	enum stroka1="Начало работы журнала\n";
	enum razdelitel="-------------------------------------------------------\n";
	enum stroka21="   Время  |  ";
	enum stroka22="Модуль";
	enum stroka23="|  Уровень   | ";
	enum stroka24=" Сообщение \n";
	
	string formatnamemodul="%-" ~ format("%s", WidthNameModul+5) ~ "s";
	//writeln(formatnamemodul);
	string zagolovokmodul=format (formatnamemodul, stroka22);
	logvar.format_name_modul="%-" ~ format("%s", WidthNameModul) ~ "s";
	string stroka2=stroka21 ~ zagolovokmodul ~ stroka23;
	string formatPustoty= "%-" ~ format("%s", std.utf.count(stroka2)) ~ "s";
	logvar.sdvigZapisey="\n" ~ format(formatPustoty, " ");
	//writeln(logvar.sdvigZapisey, stroka2.length, std.utf.count(stroka2));
	return stroka1 ~ razdelitel ~ stroka2 ~ stroka24 ~ razdelitel;
}

//-----------------------------------------------------------
// Внутренняя часть 
//------------------------------------------------------------

private enum LogUrovni[string] ConvLogUrovni = [
    "CriticalError":LogUrovni.CriticalError,
    "Error"        :LogUrovni.Error        ,
    "Warning"      :LogUrovni.Warning      ,
    "Info"         :LogUrovni.Info         ,
    "Debug"        :LogUrovni.Debug        
    ];
    
// --- Структура с переменными, расшаренными на все потоки ---	
private struct LogVar {
    LogUrovni tek_obchii_uroven;
    LogUrovni[string] modul_uroven;
    float ticksPerSecf;
	string format_name_modul;	// Нужно для правильного расположения названия модуля в функции log
	string sdvigZapisey; 		// Нужно для функции logformatmultiline
}; 

// --- Экземпляр этой структуры ----
shared private LogVar logvar;

// --- Уровень логирования по-умолчанию --------
private enum obchii_uroven_default = LogUrovni.Warning;  
private enum namelogpotok="logpotok";

private Tid tid_logpotok;


private class LogObject {
    File logfile;
    long[]  kluchi;
    string[long] stroki;    
    int razmer_bufera=10, 
        razmer_vygruzki=5,
        zanjato=0;
    
    this(string logfilename, int razmer_bufera, int razmer_vygruzki) {
        this.razmer_bufera=razmer_bufera;
        if (razmer_vygruzki>razmer_bufera)
            this.razmer_vygruzki=razmer_bufera;
        else 
            this.razmer_vygruzki=razmer_vygruzki;
        kluchi= new long[razmer_bufera+1];
        //writeln("prisvoili peremennye v strukture");
        logfile=File(logfilename, "w");
        //writeln("otkryli fail");
        stroki=[0L:""];
        //writeln("prisvoili stroki");
        //stroki=stroki1;
    }
    
    ~this() {
        zapisat (zanjato);
        logfile.close();
    }
    
    void log(long vremja, string stroka) {
        if (zanjato==0) 
            kluchi[zanjato]=vremja;
        else
            if (kluchi[zanjato-1]< vremja)
                kluchi[zanjato]=vremja;
            else
                insertkluch(vremja);
        zanjato++;
        stroki[vremja]=stroka;
        if (zanjato>=razmer_bufera) zapisat(razmer_vygruzki);
    }
    
    void insertkluch(long vremja) {
        //writeln("zanjato=", zanjato);
        //writeln("vremja=", vremja);
        //writeln("kluchi=", kluchi);
        if (zanjato==razmer_bufera)
            writeln("perepolnenie bufera. ");
        int i;
        
        for (i=zanjato-1; i>=0; i--) //{
            if (kluchi[i]>vremja)
                kluchi[i+1]=kluchi[i];
            else 
                break;
        kluchi[i+1]=vremja;
        //writeln("kluchi=", kluchi);
    }
    
    void zapisat (int razmer) {
        if (razmer>zanjato) razmer=zanjato;
        string buffer="";
        foreach (vremja; kluchi[0..razmer]) {
            buffer~=stroki[vremja];
            stroki.remove(vremja);
        }
        //buffer~="--- Записан буфер ----\n";
        logfile.write(buffer);
        for (int i=razmer, j=0; i<zanjato; i++, j++)
            kluchi[j]=kluchi[i];
        zanjato=zanjato-razmer;
    }
}

private void funlogpotok(string logfilename, int razmer_bufera, int razmer_vygruzki) {
    auto logobject=new LogObject(logfilename, razmer_bufera, razmer_vygruzki);
    try {
        for (;;) {
            auto msg = receiveOnly!(Tid, long, string)();
            logobject.log(msg[1], msg[2]);
        }
    } catch (OwnerTerminated exc) {
        clear(logobject);
    }
 }
 
