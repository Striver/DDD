import std.stdio, std.file, std.string, std.conv, std.regex;

import constanty;

public double[string] configvarsd;
public long[string] configvarsl;
public string[string] configvarss;
public string[][string] configvarsas;
public long[][string] configvarsal;
public double[][string] configvarsad;
public string[string][string] configvarsdicts;


public string[] errorvars; 

private bool loaded=false;

bool readcfg(string filename) {
    string text=readText(filename);
    string[] text1=splitLines(text);
    string[string] text2;
    string stroka1, stroka2;
    foreach(string stroka; text1) {
        if (stroka.length<2)
            continue;
        if (stripLeft(stroka)[0..1]=="#")
            continue;
        int indexravno=indexOf(stroka, "=");
        if (indexravno<0) 
            continue;
        stroka1=strip1(stroka[0 .. indexravno]);
        stroka2=strip1(stroka[indexravno+1 .. $]);
        text2[stroka1]=stroka2;
    }
    //writeln("text2=",text2);
    errorvars=[];
    configT(text2, configvarsd, "ConfigVarsD");
    configT(text2, configvarsl, "ConfigVarsL");
    configT(text2, configvarss, "ConfigVarsS");
    configT(text2, configvarsas, "ConfigVarsAS");
    configT(text2, configvarsal, "ConfigVarsAL");
    configT(text2, configvarsad, "ConfigVarsAD");
    configT(text2, configvarsdicts, "ConfigVarsDictS");
    return true;
}

private void configT(T)(string[string] text2, ref T[string] cogfigvars, string namevars) {
    foreach(kluch; ConfigVars[namevars]) 
        if (kluch in text2) {
            try {
                T value=to!(T)(text2[kluch]);
                cogfigvars[kluch]=value;
            }
            catch (ConvException) {
                errorvars ~= kluch;
                writeln("ConvException. kluch=", kluch, "  value=", text2[kluch]);
                }
        }
        else
            errorvars ~= kluch;
}       

public string config_to_string() {
    string ret="";
    ret ~= format("configvarsd= %s \n", configvarsd);
    ret ~= format("configvarsl= %s \n", configvarsl);
    ret ~= format("configvarss= %s \n", configvarss);
    ret ~= format("configvarsad= %s \n", configvarsad);
    ret ~= format("configvarsal= %s \n", configvarsal);
    ret ~= format("configvarsas= %s \n", configvarsas);
    ret ~= format("configvarsdicts= %s", configvarsdicts);
    return ret;
}

private string strip1(string param) {
    string ret=strip(param);
    if ((ret[0]=='"') && (ret[$-1]=='"'))
        ret=ret[1 .. $-1];
    return ret;
}