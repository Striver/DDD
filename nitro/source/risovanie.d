import derelict.opengl.gl, derelict3.sdl2.sdl; 
import std.file, std.string, std.conv;
import logging;

import mesh, light, node, entity, windowSDL2;

enum    log_module="risovanie";

enum filenamemesh="res\\meshes\\bibizjana2.me";

//GLuint triangleVBO;

Node[] nodes;
//SDL_Window* mainwindow1;

void risovanie() { //SDL_Window* mainwindow) {
//void risovanie() {

    //mainwindow1=mainwindow;
    float[] lightAmbient = [ 0.2f, 0.2f, 0.2f, 1.0f ];
    float[] lightSpecular = [ 0.5f, 0.5f, 0.5f, 1.0f ];

    glEnable(GL_LIGHTING);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, &lightAmbient[0]);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    
    glMatrixMode (GL_MODELVIEW);
    

    //glClearColor ( 0.2, 0.0, 0.7, 1.0 );
    //glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    log(log_module, LogUrovni.Info, "Шаг 7. Отрисован фон");
    auto me=new Mesh(filenamemesh);
    log(log_module, LogUrovni.Info, "сформировали меш");
    auto ent=new Entity("ent1", me);
    auto node1= new Node("node1");
    node1.setPosition(0,0,0);
    node1.setScale(0.2f,0.2f,0.2f);
    node1.attachObject(ent);

    auto node0= new Node("node0");
    node0.setPosition(3,-1.5f,0);
    auto l0=new Light("light1", 0);
    node0.attachObject(l0);
    //l0.setParent(node0);
    l0.setDiffuse([0.5,0.5,0.2]);
    l0.setSpecular([0.2,0.2,0.2]);
    log(log_module, LogUrovni.Info, "сформировали свет0");
    
    nodes=[node0, node1];
    
    drawframe();
    
    obr_event();
    
}

void drawframe(){
    glClearColor ( 0.2, 0.0, 0.7, 1.0 );
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    log(log_module, LogUrovni.Info, "Шаг 8. Активированы массивы ");
    
    //glMaterialfv(GL_FRONT, GL_DIFFUSE,&lightSpecular[0]);
    //glMaterialfv(GL_FRONT, GL_SPECULAR,&lightSpecular[0]);
    glMaterialf(GL_FRONT, GL_SHININESS,100.0f);
    
    foreach(node; nodes) 
        node.draw();
    
    glDisableClientState(GL_NORMAL_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    //SDL_GL_SwapBuffers();
    //SDL_GL_SwapWindow(mainwindow1);
    SwapWindow();

}


void obr_event() {
    //Our event structure
	SDL_Event e;
	//For tracking if we want to quit
	bool quit = false;
	while (!quit){
		//Read any events that occured, for now we'll just quit if any event occurs
		while (SDL_PollEvent(&e)){
			//If user closes the window
			if (e.type == SDL_QUIT){
				quit = true;
			}
			//If user presses any key
			if (e.type == SDL_KEYUP){
                log(log_module, LogUrovni.Info, format("Нажата клавиша Keycode=%s", e.key.keysym.sym));
                if (e.key.keysym.sym == SDLK_ESCAPE) {
                    log(log_module, LogUrovni.Info, "Нажата клавиша Esc");
                    quit = true;
                }
			}
			//If user clicks the mouse
			//if (e.type == SDL_MOUSEBUTTONDOWN){
			//	quit = true;
			//}
			//If user presses any key
            if (e.type == SDL_WINDOWEVENT) {
                log(log_module, LogUrovni.Debug, format("Событие %s", e.window.event));
                switch (e.window.event) {
                case SDL_WINDOWEVENT_MINIMIZED:
                    log(log_module, LogUrovni.Info, "Окно минимизировано");
                    break;
                case SDL_WINDOWEVENT_SHOWN, SDL_WINDOWEVENT_MAXIMIZED: 
                    break;
                case SDL_WINDOWEVENT_EXPOSED:
                    //auto windowstate=SDL_GetWindowFlags(mainwindow1);
                    auto windowstate=GetWindowFlags();
                    if (!(SDL_WINDOW_MINIMIZED & windowstate)) {
                        log(log_module, LogUrovni.Info, "Восстанавливаем окно");
                       //SDL_SetWindowSize(mainwindow1, 1024, 768);
                       RestoreWindowSize();
                        //SDL_RestoreWindow(mainwindow1);
                       drawframe();
                    }
                    break;
                default:
                    break;
                }
            }
		}
	}

}
