﻿/++
Модуль, определяющий абстрактный класс $(LINK2 #Визуализатор, Визуализатор), 
используемый для реализации Игрового Цикла и вывода 3D-объектов и инфопанелей на экран.
+/
module ddd.vis;

import std.conv, std.string;
import ddd.okno, ddd.ddd_manager;

import my_utils.journal;

private enum имя_модуля = "визуализатор";

/++
Абстрактный класс, используемый для реализации Игрового Цикла и вывода 3D-объектов и инфопанелей на экран.

Реальный (не абстрактный) класс-потомок этого класса, для которого можно создать экземпляр - 
это класс $(B Визуализатор_gl) из модуля $(B ddd.zavisimost.vis_gl). Пользователю не требуется
самому создавать объекты этого класса, один такой объект содержится в объекте 
$(LINK2 ddd_manager.html#DDDМенеджер, DDDМенеджер) в поле под именем $(B визуализатор) и 
доступен публично.
+/
class Визуализатор {

    protected DDDМенеджер менеджер;
    protected float соотношение; 
    protected float[4] цвет_фона = [0.0f, 0.0f, 0.0f, 1.0f];

    /// Конструктор, принимающий менеджер программы.
    this(DDDМенеджер ddd_менеджер) {
        менеджер = ddd_менеджер;
        поверхность_создана();
        поверхность_изменилась();
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, "Создан визуализатор");
    }

    
/*    void инициализация(DDDМенеджер ddd_менеджер) {
        менеджер = ddd_менеджер;
    }
*/
    /++
    Функция, позволяющая задать цвет фона в окне программы.

    Params:
        цвет = массив из 4 чисел, определяющиих требуемый цвет фона.
    +/
    public void задать_цвет_фона(const(float[4]) цвет){
        цвет_фона = цвет;
    }

    /// Функция, выводящая видимые 3D-объекты и инфопанели в буфер экрана.
    public abstract void отрисовка_кадра(); 

    /// Функция, перерисовывающая содержимое окна в случае каких либо изменений с ним.
    public abstract void поверхность_изменилась();

    /// Функция, инициализирующая OpenGL-контекст.
    public abstract void поверхность_создана(); 

    
    //void отрисовка_инфопанелей() {
    //    менеджер.отрисовка_инфопанелей();
    //}
    

    /++
    Функция, реализующая Игровой Цикл. 
    
    По-умолчанию визуализируется бесконечное количество кадров. Выход из функции 
    (и из всей программы) осуществляется, если вызываемая внутри функция объекта
    $(B DDDМенеджер) $(LINK2 ddd_manager.html#обработка_событий, обработка_событий) вернула 
    значение true.

    Params:
        максимум_кадров = Если задать положительное число, будет отображено такое количество
            кадров, после чего функция завершится. Если 
            значение параметра отрицательно, визуализация длится бесконечно.
    +/
    void визуализация(long максимум_кадров=-1) {
             //onDrawFrame();
		long номер_кадра = 0;
		for (номер_кадра=0; (номер_кадра<максимум_кадров) || (максимум_кадров<0) ; ) {
            if (менеджер.обработка_событий())
				break;
            менеджер.обновить_сцену();
			поверхность_изменилась();
            отрисовка_кадра();
            //отрисовка_инфопанелей();
			обменять_окна();
			debug в_журнал(имя_модуля, УровниЖурнала.Отладка, format("Отрисован  кадр %s", номер_кадра));
            номер_кадра++;
        }
		в_журнал(имя_модуля, УровниЖурнала.Информация, format("Закончили рендерить %s кадров", номер_кадра));
    }
    
}

