﻿module ddd.zavisimost.okno_SDL2;

import std.stdio, std.conv, std.string, std.file, std.path, std.process, std.system;
import derelict.opengl3.gl, derelict.sdl2.sdl, derelict.sdl2.image, derelict.sdl2.ttf; //

//import constanty, logging, loadconfig;
import my_utils.journal;
import ddd.types;

enum    имя_модуля="окно_SDL2";

private ПараметрыОкна параметры_окна;    //shared 
//private SDL_Window* параметры_окна_window;
//shared private uint windowID;
//SDL_GLContext главный_контекст;
    
void инициализация_параметров() {
    with (параметры_окна) {
        uint флаг_полный_экран_desktop = режим_desktop ? SDL_WINDOW_FULLSCREEN_DESKTOP : SDL_WINDOW_FULLSCREEN;
        флаг_полный_экран = флаг_полный_экран ? флаг_полный_экран_desktop : 0;
    }
}    
    
void инициализация_SDL() {
    //string cwd=getcwd();
    //в_журнал(имя_модуля, УровниЖурнала.Информация, format("Текущий каталог %s", cwd));
    version(Windows) {
        /*
        auto path = environment["PATH"];
        environment["PATH"] = "D:\\moe\\prg\\106-D-workspaces\\DDDworkspace\\DDD\\test\\dll"; //path ~ ";" ~ 
        */
        
        DerelictSDL2.load(); 
        в_журнал(имя_модуля, УровниЖурнала.Информация, "Шаг 1.1 Загружен SDL");
        DerelictSDL2Image.load();
        в_журнал(имя_модуля, УровниЖурнала.Отладка, "Шаг 1.2. Загружен DerelictSDL2Image");
        DerelictSDL2ttf.load();
        в_журнал(имя_модуля, УровниЖурнала.Отладка, "Шаг 1.3. Загружен DerelictSDL2ttf");
        
        /*
        DerelictSDL2.load("dll\\SDL2.dll"); 
        в_журнал(имя_модуля, УровниЖурнала.Информация, "Шаг 1.1. Загружен SDL");
        DerelictSDL2Image.load([".\\dll\\SDL2_image.dll", ".\\dll\\libpng16-16.dll", ".\\dll\\libjpeg-9.dll", "dll\\libwebp-7.dll", "dll\\libtiff-5.dll"]);
        в_журнал(имя_модуля, УровниЖурнала.Отладка, "Шаг 1.2. Загружен DerelictSDL2Image");
        DerelictSDL2ttf.load("dll\\SDL2_ttf.dll"); //, dll\\libfreetype-6.dll, dll\\zlib1.dll");
        в_журнал(имя_модуля, УровниЖурнала.Отладка, "Шаг 1.3. Загружен DerelictSDL2ttf");
        */
    }
    else {
        DerelictSDL2.load(); 
        в_журнал(имя_модуля, УровниЖурнала.Информация, "Шаг 1.1 Загружен SDL");
        DerelictSDL2Image.load();
        в_журнал(имя_модуля, УровниЖурнала.Отладка, "Шаг 1.2. Загружен DerelictSDL2Image");
        DerelictSDL2ttf.load();
        в_журнал(имя_модуля, УровниЖурнала.Отладка, "Шаг 1.3. Загружен DerelictSDL2ttf");
    }
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    //if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
        throw new Exception(": Невозможно инициализировать видеорежим. Ошибка в SDL_Init().");
    в_журнал(имя_модуля, УровниЖурнала.Информация, "Шаг 2. Инициализирован SDL");

    int результат_ttf = TTF_Init();
    if (результат_ttf<0)
        в_журнал(имя_модуля, УровниЖурнала.Ошибка, "Библиотека SDL2ttf не проинициализировалась!");
    
    //chdir(cwd);
}
    
    
 //Для Derelict3
bool инициализация_окна_SDL_GL(ПараметрыОкна параметры_окна0) {
    параметры_окна = параметры_окна0;
    инициализация_SDL();
    в_журнал(имя_модуля, УровниЖурнала.Отладка, "Функция initSDL() отработала");
    DerelictGL.load();
    //DerelictGL3.load(); // Для OpenGL с шейдерами без матриц
    в_журнал(имя_модуля, УровниЖурнала.Информация, "Шаг 3. Загружен DerelictGL");
    инициализация_параметров();
    //initializevars();
    //int ширина_окна = to!(int) (configvarsl.get("window_width", window_width_default));
    //int window_height= to!(int) (configvarsl.get("window_height", window_height_default));
    //const(char)* window_name2=toStringz(window_name1);
    const(char)* имя_окна2=toStringz(параметры_окна.имя_окна);
    auto главное_окно = SDL_CreateWindow(имя_окна2, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        параметры_окна.ширина_окна, параметры_окна.высота_окна, 
        параметры_окна.флаг_полный_экран | SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN ); //| SDL_WINDOW_RESIZABLE);
    //главное_окно = null;
    if (!главное_окно) // Выключаемся, если создать не получилось 
        {
        в_журнал(имя_модуля, УровниЖурнала.Ошибка, "Окно не создалось. Выходим.");
        SDL_смерть("Невозможно создать окно");
        return false;
        }
    в_журнал(имя_модуля, УровниЖурнала.Информация, format("Шаг 4. Создано окно размером %sx%s, с именем \"%s\"", 
        параметры_окна.ширина_окна, параметры_окна.высота_окна, параметры_окна.имя_окна));
    auto главный_контекст = SDL_GL_CreateContext(главное_окно);
    if (главный_контекст == null) {
        в_журнал(имя_модуля, УровниЖурнала.Ошибка, "GL-контекст не получен. Выходим.");
        SDL_смерть("Не получен GL-контекст");
        return false;
    }
    в_журнал(имя_модуля, УровниЖурнала.Информация, "Шаг 5. Создан SDL_GL-контекст");
    //Derelict3GL3_reload();
    Derelict2GL1_reload();  // Используем старый OpenGL
    SDL_GL_SetSwapInterval(параметры_окна.флаг_синхронизация);
    параметры_окна.ID_окна=SDL_GetWindowID(главное_окно);
    //LogDisplayModes(главное_окно);
    
    // Выясняем, какой видеорежим получился на самом деле
    SDL_DisplayMode режим;
    auto display_index=SDL_GetWindowDisplayIndex(главное_окно);
    SDL_GetCurrentDisplayMode(display_index, &режим);
    в_журнал(имя_модуля, УровниЖурнала.Информация, format("Текущий видеорежим:  %sx%s %sHz %s bpp", режим.w, режим.h, режим.refresh_rate, SDL_BITSPERPIXEL(режим.format)));
    // Заполняем длину и ширину окна реальными значениями
    if (параметры_окна.флаг_полный_экран) {
        параметры_окна.ширина_окна=режим.w;
        параметры_окна.высота_окна=режим.h;
    }
    //SDL_ShowCursor(SDL_DISABLE);
    //SDL_SetRelativeMouseMode(true);
    /* 
    //Временные пробные команды
    writeln("Щас буду матрицу загружать");
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glClearColor(0,0,1,1);
    glClear(GL_COLOR_BUFFER_BIT);
    SDL_GL_SwapWindow(главное_окно);
    */
    return true;
}


void Derelict3GL3_reload() {
    GLVersion версия_gl = DerelictGL3.reload();
    //writeln("GLVersion=", версия_gl);
    в_журнал(имя_модуля, УровниЖурнала.Информация, format ("Шаг 6. Перезагружен GL3. Версия GL = %s ", версия_gl));
}


void Derelict2GL1_reload() {
    GLVersion версия_gl;
    try
        версия_gl = DerelictGL.reload(GLVersion.None, GLVersion.GL21);
        //версия_gl=DerelictGL.loadClassicVersions(GLVersion.GL21);
    catch (Throwable)
        версия_gl=GLVersion.GL11;
    //GLVersion версия_gl=DerelictGL.maxVersion();
    в_журнал(имя_модуля, УровниЖурнала.Информация, format ("Шаг 6. Перезагружен GL3. Версия GL = %s ", версия_gl));
}


void SDL_выход() {
    if (параметры_окна.ID_окна) {
        auto окно=SDL_GetWindowFromID(параметры_окна.ID_окна);
        SDL_DestroyWindow(окно);
    }
    derelict.sdl2.sdl.SDL_Quit();
}

void SDL_смерть(const char *сообщение)
{
    //writeln("%s: %s\n", сообщение, SDL_GetError());
    SDL_выход();
    throw new Exception("Ошибка: " ~ to!(string)(сообщение));
}

void SDL_восстановление_размеров_окна() {
    в_журнал(имя_модуля, УровниЖурнала.Информация, "Восстанавливаем окно");
    auto окно=SDL_GetWindowFromID(параметры_окна.ID_окна);
    SDL_SetWindowSize(окно, параметры_окна.ширина_окна, параметры_окна.высота_окна);
}

uint SDL_получить_флаги_окна() {
    auto окно=SDL_GetWindowFromID(параметры_окна.ID_окна);
    return SDL_GetWindowFlags(окно);
}

bool SDL_окно_минимизировано() {
    return ((SDL_WINDOW_MINIMIZED & SDL_получить_флаги_окна()) != 0);  
} 

void SDL_обменять_окна() {
    auto окно=SDL_GetWindowFromID(параметры_окна.ID_окна);
    //auto окно=параметры_окна_window;
    SDL_GL_SwapWindow(окно);
}

int SDL_ширина_окна() {
    return параметры_окна.ширина_окна;
}

int SDL_высота_окна() {
    return параметры_окна.высота_окна;
}

void SDL_установить_мышь_в_центре() {
    int x=параметры_окна.ширина_окна / 2;
    int y=параметры_окна.высота_окна / 2;
    SDL_WarpMouseInWindow(null, x, y);
}

void SDL_включить_управление_мышью() {
    SDL_ShowCursor(SDL_DISABLE);
    auto окно=SDL_GetWindowFromID(параметры_окна.ID_окна);
    SDL_SetWindowGrab(окно, true);
    SDL_SetRelativeMouseMode(true);
}

void SDL_выключить_управление_мышью() {
    SDL_ShowCursor(SDL_ENABLE);
    SDL_SetRelativeMouseMode(false);
    auto окно=SDL_GetWindowFromID(параметры_окна.ID_окна);
    SDL_SetWindowGrab(окно, false);
    SDL_установить_мышь_в_центре();
}

void SDL_получить_центр_окна(ref int x, ref int y) {
    x=параметры_окна.ширина_окна / 2;
    y=параметры_окна.высота_окна / 2;
} 

void SDL_режимы_экрана_в_журнал() { //(SDL_Window*  главное_окно) {
    SDL_DisplayMode режим;
    int всего_режимов=SDL_GetNumDisplayModes(0);
    в_журнал(имя_модуля, УровниЖурнала.Информация, format("Доступно %s видеорежимов", всего_режимов));
    for (int j = 0; j < всего_режимов; ++j) {
       SDL_GetDisplayMode(0, j, &режим);
       в_журнал(имя_модуля, УровниЖурнала.Информация, format("Режим %s:  %sx%s %sHz %s bpp", j, режим.w, режим.h, режим.refresh_rate, SDL_BITSPERPIXEL(режим.format)));
    }
    SDL_GetCurrentDisplayMode(0, &режим);
    в_журнал(имя_модуля, УровниЖурнала.Информация, format("Текущий режим:  %sx%s %sHz %s bpp", режим.w, режим.h, режим.refresh_rate, SDL_BITSPERPIXEL(режим.format)));
    
}

void SDL_изменить_режим_экрана(SDL_Window*  главное_окно, int номер_режима) {
    //Как-то странно работает
    SDL_DisplayMode режим;
    SDL_GetDisplayMode(0, номер_режима, &режим);
    в_журнал(имя_модуля, УровниЖурнала.Информация, format("Пытаемся установить режим %s:  %sx%s %sHz %s bpp", номер_режима, режим.w, режим.h, режим.refresh_rate, SDL_BITSPERPIXEL(режим.format)));
    if (параметры_окна.флаг_полный_экран) {
        int rez=SDL_SetWindowDisplayMode(главное_окно, &режим);
        //SDL_SetWindowFullscreen(главное_окно, 0);
        //SDL_Delay(5);
        //SDL_SetWindowFullscreen(главное_окно, параметры_окна.флаг_полный_экран);
        //SDL_Delay(5);
        if (rez!=0) 
            в_журнал(имя_модуля, УровниЖурнала.Информация, "Не удалось установить режим экрана");
        else {
            SDL_GetCurrentDisplayMode(0, &режим);
            в_журнал(имя_модуля, УровниЖурнала.Информация, format("Новый режим экрана:  %sx%s %sHz %s bpp", режим.w, режим.h, режим.refresh_rate, SDL_BITSPERPIXEL(режим.format)));
            }
    }   

}

auto SDL_получить_окно() {
    return SDL_GetWindowFromID(параметры_окна.ID_окна);
}

