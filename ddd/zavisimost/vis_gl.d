module ddd.zavisimost.vis_gl;

import std.conv, std.stdio;
import derelict.opengl3.gl; //, derelict.sdl2.sdl;
import ddd.vis, ddd.ddd_manager, ddd.camera; 
import ddd.zavisimost.okno_SDL2;

class Визуализатор_gl : Визуализатор {

    this(DDDМенеджер ddd_менеджер) {
        super(ddd_менеджер);
    }
    
    override public void отрисовка_кадра() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(цвет_фона[0], цвет_фона[1], цвет_фона[2], цвет_фона[3]);
        
        glEnable(GL_BLEND);
        glEnable(GL_TEXTURE_2D);            //Enable Texture Mapping ( NEW )
        //glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        
        //glFrontFace(GL_CCW);
        glEnableClientState(GL_VERTEX_ARRAY);
        glEnableClientState(GL_TEXTURE_COORD_ARRAY);
        glEnableClientState(GL_NORMAL_ARRAY);
        
        // --- рендерим Фон. Тест глубины отключен
        glDisable(GL_DEPTH_TEST);
        glDisable(GL_LIGHTING);
        менеджер.камера.установить_направление_камеры(true);
        менеджер.место_фона.рисовать(1);
        
        
        // --- Включаем тест глубины, рендерим непрозрачные объекты сцены
        glEnable(GL_DEPTH_TEST);
        менеджер.камера.установить_камеру(false);
        glEnable(GL_LIGHTING);
        менеджер.начальное_место.рисовать(1);

        // --- Рендерим прозрачные объекты сцены
        glEnable(GL_DEPTH_TEST); // glDisable(GL_DEPTH_TEST);
        //менеджер.камера.установить_камеру(false);
        менеджер.начальное_место.рисовать(2);


        
        менеджер.отрисовка_инфопанелей();
        
        glDisableClientState(GL_NORMAL_ARRAY);
        glDisableClientState(GL_TEXTURE_COORD_ARRAY);
        glDisableClientState(GL_VERTEX_ARRAY);
    }

    override public void поверхность_изменилась() {
        int ширина = SDL_ширина_окна();
        int высота = SDL_высота_окна();
        glViewport(0, 0, ширина, высота);
        соотношение = to!(float)(ширина) / высота;
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        if (менеджер.камера is null)
            glFrustum(-соотношение, соотношение, -1, 1, 2, 100);
        else
            glFrustum(-соотношение, соотношение, -1, 1, 
                менеджер.камера.ближнее_отсечение, менеджер.камера.дальнее_отсечение);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
    }

    override public void поверхность_создана() {
        glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST);
        
        glEnable(GL_CULL_FACE);
        glShadeModel(GL_SMOOTH);
        glEnable(GL_DEPTH_TEST);
         
        glEnable(GL_TEXTURE_2D);            //Enable Texture Mapping ( NEW )
         
        //glEnable(GL_LIGHTING);
        //glLightModelfv(GL_LIGHT_MODEL_AMBIENT, &([0.1f, 0.1f, 0.1f, 1.0f])[0]);
    }

}
