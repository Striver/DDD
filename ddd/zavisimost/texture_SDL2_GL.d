module ddd.zavisimost.texture_SDL2_GL;

import derelict.opengl3.gl, derelict.sdl2.sdl, derelict.sdl2.image, derelict.sdl2.ttf;
import std.string, std.conv;
    
import ddd.texture;
import ddd.zavisimost.shrift_SDL2;
import my_utils.journal;

private enum имя_модуля = "текстура_SDLGL";

class Текстура_SDL2 : Текстура {
    
    override void загрузить_из_файла(string имя_файла) {
    
        SDL_Surface* поверхность = null;
    
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, format("Щас текстуру %s грузить будем!", имя_файла));
        поверхность = IMG_Load(toStringz(имя_файла));
    
        if (поверхность is null)
        {
            в_журнал(имя_модуля, УровниЖурнала.Ошибка, format("Картинка %s не загрузилась" , имя_файла));
            в_журнал(имя_модуля, УровниЖурнала.Ошибка, format("%s", fromStringz(IMG_GetError())));
            return;
        }   
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, "По крайней мере картинку загрузили!");

        _назначить_текстуру(поверхность);
    
        SDL_FreeSurface(поверхность);
    }

    void построить_из_текста(TTFШрифт шрифт, string текст, float[3] цвет0 ) {
        
        SDL_Color цвет_текста = SDL_Color(to!ubyte(цвет0[2]*255), to!ubyte(цвет0[1]*255), to!ubyte(цвет0[0]*255));
        const(char)* текст_z = toStringz(текст);
        //SDL_Surface* поверхность = TTF_RenderUTF8_Solid( gFont, текст_z, цвет_текста );
        SDL_Surface* поверхность = TTF_RenderUTF8_Blended_Wrapped(шрифт(), текст_z, цвет_текста , 600);
        if (поверхность is null)
        {
            в_журнал(имя_модуля, УровниЖурнала.Ошибка, format("Текст \"%s\" не построился" , текст));
            return;
        }   
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, "По крайней мере текст построили!");

        _назначить_текстуру(поверхность);
        if (номер_текстуры)
            debug в_журнал(имя_модуля, УровниЖурнала.Отладка,  
                format("Текст загружен в текстуру! Width=%s, Height=%s", ширина, высота));
        
        SDL_FreeSurface(поверхность);
        //TTF_CloseFont( gFont );
    }
    
    
    abstract protected void _назначить_текстуру(SDL_Surface* поверхность); 

}  

class Текстура_SDL2_GL : Текстура_SDL2 {
    
    override void рисовать() {
        glBindTexture(GL_TEXTURE_2D, номер_текстуры);
    }
    
    override protected void _назначить_текстуру(SDL_Surface* поверхность) {
        GLuint полученный_номер = 0;
        if (номер_текстуры == 0)
            glGenTextures(1, &полученный_номер);
        else
            полученный_номер = номер_текстуры;
		
        glBindTexture(GL_TEXTURE_2D, полученный_номер);
    
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    
        формат = GL_RGB; 
    
        //access violation on поверхность (until I added NULL test)
        if(поверхность.format.BytesPerPixel == 4) {
            формат = GL_RGBA;
        }
    
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, "Щас текстуру определять будем!");
        glTexImage2D(GL_TEXTURE_2D, 0, формат, поверхность.w, поверхность.h, 0, формат, GL_UNSIGNED_BYTE, поверхность.pixels); 
		GLenum ошибка = glGetError(); 
		if (ошибка != GL_NO_ERROR) {
			в_журнал(имя_модуля, УровниЖурнала.Ошибка, format("Ошибка копирования текстуры %s", ошибка));
			glDeleteTextures(1, &полученный_номер);
			return;
		}
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, "Определили текстуру");
		
		ширина = поверхность.w;
		высота = поверхность.h;
		
        номер_текстуры = полученный_номер;
		
	}

    ~this() {
		if (номер_текстуры != 0)
			glDeleteTextures(1, &номер_текстуры);
	}

}


class ТекстураВПамяти_SDL2 : ТекстураВПамяти {
    SDL_Surface* сохранённая_поверхность = null;

    override void загрузить_из_файла(string имя_файла) {
    
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, format("Щас текстуру в память %s грузить будем!", имя_файла));
        сохранённая_поверхность = IMG_Load(toStringz(имя_файла));
    
        if (сохранённая_поверхность is null)
        {
            в_журнал(имя_модуля, УровниЖурнала.Ошибка, format("Картинка %s не загрузилась" , имя_файла));
            в_журнал(имя_модуля, УровниЖурнала.Ошибка, format("%s", fromStringz(IMG_GetError())));
            return;
        }   
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, "По крайней мере картинку загрузили!");

        формат = GL_RGB; 
    
        //access violation on поверхность (until I added NULL test)
        if(сохранённая_поверхность.format.BytesPerPixel == 4) {
            формат = GL_RGBA;
        }

        ширина = сохранённая_поверхность.w;
        высота = сохранённая_поверхность.h;
    }

    override void* получить_пиксели() {
        if (сохранённая_поверхность)
            return сохранённая_поверхность.pixels;
        return null;
    }

    override int получить_размер_строки() {
        if (сохранённая_поверхность)
            return сохранённая_поверхность.pitch;
        return -1;
    }

    override int получить_размер_пикселя() {
        if (сохранённая_поверхность)
            return сохранённая_поверхность.format.BytesPerPixel;
        return -1;
    }
    
    ~this() {
        if (сохранённая_поверхность)
            SDL_FreeSurface(сохранённая_поверхность);
    }

}
