module ddd.zavisimost.sobytija_SDL2;

import std.string;
public import derelict.sdl2.sdl; 

import ddd.okno, ddd.ddd_manager, ddd.okno, ddd.vector; //, ddd.zavisimost.okno_SDL2;
import my_utils.journal;

private enum имя_модуля = "события";

const float ИЗМЕНЕНИЕ_КООРДИНАТ = 1;
const float ИЗМЕНЕНИЕ_УГЛА = 0.1f;

class ОбработчикСобытий {
    protected bool delegate(SDL_Event e) [SDL_EventType]         функция1; //функции обработки всех событий
    protected bool delegate(SDL_Event e) [SDL_EventType][int]    функция2; //функции дополнительной обработки событий с параметром
    
    this() {
        _регистратор_функций1();
        _регистратор_функций2();
    }

    protected void _регистратор_функций1() {}

    protected void _регистратор_функций2() {}

    bool обработка() {
        return обработка_всех_событий();
    }

    bool обработка_всех_событий() {
        SDL_Event e;
		bool результат = false; 
        while (SDL_PollEvent(&e))
            результат = результат || функция1.get(e.type, &заглушка)(e);
        return результат; // Если истина - выходим из программы
    }

    bool заглушка(SDL_Event e) {
        return false;
    }
    
    void зарегистрировать_функцию1(SDL_EventType тип, bool delegate(SDL_Event e) функция) {
        функция1[тип] = функция;
        функция2[тип][-1] = &заглушка;
    }
    
    void разрегистрировать_функцию1(SDL_EventType тип) {
        функция1.remove(тип);
        функция2.remove(тип);
    }
    
    void зарегистрировать_функцию2(SDL_EventType тип, int код_события, bool delegate(SDL_Event e) функция) {
        функция2[тип][код_события] = функция;
    }
    
    void разрегистрировать_функцию2(SDL_EventType тип, int код_события) {
        if (тип in функция2)
            функция2[тип].remove(код_события);
    }

    public @property bool управление_мышью() {
        return false;
    }
}

class МинимальныйОбработчикСобытий : ОбработчикСобытий {

    this() {
        super();
    }
    
    override protected void _регистратор_функций1() {
        зарегистрировать_функцию1(SDL_QUIT, &обработка_выхода);
        зарегистрировать_функцию1(SDL_KEYUP, &обработка_отжатия_клавиши);
        зарегистрировать_функцию1(SDL_KEYDOWN, &обработка_нажатия_клавиши);
        зарегистрировать_функцию1(SDL_WINDOWEVENT, &обработка_события_окна);
    }

    override protected void _регистратор_функций2() {
        зарегистрировать_функцию2(SDL_WINDOWEVENT, SDL_WINDOWEVENT_MINIMIZED, &обработка_минимизации_окна);
        зарегистрировать_функцию2(SDL_WINDOWEVENT, SDL_WINDOWEVENT_EXPOSED, &обработка_показа_окна);
    }

    bool обработка_отжатия_клавиши(SDL_Event e) {
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, format("Отжата клавиша с кодом=%s", e.key.keysym.sym));
        return функция2[e.type].get(e.key.keysym.sym, &заглушка)(e);
    }
    
    bool обработка_нажатия_клавиши(SDL_Event e) {
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, format("Нажата клавиша с кодом=%s", e.key.keysym.sym));
        return функция2[e.type].get(e.key.keysym.sym, &заглушка)(e);
    }
    
    bool обработка_выхода(SDL_Event e) {
        в_журнал(имя_модуля, УровниЖурнала.Информация, "Окно закрыто пользователем");
        return true;
    }
    
    bool обработка_события_окна(SDL_Event e) {
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, format("Событие окна №%s", e.window.event));
        return функция2[e.type].get(e.window.event, &заглушка)(e);
    }

    bool обработка_минимизации_окна(SDL_Event e) {
        в_журнал(имя_модуля, УровниЖурнала.Информация, "Окно минимизировано");
        return false;
    }
    
    bool обработка_показа_окна(SDL_Event e) {
        if (!окно_минимизировано()) 
            восстановление_размеров_окна();
        return false;
    }

}

class СтандартныйОбработчикСобытий : МинимальныйОбработчикСобытий {

    this() {
        super();
        зарегистрировать_функцию2(SDL_KEYUP, SDLK_ESCAPE, &обработка_отжатия_клавиши_ESCAPE);
    }

    bool обработка_отжатия_клавиши_ESCAPE(SDL_Event e) {
        в_журнал(имя_модуля, УровниЖурнала.Информация, "Сработала клавиша Esc");
        return true;
    }
    
}

class ОбработчикСобытийСДвижением : СтандартныйОбработчикСобытий {
    private float сдвиг_x, сдвиг_z, поворот_x, поворот_y;
    private bool управлять_стрелками = true;
    private bool управлять_WASD = false;
    private	int мышь_x, мышь_y;
    private float _изменение_координат = ИЗМЕНЕНИЕ_КООРДИНАТ;
    private float _изменение_угла = ИЗМЕНЕНИЕ_УГЛА;

    this(string кнопки_управления="стрелки") {
        super();
        if (кнопки_управления=="WASD") {
            управлять_стрелками = false;
            управлять_WASD = true;
        }
        if (кнопки_управления=="стрелки и WASD") {
            управлять_стрелками = true;
            управлять_WASD = true;
        }
        _дополнительный_регистратор_функций();
        сдвиг_x = 0;
        сдвиг_z = 0;
        поворот_x = 0;
        поворот_y = 0;
        мышь_x = 0;
        мышь_y = 0;
        //включить_управление_мышью();
        //установить_мышь_в_центре();
    }
    
    private void _регистратор_сдвигов(int кнопка_вправо, int кнопка_влево, int кнопка_вперёд, int кнопка_назад) {
        зарегистрировать_функцию2(SDL_KEYUP, кнопка_вправо, &вправо_влево);
        зарегистрировать_функцию2(SDL_KEYDOWN, кнопка_вправо, &вправо_влево);
        зарегистрировать_функцию2(SDL_KEYUP, кнопка_влево, &вправо_влево);
        зарегистрировать_функцию2(SDL_KEYDOWN, кнопка_влево, &вправо_влево);
        зарегистрировать_функцию2(SDL_KEYUP, кнопка_вперёд, &вперёд_назад);
        зарегистрировать_функцию2(SDL_KEYDOWN, кнопка_вперёд, &вперёд_назад);
        зарегистрировать_функцию2(SDL_KEYUP, кнопка_назад, &вперёд_назад);
        зарегистрировать_функцию2(SDL_KEYDOWN, кнопка_назад, &вперёд_назад);
    }
    
    protected void _дополнительный_регистратор_функций() {
        зарегистрировать_функцию1(SDL_MOUSEMOTION, &движение_мыши);
        if (управлять_стрелками) 
            _регистратор_сдвигов(SDLK_RIGHT, SDLK_LEFT, SDLK_UP, SDLK_DOWN);
        if (управлять_WASD) 
            _регистратор_сдвигов(SDLK_d, SDLK_a, SDLK_w, SDLK_s);
    }
    
    bool вправо_влево(SDL_Event e) {
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, "Двигаем камеру вправо-влево");
        сдвиг_x = 0;
		auto состояние = SDL_GetKeyboardState(null);
		if ((управлять_стрелками && состояние[SDL_SCANCODE_RIGHT]) || 
            (управлять_WASD && состояние[SDL_SCANCODE_D]))
			сдвиг_x += _изменение_координат;
		if ((управлять_стрелками && состояние[SDL_SCANCODE_LEFT]) || 
            (управлять_WASD && состояние[SDL_SCANCODE_A]))
			сдвиг_x -= _изменение_координат;
        return false;
    }
    
    bool вперёд_назад(SDL_Event e) {
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, "Двигаем камеру вперёд-назад");
        сдвиг_z = 0;
		auto состояние = SDL_GetKeyboardState(null);
		if ((управлять_стрелками && состояние[SDL_SCANCODE_UP]) || 
            (управлять_WASD && состояние[SDL_SCANCODE_W]))
			сдвиг_z += _изменение_координат;
		if ((управлять_стрелками && состояние[SDL_SCANCODE_DOWN]) || 
            (управлять_WASD && состояние[SDL_SCANCODE_S]))
			сдвиг_z -= _изменение_координат;
        return false;
    }

    bool движение_мыши(SDL_Event e) {
        SDL_GetRelativeMouseState(&мышь_x, &мышь_y);
        поворот_x += мышь_x;
        поворот_y += мышь_y;
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, format("Относительные координаты мыши x=%s, y=%s", мышь_x, мышь_y));
        return false;
    }
    
    void события_в_обновлении_сцены(DDDМенеджер менеджер, float длительность_кадра) {
        //debug в_журнал(имя_модуля, УровниЖурнала.Отладка, format("Поворот мыши x=%s, y=%s", поворот_x, поворот_y));
        if (поворот_x != 0) {
            менеджер.место_камеры.поворот(_изменение_угла*поворот_x*длительность_кадра);
            поворот_x = 0;
        }
        if (поворот_y != 0) {
            менеджер.место_камеры.наклон(_изменение_угла*поворот_y*длительность_кадра);
            поворот_y = 0;
        }
        Вектор3 сдвиг1 = Вектор3(сдвиг_x*длительность_кадра, 0, -сдвиг_z*длительность_кадра);
        Вектор3 сдвиг2 = менеджер.место_камеры.получить_ориентацию() * сдвиг1;
        менеджер.место_камеры.сдвинуть(сдвиг2);
    }

    public @property void скорость_движения(float скорость) {
        _изменение_координат = скорость;
    } 

    public @property float скорость_движения() {
        return _изменение_координат;
    } 

    public @property void скорость_поворота(float скорость) {
        _изменение_угла = скорость;
    } 

    public @property float скорость_поворота() {
        return _изменение_угла;
    } 

    override public @property bool управление_мышью() {
        return true;
    }

}

