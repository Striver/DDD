module ddd.zavisimost.shrift_SDL2;

import derelict.sdl2.sdl, derelict.sdl2.ttf;
import std.string, std.conv; //, core.stdc.string, std.conv, std.json, std.file;
//import std.string : fromStringz;

import ddd.resources; //, texturesymbolattributes;
import my_utils.journal;

private enum имя_модуля = "шрифт_SDL";
    
enum ТИП_РЕСУРСА_TTF = "TTF_шрифт";
enum ТИП_РЕСУРСА_TEXTATTR = "texfontattr";

enum РАЗДЕЛИТЕЛЬ = "_";

/*
inout(char)[] fromStringz(inout(char)* cString) @system pure {
    return cString ? cString[0 .. strlen(cString)] : null;
}
*/

class TTFШрифт {

private:

	TTF_Font *gFont;
	
public:   
    
    void загрузить_из_файла(string имя_файла, int размер) {
    
        в_журнал(имя_модуля, УровниЖурнала.Отладка, format("Щас шрифт %s грузить будем!", имя_файла));
		gFont = TTF_OpenFont( toStringz(имя_файла), размер );
		if (gFont is null) {
			auto error = fromStringz(TTF_GetError());
			в_журнал(имя_модуля, УровниЖурнала.Ошибка, format("Шрифт %s_%d не загрузился. Ошибка: %s ", 
                                                              имя_файла, размер, error));
		}
    
        в_журнал(имя_модуля, УровниЖурнала.Отладка, format("Шрифт %s_%s загрузили!", имя_файла, размер));
   
    }

   
    public TTF_Font* opCall() {
		return gFont;
    }
	
	~this() {
		if (!(gFont is null))
			TTF_CloseFont( gFont );
	}
    
}

class ЗагруженныеTTFШрифты : ЗагруженныеРесурсы!(TTFШрифт) {
	
    this(ИменаРесурсов имена_ресурсов0) {
        super(имена_ресурсов0);
    }


    //параметр имя должен быть в виде "имя-файла-шрифта_размер", например "LiberationSerif-Italic_32"
    override protected bool загрузить_один(string имя) {
        string имя_файла;
        TTFШрифт новый_шрифт;
		int размер;
        ptrdiff_t индекс_;
        
		индекс_ = lastIndexOf(имя, РАЗДЕЛИТЕЛЬ);
		if (индекс_ < 0) {
			в_журнал(имя_модуля, УровниЖурнала.Ошибка, format("Имя шрифта %s неверно построено.", имя));
			return false;
		}
		имя_файла = имя[0 .. индекс_];
		try
			размер = to!int(имя[индекс_+1 .. $]);
		catch (ConvException) {
			в_журнал(имя_модуля, УровниЖурнала.Ошибка, format("Размер шрифта %s не определяется.", имя));
			return false;
		}
        try 
            имя_файла = имена_ресурсов.получить_имя_файла_ресурса(имя_файла, ТИП_РЕСУРСА_TTF);
        catch (РесурсыИсключение) {
            в_журнал(имя_модуля, УровниЖурнала.Ошибка, format("Отсутствует файл шрифта %s", имя));
            return false;
        }
        новый_шрифт = new TTFШрифт();
        try 
			новый_шрифт.загрузить_из_файла(имя_файла, размер);
        catch (Throwable) {
            в_журнал(имя_модуля, УровниЖурнала.Ошибка, format("Ошибка в файле шрифта %s", имя_файла));
            return false;
        }
		ресурсы[имя] = новый_шрифт;
        return true;
    }
    
}

/*
class TextureFontAttr : LoadableResource {

private: 
	TextureSymbolAttributes[string] attributes;
	int fontHeight;
	string имя;
	
	// Приватный конструктор. Класс создаём только из JSON-файла
	this(string имя, int Height, TextureSymbolAttributes[string] attr){
		attributes=attr;
		this.имя=имя;
		fontHeight=Height;
	
	
}
public:   
    
	// --- принимаем атрибуты из файла в формате JSON ----
	static public TextureFontAttr fromJSON(JSONValue jsv) {
		JSONValue[string] jsv1, jsv2;
		string fontname;
		int fontHeight;
		try {
			jsv1=jsv.object;
			fontname=jsv1["fontname"].str;
			fontHeight=to!int(jsv1["fontHeight"].integer);
			jsv2=jsv1["symbols"].object;
		}	
		catch {
			log(log_module, LogUrovni.Error, "Файл атрибутов шрифта не соответсвует формату");
			log(log_module, LogUrovni.Debug, format("fontname=%s, fontHeight=%s, symbols=%s", 
				fontname, fontHeight, jsv2));
			return null;
		}
		TextureSymbolAttributes[string] list;
		foreach (sym, attr; jsv2) {
			try {
				JSONValue[] arr=attr.array;
				short[7] a;
				for (int i=0; i<arr.length; i++) 
					a[i]=to!(short)(arr[i].integer);
				list[sym]=TextureSymbolAttributes(a);
			}
			catch
				log(log_module, LogUrovni.Warning, format("Атрибут символа \"%s\" не прочитался",sym));
		}
		TextureFontAttr ret= new TextureFontAttr(fontname, fontHeight, list);
		return ret;
	}

	TextureSymbolAttributes opIndex(string sym) {
		return attributes[sym];
	}
	
	@property int Height() {return fontHeight;}
    
}

class LoadedTextureFontAttr : LoadedResources {
	
	this(ResourceNames resources) {
		super(resources);
	}

    override protected bool load1(string имя) {
        string имя_файла;
        TextureFontAttr newTexattr;
		JSONValue jsv;
        try 
            имя_файла=resources.getfilenameres(имя, Tip_resursaTexattr);
        catch (ResourseException) {
            log(log_module, LogUrovni.Error, format("Отсутствует файл атрибутов шрифта %s", имя));
            return false;
        }
		string text1=readText(имя_файла);
		try 
			jsv=parseJSON(text1);
        catch {
            log(log_module, LogUrovni.Error, format("Ошибка в файле атрибутов шрифта %s", имя_файла));
            return false;
        }
		newTexattr=TextureFontAttr.fromJSON(jsv);
		if (newTexattr) {
			res[имя]=newTexattr;
			return true;
		}
		else
			return false;
	}
}
*/
