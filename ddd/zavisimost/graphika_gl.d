module ddd.zavisimost.graphika_gl;

import derelict.opengl3.gl;
import ddd.matrix;

import std.string;
import my_utils.journal;

enum имя_модуля="графика_gl";

static const GLenum[8] GL_ЛАМПЫ=
    [GL_LIGHT0,
     GL_LIGHT1,
     GL_LIGHT2,
     GL_LIGHT3,
     GL_LIGHT4,
     GL_LIGHT5,
     GL_LIGHT6,
     GL_LIGHT7];

enum GL_ОКРУЖАЮЩИЙ_СВЕТ = GL_AMBIENT;
enum GL_ДИФФУЗНЫЙ_СВЕТ = GL_DIFFUSE;
enum GL_БЛИКОВЫЙ_СВЕТ = GL_SPECULAR;

void gl_сохранить_матрицу() { glPushMatrix(); }    

void gl_восстановить_матрицу() { glPopMatrix(); }  

void gl_перемножить_матрицы(const GLfloat *m) { 
    debug в_журнал(имя_модуля, УровниЖурнала.Отладка, format("Умножение на матрицу %s %s %s %s", *m, *(m+1), *(m+2), *(m+3)));
    glMultMatrixf(m); 
    float[16] m1;
    glGetFloatv(GL_MODELVIEW_MATRIX, &(m1[0]));
    debug в_журнал(имя_модуля, УровниЖурнала.Отладка, format("Результат умножения на матрицу = (%(%.4g, %))",m1));
}

void gl_загрузить_матрицу(const GLfloat *m) { glLoadMatrixf(m); }

void gl_рисовать_чередующийся_буфер(int граней, void* чередующийся_буфер, void* буфер_индексов) {
    float[16] m1;
    glGetFloatv(GL_MODELVIEW_MATRIX, &(m1[0]));
    debug в_журнал(имя_модуля, УровниЖурнала.Отладка, format("Матрица перед рисованием = (%(%.4g, %))",m1));
    glInterleavedArrays(GL_T2F_N3F_V3F, 0, чередующийся_буфер);
    glDrawElements(GL_TRIANGLES, граней*3, GL_UNSIGNED_SHORT, буфер_индексов);
}

void gl_рисовать_отдельные_буферы(int граней, void* буфер_вершин, void* буфер_нормалей, void* буфер_текстур, void* буфер_индексов) { 
        glVertexPointer(3, GL_FLOAT, 0, буфер_вершин);
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, "Загрузили вершины");
        
        glNormalPointer(GL_FLOAT, 0, буфер_нормалей);
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, "Загрузили нормали");

        glTexCoordPointer(2, GL_FLOAT, 0, буфер_текстур);
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, "Загрузили текстурные координаты");
        
        glDrawElements(GL_TRIANGLES, граней*3, GL_UNSIGNED_SHORT, буфер_индексов);
        debug в_журнал(имя_модуля, УровниЖурнала.Отладка, "Отрисовали меш");
    }  




void gl_задать_окружающий_материал(float[4] материал) {
    glMaterialfv(GL_FRONT, GL_AMBIENT, &материал[0]);
}    

void gl_задать_диффузный_материал(float[4] материал) {
    glMaterialfv(GL_FRONT, GL_DIFFUSE, &материал[0]);
} 

void gl_задать_бликовый_материал(float[4] материал) {
    glMaterialfv(GL_FRONT, GL_SPECULAR, &материал[0]);
} 

void gl_задать_степень_блика(float степень_блика) {
    glMaterialf(GL_FRONT, GL_SHININESS, степень_блика);
} 

void gl_задать_позицию_лампы(size_t номер_лампы, const GLfloat *позиция) {
    glLightfv(GL_ЛАМПЫ[номер_лампы], GL_POSITION, позиция);
}

void gl_включить_лампу(size_t номер_лампы) { glEnable(GL_ЛАМПЫ[номер_лампы]); }

void gl_включить(GLenum параметр) { glEnable(параметр); }

void gl_выключить_лампу(size_t номер_лампы) { glDisable(GL_ЛАМПЫ[номер_лампы]); }


void gl_выключить(GLenum параметр) { glDisable(параметр); }

void gl_изменить_свет(size_t номер_лампы, GLenum вид_освещения, const GLfloat *свет) {
    glLightfv(GL_ЛАМПЫ[номер_лампы], вид_освещения, свет);
}

void gl_отключить_текстуры() {
    glBindTexture(GL_TEXTURE_2D, 0);
}   

void gl_подготовить_рисование_инфопанели(int x, int y, int ширина, int высота) {
    glViewport(x, y, ширина, высота);
    //float ratio = to!(float)(width) / height;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, 1, 0, 1,-1,1);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void gl_подготовить_рисование_элемента_инфопанели(float x, float y, float ширина, float высота) {
    glPushMatrix();
    glTranslatef(x, y, 0);
    glScalef(ширина, высота, 1);
}
