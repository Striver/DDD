module ddd.quaternion;

import std.string, std.math, core.exception, std.stdio;
import ddd.vector, ddd.matrix;

/++
Гиперкомплексное число (w,x,y,z), определяющее повороты объектов в 3D-пространстве.

Для кватернионов определены:

Преобразование в матрицу поворота и обратно.

Унарные операции:
обратить - получение кватерниона, реализующего поворот в обратную сторону на тот же угол.
нормализовать - получение кватерниона единичной длины, реализующего тот же поворот.

Бинарные операции:
умножение * на другой кватернион - результирующий кватернион реализует поворот, соответствующий последовательному применению поворотов двух перемножаемых кватернионов.
умножение * на скаляр.
скалярное_произведение кватернионов.

Функции интерполяции для анимации поворотов во времени:
линейная_интерполяция - быстрый вариант интерполяции (ошибка до 4% в случае угла между кватернионами pi/2), используется для интерполяции между кватернионами, не сильно различающимися между собой.
сферическая_интерполяция - точная, но более медленная реализация интерполяции.
+/
public struct Кватернион {
    float w, x, y, z;

    /// Кватернион, не выполняющий поворотов
    public static const Кватернион НОЛЬ = Кватернион(1,0,0,0);

    /// Конструктор, принимающий 4 числа
    public this( float fW, float fX, float fY, float fZ ){
        w=fW;
        x=fX;
        y=fY;
        z=fZ;
    }
    
    /// Конструктор копирования
    public this( const(Кватернион) другой_кватернион ){
        w=другой_кватернион.w;
        x=другой_кватернион.x;
        y=другой_кватернион.y;
        z=другой_кватернион.z;
    }

    /// Конструктор из матрицы поворота 3x3
    public this( Матрица3 поворот)
    {
        this.из_матрицы_поворота(поворот);
    }

    /// Конструктор, создающий кватернион из угла/оси
    public this(float угол, const(Вектор3) оси)
    {
        this.из_угла_и_оси(угол, оси);
        //writeln("Создан ", this);
    }
    
    /// Конструктор, создающий кватернион из 3 ортонормированных локальных осей
    public  this( Вектор3 x_ось,  Вектор3 y_ось,  Вектор3 z_ось)
    {
        this.из_осей(x_ось, y_ось, z_ось);
    }
    
    /// Конструктор, создающий кватернион напрямую из 4 чисел w/x/y/z
    public  this(float[] массив)
    {
        try {
            w=массив[0];
            x=массив[1];
            y=массив[2];
            z=массив[3];
        }
        catch (RangeError e) {
            w=1.0f;
            x=y=z=0.0f;
        }
    }
    
    /// Функция, строящая кватернион из матрицы поворота
    public void из_матрицы_поворота(const(Матрица3) матрица_поворота) {
        const float[] kRot = матрица_поворота.m;
        float fTrace = kRot[0+0*3]+kRot[1+1*3]+kRot[2+2*3];
        float fRoot;

        if ( fTrace > 0.0 )
        {
            // |w| > 1/2, may as well choose w > 1/2
            fRoot = sqrt(fTrace + 1.0f);  // 2w
            w = 0.5f*fRoot;
            fRoot = 0.5f/fRoot;  // 1/(4w)
            x = (kRot[2+1*3]-kRot[1+2*3])*fRoot;
            y = (kRot[0+2*3]-kRot[2+0*3])*fRoot;
            z = (kRot[1+0*3]-kRot[0+1*3])*fRoot;
        }
        else
        {
            // |w| <= 1/2
            const int[] s_iNext = [ 1, 2, 0 ];
            int i = 0;
            if ( kRot[1+1*3] > kRot[0+0*3] )
                i = 1;
            if ( kRot[2+2*3] > kRot[i*i*3] )
                i = 2;
            int j = s_iNext[i];
            int k = s_iNext[j];

            fRoot = cast(float) sqrt(kRot[i+i*3]-kRot[j+j*3]-kRot[k+k*3] + 1.0f);
            //Real* apkQuat[3] = { &x, &y, &z };
            float[] apkQuat = [0.0f, 0.0f, 0.0f];
            
            apkQuat[i] = 0.5f*fRoot;
            fRoot = 0.5f/fRoot;
            w = (kRot[k+j*3]-kRot[j+k*3])*fRoot;
            apkQuat[j] = (kRot[j+i*3]+kRot[i+j*3])*fRoot;
            apkQuat[k] = (kRot[k+i*3]+kRot[i+k*3])*fRoot;
            x= apkQuat[0];
            y= apkQuat[1];
            z= apkQuat[2];
            
        }       
    }

    /// Функция, строящая кватернион из угла/оси
    public void из_угла_и_оси (float угол, const(Вектор3) ось)
        {
            // assert:  axis[] is unit length
            //
            // The quaternion representing the rotation is
            //   q = cos(A/2)+sin(A/2)*(x*i+y*j+z*k)

            float fHalfAngle = 0.5f*угол ;
            float fSin = cast(float) sin(fHalfAngle);
            this.w = cast(float) cos(fHalfAngle);
            this.x = fSin*ось.x;
            this.y = fSin*ось.y;
            this.z = fSin*ось.z;
        }
    
    public float[] в_угол_и_ось(){
        float[] ret = [0.0f,0.0f,0.0f,0.0f];
        float fSqrLength = x*x+y*y+z*z;
        if ( fSqrLength > 0.0 )
        {
            ret[0] = cast(float) (2.0*acos(w));
            float fInvLength = cast(float) (1.0/sqrt(fSqrLength));
            ret[1] = x*fInvLength;
            ret[2] = y*fInvLength;
            ret[3] = z*fInvLength;
        }
        else
        {
            // angle is 0 (mod 2*pi), so any axis will do
            ret[0] = 0.0f;
            ret[1] = 1.0f;
            ret[2] = 0.0f;
            ret[3] = 0.0f;
        }
        return ret;

    }
    
    /// Функция, строящая кватернион из 3 ортонормированных локальных осей
    public void из_осей (Вектор3 x_ось, Вектор3 y_ось, Вектор3 z_осьs)
    {
        Матрица3 kRotMat = Матрица3();
        float[] kRot = kRotMat.m;

        kRot[0+0*3] = x_ось.x;
        kRot[1+0*3] = x_ось.y;
        kRot[2+0*3] = x_ось.z;

        kRot[0+1*3] = y_ось.x;
        kRot[1*1*3] = y_ось.y;
        kRot[2+1*3] = y_ось.z;

        kRot[0+2*3] = z_ось.x;
        kRot[1+2*3] = z_ось.y;
        kRot[2+2*3] = z_ось.z;

        из_матрицы_поворота(kRotMat);

    }
    
    /// Построение матрицы поворота 3x3 из этого кватерниона
    const Матрица3 в_матрицу_поворота()
    {
        Матрица3 kRotMat = Матрица3();
        float[] kRot = kRotMat.m;
        
        float fTx  = x+x;
        float fTy  = y+y;
        float fTz  = z+z;
        float fTwx = fTx*w;
        float fTwy = fTy*w;
        float fTwz = fTz*w;
        float fTxx = fTx*x;
        float fTxy = fTy*x;
        float fTxz = fTz*x;
        float fTyy = fTy*y;
        float fTyz = fTz*y;
        float fTzz = fTz*z;

        kRot[0+0*3] = 1.0f-(fTyy+fTzz);
        kRot[0+1*3] = fTxy-fTwz;
        kRot[0+2*3] = fTxz+fTwy;
        kRot[1+0*3] = fTxy+fTwz;
        kRot[1+1*3] = 1.0f-(fTxx+fTzz);
        kRot[1+2*3] = fTyz-fTwx;
        kRot[2+0*3] = fTxz-fTwy;
        kRot[2+1*3] = fTyz+fTwx;
        kRot[2+2*3] = 1.0f-(fTxx+fTyy);
        
        return kRotMat;
    }
    
    const Вектор3 x_ось()
    {
        //float fTx  = 2.0f*x;
        float fTy  = 2.0f*y;
        float fTz  = 2.0f*z;
        float fTwy = fTy*w;
        float fTwz = fTz*w;
        float fTxy = fTy*x;
        float fTxz = fTz*x;
        float fTyy = fTy*y;
        float fTzz = fTz*z;

        return Вектор3(1.0f-(fTyy+fTzz), fTxy+fTwz, fTxz-fTwy);
    }
    
    const Вектор3 y_ось()
    {
        float fTx  = 2.0f*x;
        float fTy  = 2.0f*y;
        float fTz  = 2.0f*z;
        float fTwx = fTx*w;
        float fTwz = fTz*w;
        float fTxx = fTx*x;
        float fTxy = fTy*x;
        float fTyz = fTz*y;
        float fTzz = fTz*z;

        return Вектор3(fTxy-fTwz, 1.0f-(fTxx+fTzz), fTyz+fTwx);
    }
    
    const Вектор3 z_ось()
    {
        float fTx  = 2.0f*x;
        float fTy  = 2.0f*y;
        float fTz  = 2.0f*z;
        float fTwx = fTx*w;
        float fTwy = fTy*w;
        float fTxx = fTx*x;
        float fTxz = fTz*x;
        float fTyy = fTy*y;
        float fTyz = fTz*y;

        return Вектор3(fTxz+fTwy, fTyz-fTwx, 1.0f-(fTxx+fTyy));
    }
    
    /++
    Умножение на другой кватернион. Результирующий кватернион реализует поворот, соответствующий последовательному применению поворотов двух перемножаемых кватернионов.
    
    Умножение не коммутативно! k1*k2 != k2*k1
    +/
    public Кватернион opBinary(string операция)(const(Кватернион) к2) 
        if (операция == "*")
    {
        return Кватернион
        (
            w * к2.w - x * к2.x - y * к2.y - z * к2.z,
            w * к2.x + x * к2.w + y * к2.z - z * к2.y,
            w * к2.y + y * к2.w + z * к2.x - x * к2.z,
            w * к2.z + z * к2.w + x * к2.y - y * к2.x
        );
    }

    /++
    Сложение-вычитание с другим кватернионом. Смысл операции сложения можно описать как "смесь" вращений, т.е. мы получим вращение, 
    которое находится между this и к2.
    +/
    public Кватернион opBinary(string операция)(const(Кватернион) к2) 
        if (операция == "+" || операция == "-")
    {
        mixin("return Кватернион(this.w" ~ операция ~ "к2.w, " ~
                             "this.x" ~ операция ~ "к2.x, " ~
                             "this.y" ~ операция ~ "к2.y, " ~ 
                             "this.z" ~ операция ~ "к2.z);" );
    }


    
    /// Версия умножения с последующим присваиванием
    public Кватернион opOpAssign(string операция)(const(Кватернион) к2) 
        if (операция == "*")
    {
        this = this * к2;
        return this;
    }

    
    /// Умножение кватерниона на скаляр
    public Кватернион opBinary(string операция)(float скаляр) 
        if (операция == "*")
    {
        return Кватернион(скаляр*w, скаляр*x, скаляр*y, скаляр*z);
    }
    
    public Кватернион opOpAssign(string операция)(float скаляр) 
        if (операция == "*")
    {
        this = this * скаляр;
        return this;
    }

        
    
    public Кватернион opUnary(string операция)() 
        if (операция == "-")
    {
        return Кватернион(-w,-x,-y,-z);
    }
    
    const float Dot (const(Кватернион) к2) 
    {
        return w*к2.w + x*к2.x + y*к2.y + z*к2.z;
    }

    /// Скалярное произведение кватернионов
    const float скалярное_произведение (const(Кватернион) к2) {
        return this.Dot(к2);
    }
    
    /// Квадрат абсолютного значения кватерниона
    const float Norm () 
    {
        return w*w+x*x+y*y+z*z;
    }
    
    /// Обращение кватерниона - получение кватерниона, реализующего поворот в обратную сторону на тот же угол.
    const Кватернион обратить()
    {
        float fNorm = w*w+x*x+y*y+z*z;
        if ( fNorm > 0.0 )
        {
            float fInvNorm = 1.0f/fNorm;
            return Кватернион(w*fInvNorm,-x*fInvNorm,-y*fInvNorm,-z*fInvNorm);
        }
        else
        {
            // return an invalid result to flag the error
            return Кватернион(1.0,0,0,0);
        }
    }
    
    /// Нормализация кватерниона - получение кватерниона с единичным абсолютным значением, реализующего поворот на тот же угол
    float нормализовать()
    {
        float len = Norm();
        float factor = cast(float)(1.0f / sqrt(len));
        w*=factor;
        x*=factor;
        y*=factor;
        x*=factor;
        return len;
    }
    
    /+++
    Обращение кватерниона с единичным абсолютным значением (быстрее, чем $(LINK2 #Кватернион.обратить обратить()))
    
    В случае, если достоверно не известно, что абсолютное значение кватерниона равно 1, необходимо пользоваться методом $(B обратить()).
    +/
    const Кватернион UnitInverse ()
    {
        return Кватернион(w,-x,-y,-z);
    }
    
    /++
    Умножение кватерниона на вектор - реализация поворота этого вектора.
    
    Returns:
    Вектор той же длины, что и переданный на вход, повёрнутый на угол этого кватерниона.
    +/
    public Вектор3 opBinary(string операция)(Вектор3 v) 
        if (операция == "*")
    {
        // nVidia SDK implementation
        Вектор3 uv, uuv;
        Вектор3 qvec = Вектор3(x, y, z);
        uv = qvec.векторное_произведение(v);
        uuv = qvec.векторное_произведение(uv);
        uv = uv * (2.0f * w);
        uuv = uuv * (2.0f);

        return v + uv + uuv; 
    }
    
    /++
    Поворот вектора. Тоже, что и умножение на этот вектор, но, надеюсь, что работает быстрее...
    Изменяется сам вектор на-месте (из соображений оптимизации).
    +/
    public void повернуть_вектор(ref Вектор3 v) 
    {
        // nVidia SDK implementation
        float uv_x, uv_y, uv_z, uuv_x, uuv_y, uuv_z, ww;
        
        uv_x = y * v.z - z * v.y;
        uv_y = z * v.x - x * v.z;
        uv_z = x * v.y - y * v.x;
        
        uuv_x = 2.0 * (y * uv_z - z * uv_y);
        uuv_y = 2.0 * (z * uv_x - x * uv_z);
        uuv_z = 2.0 * (x * uv_y - y * uv_x);
        
        ww = 2.0 * w;
        
        uv_x *= ww;
        uv_y *= ww;
        uv_z *= ww;
        
        v.x += uv_x + uuv_x;
        v.y += uv_y + uuv_y;
        v.z += uv_z + uuv_z;
    }

    
    /++
    Сравнение кватернионов на равенство с погрешностью
    
    Params:
        к2 = сравниваемый кватернион
        допуск = максимальная погрешность в значении разницы углов между кватернионами, при котором они считаются равными
    +/
    bool equals(const(Кватернион) к2, float допуск) 
    {
        //допуск - максимальный угол между кватернионами
        float fCos = Dot(к2);
        double angle = acos(fCos);

        return (abs(angle) <= допуск)
            || (abs(angle- PI) < допуск);
    }
    
    /// Вывод значения кватерниона
    const public string toString(){
        return format("Кватернион(%.4g,%.4g,%.4g,%.4g)",w,x,y,z);
    }
    
    /++
    Линейная интерполяция между этим кватернионом и переданным.
    
    Линейная интерполяция работает быстрее сферической, но даёт погрешность, которая тем больше, чем больше угол между кватернионами. 
    В DDD для покадровой скелетной анимации используется линейная интерполяция.
    
    Params:
        к2 = Второй кватернион
        t = Параметр интерполяции. При t=0 результат равен этому кватерниону, при t=1 результат равен к2.
        
    Returns:
        Кватернион, являющийся результатом интерполяции между этим кватернионом и к2.
    +/
    const public Кватернион линейная_интерполяция(const(Кватернион) к2, float t) {
        float t1 = (1-t);
        float t2 = t;
        return Кватернион((w*t1 +к2.w*t2),(x*t1 +к2.x*t2),(y*t1 +к2.y*t2),(z*t1 +к2.z*t2));
    }
    
    /++
    Сферическая интерполяция между этим кватернионом и переданным.
    
    Params:
        к2 = Второй кватернион
        t = Параметр интерполяции. При t=0 результат равен этому кватерниону, при t=1 результат равен к2.
        
    Returns:
        Кватернион, являющийся результатом интерполяции между этим кватернионом и к2.
    +/
    // Реализован полный расчёт (много длинных вычислений), 
    // более эффективным вариантом будет определить отдельный класс интерполятора
    const public Кватернион сферическая_интерполяция(const(Кватернион) к2, float t) {
        //return this.линейная_интерполяция(к2, t);
        Кватернион к1 = Кватернион(this);
        Кватернион к2_ = Кватернион(к2);
        к1.нормализовать();
        к2_.нормализовать();
        
        float cos_omega = к1.скалярное_произведение(к2_);
        int знак = 1;
        if(cos_omega < 0 )
        {
            cos_omega = -cos_omega;
            знак = -1;
        }
        if (cos_omega > 0.9999f) cos_omega = 0.9999f;
        float omega = cast(float)(acos(cos_omega));
        float inv_sin_omega = cast(float)(1.0 / sin(omega));
        к1 *= inv_sin_omega;
        к2_ *= inv_sin_omega;
        
        return к1 * cast(float)sin((1.0 - t)*omega ) + к2_ * знак * cast(float)sin( t*omega );
    }
}
